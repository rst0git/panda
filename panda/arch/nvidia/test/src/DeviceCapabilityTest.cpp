/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DeviceCapabilityTest.h"
#include "panda/arch/nvidia/DeviceCapability.h"

namespace ska {
namespace panda {
namespace nvidia {
namespace test {


DeviceCapabilityTest::DeviceCapabilityTest()
    : ::testing::Test()
{
}

DeviceCapabilityTest::~DeviceCapabilityTest()
{
}

void DeviceCapabilityTest::SetUp()
{
}

void DeviceCapabilityTest::TearDown()
{
}

TEST_F(DeviceCapabilityTest, test_fields)
{
    TeslaK40 device;
    ASSERT_EQ( (VersionSpecification<int, int>(3, 5)), device.compute_capability() );
    ASSERT_EQ( 6000000000U, device.total_memory );
}

TEST_F(DeviceCapabilityTest, test_capability_comparision)
{
    ASSERT_TRUE(TeslaK40() > TeslaC2070());
    ASSERT_TRUE(TeslaK40() >= TeslaK40());
}

} // namespace test
} // namespace nvidia
} // namespace panda
} // namespace ska
