/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_STREAM_H
#define SKA_PANDA_STREAM_H
#include "panda/Buffer.h"

namespace ska {
namespace panda {
class BufferManager;

/**
 * @brief
 *      Base class for Streams.
 * @details
 *      The Curiuosly recurring template pattern. Example use:
 * @code
 *   class MyStream : public Stream<MyStream>
 *   {
 *        MyStream(BufferManager& manager) : Stream<MyStream>(manager) {}
 *   };
 * @endcode
 * 
 */

template<typename DerivedStreamType>
class Stream
{
    public:
        Stream();
        ~Stream();

        Buffer<> next();

};


} // namespace panda
} // namespace ska

#include "panda/detail/Stream.cpp"
#endif // SKA_PANDA_STREAM_H 
