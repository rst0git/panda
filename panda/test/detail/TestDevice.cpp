/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestDevice.h"


namespace ska {
namespace panda {
namespace test {


template<typename ArchTag>
TestDevice<ArchTag>::TestDevice()
{
}

template<typename ArchTag>
TestDevice<ArchTag>::~TestDevice()
{
}

template<typename ArchTag>
std::vector<typename TestDevice<ArchTag>::DeviceType> TestDevice<ArchTag>::devices()
{
    return _system.template resources<ArchTag>();
}

/*
template<typename ArchTag>
template<typename... CapabilityTags>
std::vector<typename TestDevice<ArchTag>::DeviceType> TestDevice<ArchTag>::matching_devices()
{
    auto devices=get_devices<typename Traits::Arch>()();
    std::vector<DeviceType> compatible;
    
    std::copy_if(devices.begin(),devices.end(),std::back_inserter(compatible),
         [](std::shared_ptr<panda::PoolResource<typename Traits::Arch> > const& device){
             return supports<CapabilityTags...>::compatible(*device);
         });  
    return compatible;
}
*/

} // namespace test
} // namespace panda
} // namespace ska
