/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PRODUCERAGGREGATOR_H
#define SKA_PANDA_PRODUCERAGGREGATOR_H


#include "panda/Producer.h"
#include "panda/ConceptChecker.h"
#include <utility>
#include <tuple>
#include <memory>

namespace ska {
namespace panda {

namespace detail {
    // generators foor Producer from a tuple type
    template<typename DerivedType, typename... Types>
    struct ProducerType {
    };

    template<typename DerivedType, typename... Types>
    struct ProducerType<DerivedType, std::tuple<Types...>> 
    {
        typedef Producer<DerivedType, Types...> Type;
    };

    template<typename ProducerAggregatorType>
    struct Initializer;
}

/**
 * @brief
 *    Provide an Aggregation of Producer template Types
 *
 * @details
 *    Allows you to combine several Streams into one. N.B. all streams must be associated with different types
 *
 *    example for two classes inheriting form ChunkedSteam: StreamA and StreamB:
 *    @code
 *    template<typename ProducerType=MyStreamType>
 *    class MyStreamType : public ProducerAggregator<ProducerType, StreamA, StreamB> {
 *
 *          typedef ProducerAggregator<ProducerType, StreamA, StreamB> BaseT;
 *
 *       public:
 *          MyStreamType(StreamA* a, StreamB* b) : BaseT(a,b) {}
 *    };
 *
 *    MyStreamType<> my_producer(new StreamA, new StreamB);
 *
 *    DataManager<MyStreamType<>> manager(my_producer)
 *    auto reuslt_tuple = manager.next();
 *
 *    @endcode
 *    n.b. in this case you could also use the convenience class CombinedStream instead of defining your own MyStream class
 *
 *    @code
 *    typedef CombinedStream<StreamA, StreamB> MyStreamType;
 *    @endcode
 */
template<typename DerivedType, template<typename> class... StreamTypes>
class ProducerAggregator : public detail::ProducerType<DerivedType, decltype(std::tuple_cat(std::declval<typename StreamTypes<DerivedType>::DataSetType>()...))>::Type
{
        typedef ConceptChecker<ProducerConcept>::Check<StreamTypes<DerivedType>...> ProducerConceptCheckStreamTypes;

        typedef ProducerAggregator<DerivedType, StreamTypes...> SelfType;
        typedef typename detail::ProducerType<DerivedType, decltype(std::tuple_cat(std::declval<typename StreamTypes<DerivedType>::DataSetType>()...))>::Type BaseT;
        using typename BaseT::ChunkManagerType;

    public:
        using typename BaseT::DataSetType;

    public:
        explicit ProducerAggregator(StreamTypes<DerivedType>*...);
        explicit ProducerAggregator(std::unique_ptr<StreamTypes<DerivedType>>&&...);
        ~ProducerAggregator();
        void stop();

    protected:
        void set_chunk_manager(ChunkManagerType*);
        mutable std::tuple<std::unique_ptr<StreamTypes<DerivedType>>...> _streams;

    private:
        friend ChunkManagerType;

        // probably redundant as we inherit from Producer TODO
        template<typename T, template<typename> class... OtherStreamTypes>
        friend class ProducerAggregator;

    private:
        friend detail::Initializer<SelfType>; // calls _set_stream
        template<typename Stream>
        inline void _set_stream(Stream& s) const;

    private:
        bool process();

};

/**
 * @brief
 *    Convenience class for generating ProducerAggregator that takes 
 *    a list of stream object pointers in its constructor
 */
template<template<typename> class... Streams>
struct CombinedStream : public ska::panda::ProducerAggregator<CombinedStream<Streams...>, Streams...>
{
    typedef ska::panda::ProducerAggregator<CombinedStream<Streams...>, Streams...> BaseT;
    CombinedStream(Streams<CombinedStream<Streams...>>*... args) : BaseT(args...) {}
    CombinedStream(std::unique_ptr<Streams<CombinedStream<Streams...>>>&&... args) : BaseT(std::move(args)...) {}
}; 

} // namespace panda
} // namespace ska

#include "panda/detail/ProducerAggregator.cpp"

#endif // SKA_PANDA_PRODUCERAGGREGATOR_H 
