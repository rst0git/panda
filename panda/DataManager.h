/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DATAMANAGER_H
#define SKA_PANDA_DATAMANAGER_H

#include "panda/concepts/ProducerConcept.h"
#include "panda/ServiceData.h"

#include <tuple>
#include <deque>
#include <memory>
#include <atomic>
#include <mutex>

namespace ska {
namespace panda {

namespace {

template<typename... Args>
struct InsertSharedTupleType;

template<typename... Args>
struct InsertSharedTupleType<std::tuple<Args...>>
{
    typedef std::tuple<std::shared_ptr<typename DataType<Args>::type>...> type;
    static const std::size_t size = sizeof...(Args);
};

} // namespace

// ---------------------
// -- class defintion --
// ---------------------

/**
 * @brief
 *
 *     Manage a queue of data chunks to be served
 *
 * @details
 *
 *     The DataManager is the link between the data producer and the data consumer.
 *     Functionally it:
 *       - supplies objects for the producer to fill with data
 *       - appends objects to a queue of data to be consumed as a coherent data set
 *       - provides an interface for the consumer to retrieve the next item on the data queue
 *       - provides a locking mechanism to keep data in scope (utilising shared_ptr).
 *
 * ## panda::ServiceData
 * @anchor DataSync
 *     If a data type of the Producer is tagged with the panda::ServiceData wrapper e.g. ```panda::ServiceData<MyDataType>```
 *     this indicated service data stream syncing will be activated for that type.
 *     Service data is considered to be relatively static
 *     and so the DataManager will not wait for a new copy
 *     at every iteration. Each next call will get the same copy of the serviceData object until new ServiceData
 *     is recieved to replace it.
 *
 * ## Producer View
 * @anchor ProducerViewSequenceDiagram
 * @verbatim
  Producer process()
  ------------------

      +-------------+          +--------------+
   +->| Producer    |          | DataManager  |
   :  +-------------+          +--------------+
   :          :                       |
   :          :   get_writable()      |
   :          + --------------------->+  creates a suitable object for the Producer to fill
   :          : <---------------------+  and returns it to the caller
   :          :
   :          :
   :  Fill object with data.
   :  Allow the object to go out of
   :  scope will trigger the DataManager
   :  to queue the data for the consumer.
   :          :
   :          :
   +----------+
  @endverbatim

 * ## Consumer View
 * @anchor ConsumerViewSequenceDiagram
 * @verbatim
  Consumer Sequence Diagram
  --------------------------

      +-------------+          +--------------+       +-----------+
   +->| Consumer    |          | DataManager  |       | Producer  |
   :  +-------------+          +--------------+       +-----------+
   :          :                       |                     |
   :          :   next()              |                     |
   :          +---------------------->+                     |
   :          |                       :                     |
   :          |           +---------->+                     |
   :          |           :           :     process()       |
   :          |   while queue empty   +-------------------->+ attempt to get a chunk of data for the queue
   :          |           :           :
   :          |           +-----------+
   :          |                       :
   :          :<----------------------+ return a data set from the queue
   :          :
   :     consume data
   :          :
   +----------+
  @endverbatim
 *  \ingroup resource
 *  @{
 */
template<typename ProducerType>
class DataManager
{
        BOOST_CONCEPT_ASSERT((ProducerConcept<ProducerType>));

        typedef InsertSharedTupleType<typename ProducerType::DataSetType> InserterType;

    public:
        typedef typename InserterType::type DataSetType;

    public:
        DataManager(ProducerType& chunker);
        ~DataManager();

        /**
         * @brief get a suitable object for writing. When object goes out of scope it
         *        will automatically be pushed to the appropriate data queue.
         *        The data can be abandoned by providing a method is_valid() that returns
         *        false on the object T.
         */
        template<typename T, typename... Args>
        std::shared_ptr<T> get_writable(Args&&...);

        /**
         * @brief fetch the next data set - may block
         */
        DataSetType next();

        /**
         * @brief report the size (number of complete datasets)  of the current data queue
         */
        unsigned size() const;

        /**
         * @brief stop the data manager
         */
        void stop();

    protected:
        /**
         * push data that is to be made available.
         */
        template<typename T>
        void push_chunk(T const& chunk);

        /**
         * push service data that is to be made available.
         */
        template<typename T>
        void push_service_chunk(T const& chunk);

    private:
        template<typename T> struct PushChunk;

        template<typename T, class PushFunction, typename... Args>
        std::shared_ptr<T> get_writable_impl(Args&&... args);

    private:
        volatile bool _abort; // signal from producer
        volatile bool _stop;  // signal from this object
        std::atomic<std::size_t> _awaiting_count;
        ProducerType& _chunker;
        std::deque<DataSetType> _queue;
        std::mutex _queue_mutex;
        DataSetType _last;                            // maintain a copy of the last data set made available
        std::vector<unsigned> _complete_set;          // track which types have been pushed to see if a complete set is ready
};

} // namespace panda
} // namespace ska

#include "panda/detail/DataManager.cpp"

#endif // SKA_PANDA_DATAMANAGER_H
/**
 *  @}
 *  End Document Group
 **/
