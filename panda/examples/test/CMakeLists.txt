include_directories(SYSTEM ${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(unit_tests CsvFileProducerTest.cpp)

add_executable(gtest_panda_examples gtest_main_examples.cpp ${unit_tests})
target_link_libraries(gtest_panda_examples panda_testutils ${PANDA_LIBRARIES} ${GTEST_LIBRARIES})
