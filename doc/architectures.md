@page ArchTutorial Understanding Accelerator Architectures In Panda - How to extend Pandas known architectures

Even if you do not wish to add a new architecture yourself, this tutorial is worth following through to get an
understanding of how panda works to support architectures and the functionality available.

## Where to put your Arhcitecture extension
The architectures should live in the "panda/arch/\<device_type\>" directory. Replace the \<device_type\> with an all lower case
name that reperesents the device.

All files mentioned in this tutorial are assumed to be relative to this directory unless specified otherwise. For the purposes of
this tutorial we will use the name "example_device".

## Defining an architecture tag - Architecture.h
For each device/platform you need to define a compile time tag that is used to differentiate between different accelerators.
If there are multiple ways of using this device, e.g. through OpenCL, or other propriety stacks then you should create a 
separate tag for each one. Note that this tag should be in the example_device namespace.

~~~~~{.cpp}
#include "panda/Architecture.h"

namespace ska {
namespace panda {
namespace example_device {

class ExampleDevice : public panda::Architecture
{
    public:
        ExampleDevice();
};

} // namespace example_device

// we also need to specialise the panda::to_string template for our new type
static std::string const example_device_string("example_device::ExampleDevice");
template<>
constexpr std::string const& to_string<Cpu>() { return example_device_string; }

} // namespace panda
} // namespace ska
~~~~~

## Defining the panda::PoolResource
The job of the PoolResource specialisation is to represet an accelerator device to other classes in panda.
We simply need to create a specialisation of the panda::PoolResource<example_device::ExampleDevice>, n.b this must be in the
ska::panda namespace.

~~~~~{.cpp}
#include "Architecture.h"
#include "panda/Resource.h"

namespace ska {
namespace panda {

/*
 *   Adapt the ExampleDevice type for use in panda reosurce pools
 */
template<>
class PoolResource<example_device::ExampleDevice>
{
    public:
        PoolResource(ExampleDeviceId device_id);
        ~PoolResource();

        /// execute a task on the device
        template<typename TaskType>
        void run(TaskType& job); // at a minimum should call job(*this);

        /// setup a device in a specific configuration
        void configure(DeviceConfig<example_device::ExampleDevice> const&);
};

} // namespace panda
} // namespace ska
~~~~~

## Device Discovery
The panda::System object relies on specialisations of the panda::DicoverResources template class. Its one job is to
return a vector of the devices found on the system as PoolResource<ExampleDevice> objects. n.b the namespace is once again ska::panda.

~~~~~{.cpp}
#include "PoolResource.h"
#include "panda/DiscoverResources.h"

namespace ska {
namespace panda {

// specialisation for ExampleDevice resource discovery
template<>
struct DiscoverResources<example_device::ExampleDevice>
{
    // return a list of discvered devices wrapped as shared_ptr
    std::vector<std::shared_ptr<PoolResource<example_device::ExampleDevice>>> operator()();
};

} // namespace panda
} // namespace ska
~~~~~

## DevicePointers and DeviceIterators
The job of the DevicePointer is to allocate and control the lifetime of any memory on the device.
It should allocate on construction and deallocate on destruction. 

Typically you might write this as a template class that takes different types (e.g. float, int) if the
device is capable of supporting multiple types.

Panda uses iterators extensively for abstraction and optimisation. To utilise this functionality your DevicePointer types
should provide explicit tagged Iterator types. Construct a specialisation of the panda::DeviceIterator for each iterator type
to allow it to integrate with panda. The DeviceIterator class is designed to be usead as a simple wrapper around an existing iterator 
in order to add a compile time architecture tag so panda can deduce which specialisations to call. It can also be used
to actually implemnt the iterator if you prefer.

~~~~~{.cpp}
#include "panda/DeviceIterator.h"

namespace ska {
namespace panda {

template<typnemae DataType>
class DeviceIterator<example_device::ExampleDevice, DataType> 
    : public std::iterator<std::random_access_iterator_tag,
                           DataType,
                           std::size_t
                          >
{
    // implement operations for iterator supporting std::random_access_iterator_tag
};

} // namespace panda
} // namespace ska
~~~~~

## Extending panda::Copy
pands::Copy is designed as an extension to std::copy that understands about specific accelerator devices. It is a consistent and
easy to use interface for memory transfer that hides all the intricate details of how its done under the hood.

With the DeviceIterator above defined for our architecture tag, integration with panda::Copy is a case of providing a
specialisations of the panda::DeviceCopy template class. You can assume that the host iterators represent contiguous memory.

~~~~~{.cpp}
#include "panda/DeviceIterator.h"

namespace ska {
namespace panda {

template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
struct DeviceCopy<Cpu, example_device::ExampleDevice, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>
{
    public:
        // perform the copy from host to ExampleDevice architectures
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static inline typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it);

};

template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
struct DeviceCopy<example_device::ExampleDevice, Cpu, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>
{
    public:
         // perform the copy from ExampleDevice architectures to host
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static inline typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it);

};

} // namespace panda
} // namespace ska
~~~~~

If this provides insufficinet flexibility you can provide specialisations for panda::Copy function itself, although you may miss
out on some optimisations as a result. If you do this you cannot assume contiguous memory of the iterators representing host memory.

## The Main API header
Last but not least, create a header file having a PascalCase version of the device_type you used for the arch directory.
This convention means that anyone wishing to use the new plugin can include a single .h file without having to hunt around.

