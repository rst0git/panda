/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "panda/AggregationBufferFiller.h"
#include "panda/Error.h"
#include <iterator>

namespace ska {
namespace panda {

template<typename DataT, typename BufferFactory>
template<typename... Args>
AggregationBufferFiller<DataT, BufferFactory>::AggregationBufferFiller(FullBufferHandlerT listener, Args&& ... args)
    : _buffer_factory(std::forward<Args>(args)...)
    , _fn(listener)
    , _overlap(0)
    , _current(_buffer_factory.create_buffer())
{
    _buffer_size = _buffer_factory.number_of_elements(_buffer_factory.size());
}

template<typename DataT, typename BufferFactory>
AggregationBufferFiller<DataT, BufferFactory>::~AggregationBufferFiller()
{
    if(_current.data_size() != _overlap)
    {
        _fn(std::move(_current));  // push any new held data to the handler
    }
}

template<typename DataT, typename BufferFactory>
bool AggregationBufferFiller<DataT, BufferFactory>::flush()
{
    // prepare a new buffer
    AggregationBufferType tmp(_buffer_factory.create_buffer());
    _current.swap(tmp);

    if( _overlap != 0 )
        tmp.transfer(_overlap, _current);

    // call our full buffer functor
    if(tmp.data_size()) {
        _fn(std::move(tmp));
        return true;
    }
    return false;
}

template<typename DataT, typename BufferFactory>
void AggregationBufferFiller<DataT, BufferFactory>::full_buffer_handler(FullBufferHandlerT const& handler)
{
    _fn = handler;
}

template<typename DataT, typename BufferFactory>
template<typename IteratorType>
void AggregationBufferFiller<DataT, BufferFactory>::_insert( DataType const& data, IteratorType & iter, IteratorType const& end_iter )
{
    auto ptr = data.shared_from_this();
    _current.insert(iter, end_iter, ptr);
    if(_current.remaining_capacity() == 0)
    {
        flush();

        // continue filling new buffer
        if(iter != end_iter) {
            _insert(data, iter, end_iter);
        }
    }
}

template<typename DataT, typename BufferFactory>
template<typename InsertDataType>
void AggregationBufferFiller<DataT, BufferFactory>::insert( InsertDataType const& data )
{
    auto iter = _buffer_factory.begin(data);
    auto end_iter = _buffer_factory.end(data);
    if(iter != end_iter) {
      _insert(static_cast<DataT const&>(data), iter, end_iter);
    }
}

template<typename DataT, typename BufferFactory>
void AggregationBufferFiller<DataT, BufferFactory>::set_overlap(SizeType const& overlap)
{
    _overlap = _buffer_factory.number_of_elements(overlap);

    if( _overlap > _buffer_size ) {
        throw Error("set_overlap() buffer too small for overlap");
    }
}

template<typename DataT, typename BufferFactory>
std::size_t AggregationBufferFiller<DataT, BufferFactory>::overlap() const
{
    return _overlap;
}

template<typename DataT, typename BufferFactory>
template<typename... Args>
void AggregationBufferFiller<DataT, BufferFactory>::resize(Args&& ... args)
{
    _buffer_factory.resize(std::forward<Args>(args)...);
    _buffer_size = _buffer_factory.number_of_elements(_buffer_factory.size());

    if( _overlap > _buffer_size ) {
        throw Error("resize() buffer too small for overlap");
    }
}

template<typename DataT, typename BufferFactory>
template<typename InsertDataType>
AggregationBufferFiller<DataT, BufferFactory>& AggregationBufferFiller<DataT, BufferFactory>::operator<<( InsertDataType const& data )
{
    insert(data);
    return *this;
}

template<typename DataT, typename BufferFactory>
std::size_t AggregationBufferFiller<DataT, BufferFactory>::size() const
{
    return _buffer_size;
}

template<typename DataT, typename BufferFactory>
std::size_t AggregationBufferFiller<DataT, BufferFactory>::remaining_capacity() const
{
    return _current.remaining_capacity();
}

template<typename DataType>
auto
BeginEndFactory<DataType>::begin(DataType const& data) -> decltype(data.begin())
{
    return data.begin();
}

template<typename DataType>
auto
BeginEndFactory<DataType>::end(DataType const& data) -> decltype(data.end())
{
    return data.end();
}

} // namespace panda
} // namespace ska
