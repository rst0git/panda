/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "CsvFileProducer.h"
#include "panda/Log.h"
#include <boost/algorithm/string/trim.hpp>

namespace ska {
namespace panda {
namespace examples {


template<class DerivedType>
CsvFileProducerCRTP<DerivedType>::CsvFileProducerCRTP(FileNameContainer const& file_names)
    : _file_names(file_names)               // make a local copy of the file names
    , _file_name_it(_file_names.cbegin())   // start with the first file
{
}

template<class DerivedType>
CsvFileProducerCRTP<DerivedType>::~CsvFileProducerCRTP()
{
}

template<class DerivedType>
void CsvFileProducerCRTP<DerivedType>::init()
{
    next_file();
}

template<class DerivedType>
bool CsvFileProducerCRTP<DerivedType>::next_file()
{
    // get the headers data structure to fill from the DataManager
    std::shared_ptr<Headers> headers=this->template get_chunk<Headers>();

    // now fill this structure from the next open file
    return read_headers(*headers);  

    // the headers object will go out of scope at the end of this function 
    // which will allow it to propagate to the DataManager as a filled data set
}

template<class DerivedType>
bool CsvFileProducerCRTP<DerivedType>::read_headers(Headers& headers)
{
    if(_file_name_it == _file_names.cend()) return false; // all out of files

    // open the file
    _file_stream.exceptions(); // turn off exceptions as we deal with erroes here
    _file_stream.clear();     // clean up any state from the previous files operations

    _file_stream.open(*_file_name_it);
    if(!_file_stream.is_open()) {
        PANDA_LOG_WARN << "could not open file '" << *_file_name_it << "'";
        return false;
    }
    _file_stream.exceptions(std::ifstream::failbit | std::ifstream::badbit); // ensure errors cause an exception elsewhere 

    try { // any problems in this block we clean up and try the next file
        std::string line; 
        std::getline(_file_stream, line);
        std::size_t pos, last_pos = 0;
        while((pos = line.find_first_of(',', last_pos)) != std::string::npos) {
            std::string token=line.substr(last_pos, pos - last_pos);
            boost::algorithm::trim(token);
            headers.emplace_back(token);
            last_pos = pos + 1;
        }
    }
    catch (std::exception const& e) {
        PANDA_LOG_ERROR << "problem reading file '" << *_file_name_it << "' :" << e.what();
        ++_file_name_it;
        return read_headers(headers);
    }
    catch (...) {
        PANDA_LOG_ERROR << "problem reading file: '" << *_file_name_it << "'";
        ++_file_name_it;
        return read_headers(headers);
    }

    // prep for the next call
    ++_file_name_it;
    return true;
}

template<class DerivedType>
bool CsvFileProducerCRTP<DerivedType>::process()
{
    // make sure we have a file to read from
    if(!_file_stream.is_open() || _file_stream.eof() || _file_stream.peek() == decltype(_file_stream)::traits_type::eof()) {
        if(!next_file()) return true; // tell the DataManager not to expect any more data
    }

    // ask the DataManager for a data structure to fill
    std::shared_ptr<Data> data = this->template get_chunk<Data>();

    // read in the CSV from the next line of the file into data
    std::string line;
    std::getline(_file_stream, line);
    std::size_t pos, last_pos = 0;
    while((pos = line.find_first_of(',', last_pos)) != std::string::npos) {
        std::string const token=line.substr(last_pos, pos - last_pos);
        data->emplace_back(std::stoi(token));
        last_pos = pos + 1;
    }
    return false;

    // data goes out of scope here, propagating to the DataManager
}

template<class DerivedType>
void CsvFileProducerCRTP<DerivedType>::stop()
{
}

} // examples
} // namespace panda
} // namespace ska
