/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/TypeTraits.h"


namespace ska {
namespace panda {

namespace {

    template<typename CallBack, typename... Ts>
    struct SelectMethodHelper;

    template <typename CallBack, typename... TupleParams, typename T, typename... Ts>
    struct SelectMethodHelper<CallBack, std::tuple<TupleParams...>, SelectMethod::Select<T>, Ts...>
    {
        static inline
        bool exec(CallBack& callback, SelectMethod::Select<T> const& t1, Ts const&... args)
        {
            if(t1()) {
                return SelectMethodHelper<CallBack, std::tuple<TupleParams..., T>, Ts...>::exec(callback, args...);
            }
            else {
                return SelectMethodHelper<CallBack, std::tuple<TupleParams...>, Ts...>::exec(callback, args...);
            }
        };
    };

    template<typename CallBack, typename...TupleParams>
    struct SelectMethodHelper<CallBack, std::tuple<TupleParams...>>
    {
        static inline
        bool exec(CallBack& callback)
        {
            callback.template exec<TupleParams...>();
            return true;
        }
    };

    template<typename CallBack>
    struct SelectMethodHelper<CallBack, std::tuple<>>
    {
        static inline
        bool exec(CallBack& callback)
        {
            callback.exec();
            return false;
        }
    };

} // namespace

template<typename CallBack, typename... Ts>
bool SelectMethod::exec(CallBack& callback, Select<Ts> const&... types)
{
    return SelectMethodHelper<CallBack, std::tuple<>, Select<Ts>...>::exec(callback, types...);
}


} // namespace panda
} // namespace ska
