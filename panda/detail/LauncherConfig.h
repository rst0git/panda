/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_LAUNCHERCONFIG_H
#define SKA_PANDA_LAUNCHERCONFIG_H

#include "panda/ConfigModule.h"
#include <vector>
#include <algorithm>
#include <string>


namespace ska {
namespace panda {
namespace detail {

template<typename ConfigT, typename SinkConfig, typename SourceConfig, typename ComputeFactory, typename Sink>
class LauncherConfig : public ConfigT
{
        typedef ConfigT BaseT;

    public:
        LauncherConfig();

        /**
         * @brief returns true if info options have been executed
         */
        bool info_options_run() const;

        template<typename ContainerType>
        void source_options(ContainerType const& cont)
        {
            std::copy(cont.begin(), cont.end(), std::back_inserter(_source_options));
        }

        template<typename ContainerType>
        void compute_options(ContainerType const& cont)
        {
            std::copy(cont.begin(), cont.end(), std::back_inserter(_compute_options));
        }

        template<typename ContainerType>
        void sink_options(ContainerType const& cont)
        {
            std::copy(cont.begin(), cont.end(), std::back_inserter(_sink_options));
        }

        std::string const& source_name() const;
        std::string const& compute_name() const;

        // return the sink_configuration object
        SinkConfig const& sink_config() const;

        // return the source configuration object
        SourceConfig const& source_config() const;

    private:
        void add_options(typename BaseT::OptionsDescriptionEasyInit& add_options) override;

    protected:
        SourceConfig _source_config;
        SinkConfig _sink_config;
        std::vector<std::string> _source_options;
        std::vector<std::string> _compute_options;
        std::vector<std::string> _sink_options;
        std::string _source;
        std::string _compute;
        bool _info_run;
};

template<typename ConfigT, typename SinkConfig, typename SourceConfig, typename ComputeFactory, typename Sink>
LauncherConfig<ConfigT, SinkConfig, SourceConfig, ComputeFactory, Sink>::LauncherConfig()
    : _info_run(false)
{
    BaseT::add(_source_config);
    BaseT::add(_sink_config);
}

template<typename ConfigT, typename SinkConfig, typename SourceConfig, typename ComputeFactory, typename Sink>
SourceConfig const& LauncherConfig<ConfigT, SinkConfig, SourceConfig, ComputeFactory, Sink>::source_config() const
{
   return _source_config;
}

template<typename ConfigT, typename SinkConfig, typename SourceConfig, typename ComputeFactory, typename Sink>
SinkConfig const& LauncherConfig<ConfigT, SinkConfig, SourceConfig, ComputeFactory, Sink>::sink_config() const
{
   return _sink_config;
}

template<typename ConfigT, typename SinkConfig, typename SourceConfig, typename ComputeFactory, typename Sink>
bool LauncherConfig<ConfigT, SinkConfig, SourceConfig, ComputeFactory, Sink>::info_options_run() const
{
    return _info_run;
}

template<typename ConfigT, typename SinkConfig, typename SourceConfig, typename ComputeFactory, typename Sink>
std::string const& LauncherConfig<ConfigT, SinkConfig, SourceConfig, ComputeFactory, Sink>::source_name() const
{
   return _source;
}

template<typename ConfigT, typename SinkConfig, typename SourceConfig, typename ComputeFactory, typename Sink>
std::string const& LauncherConfig<ConfigT, SinkConfig, SourceConfig, ComputeFactory, Sink>::compute_name() const
{
   return _compute;
}

template<typename ConfigT, typename SinkConfig, typename SourceConfig, typename ComputeFactory, typename Sink>
void LauncherConfig<ConfigT, SinkConfig, SourceConfig, ComputeFactory, Sink>::add_options(typename BaseT::OptionsDescriptionEasyInit& add_options)
{
    BaseT::add_options(add_options);
    add_options
      ("list-sinks", boost::program_options::bool_switch()->notifier(
                [&](bool active) {
                    if(!active) return;
                    Sink sink(sink_config());
                    sink_options(sink.available());
                    for( auto const& s : _sink_options ) {
                        std::cout << s << "\n";
                    }
                    _info_run = true;
                })
        , "list the available data sink types and exit")
      ("list-sources", boost::program_options::bool_switch()->notifier(
                [&](bool active) {
                    if(!active) return;
                    for( auto const& s : _source_options ) {
                        std::cout << s << "\n";
                    }
                    _info_run = true;
                })
        , "list the available data sources for the pipeline and exit")
      ("list-pipelines", boost::program_options::bool_switch()->notifier(
                [&](bool active) {
                    if(!active) return;
                    Sink sink(sink_config());
                    ComputeFactory cf(*this, sink);
                    compute_options(cf.available());
                    for( auto const& c : _compute_options) {
                        std::cout << c << "\n";
                    }
                    _info_run = true;
                })
        ,"list the available computational pipelines and exit")
      ("pipeline,p", boost::program_options::value<std::string>(&_compute), "specify the computational pipeline to run")
      ("source,s", boost::program_options::value<std::string>(&_source), "select the data source");
}

} // namespace detail
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_LAUNCHERCONFIG_H
