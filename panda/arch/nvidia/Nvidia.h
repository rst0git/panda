/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_NVIDIA_NVIDIA_H
#define SKA_PANDA_NVIDIA_NVIDIA_H

#include "panda/Configuration.h"
#include "panda/DiscoverResources.h"
#include "panda/arch/nvidia/PoolResource.h"
#include "panda/arch/nvidia/DeviceConfig.h"
#include <memory>

/**
 * @brief
 *    Nvidia accelerator defintions and discovery
 */

namespace ska {
namespace panda {

// specialisation for NVidia Cuda resource discovery
template<>
struct DiscoverResources<nvidia::Cuda> {
    std::vector<std::shared_ptr<PoolResource<nvidia::Cuda>>> operator()() {
        std::vector<std::shared_ptr<PoolResource<nvidia::Cuda>>> res;
        int num_devices=0;
#ifdef SKA_PANDA_ENABLE_CUDA
        cudaGetDeviceCount(&num_devices);
#endif // SKA_PANDA_ENABLE_CUDA
        for(int i = 0; i < num_devices; ++i) {
           res.emplace_back(std::make_shared<PoolResource<nvidia::Cuda>>(i));
        }
        return res;
    };
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_NVIDIA_NVIDIA_H
