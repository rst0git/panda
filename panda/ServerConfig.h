/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_SERVERCONFIG_H
#define SKA_PANDA_SERVERCONFIG_H


#include "panda/ConfigModule.h"
#include "panda/IpAddress.h"
#include "panda/ProcessingEngine.h"
#include <string>

namespace ska {
namespace panda {

/**
 * @brief
 *    Configuration options for the panda Server
 * @details
 * 
 */
class ServerConfig : public ConfigModule
{
    public:
        ServerConfig( std::string const& name_tag="server");
        ~ServerConfig();

        /**
         * @brief the address on which the server should listen for connections
         */
        IpAddress const& listen_address() const;

        /**
         * @brief set the number of threads to be used by the servers main event loop (engine)
         */
        void number_of_threads(unsigned num);

        /**
         * @brief the engine to power the server
         */
        Engine& engine() const;

    protected:
        virtual void add_options(OptionsDescriptionEasyInit& add_options) override;

    protected:
        IpAddress _listen_address;

    private:
        mutable std::unique_ptr<ProcessingEngine>  _engine;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_SERVERCONFIG_H 
