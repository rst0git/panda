@page build PANDA Build Guide
Panda uses cmake to generate the makefiles (or configuration files for other build tools) for your required tool chain.
One of cmake's primary jobs is to locate the software dependencies required to build panda on your system.
Cmake knows to look in the standard places (e.g. paths relative to "/usr/local", "/usr", "/opt") for these dependencies
but you can always explicitly tell cmake where some dependency is via command line options.

# Build Instructions
First create a directory for your build (you can have more than one, for each type of build such as optimised or debug, with or without accelerator support built in etc).
Then use cmake with suitable options to generate the build you require.

e.g. to build an optimised version of panda with CUDA support:

~~~~{bash}
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_CUDA=true <path_to_panda_src>
~~~~

If all is well cmake will generate the necessary files for your build system (by default on linux systems this will be makefiles compatible with gnu make).
Once the files are generated you can use your build tool (gnu ```make``` on linux) to build the libraries and executables
e.g.
~~~~{bash}
make -j
make install
~~~~

## some cmake options

| cmake option | Description
|--------------|-------------
| -DBOOST_ROOT=<path_to_boost>        | location of your boost version (see /usr/share/cmake/Modules/FindBoost.cmake for more options)
| -DCMAKE_INSTALL_PREFIX              | Root directory used to install files when calling 'make install'. The default for this is usually /usr/local.
| -DCMAKE_BUILD_TYPE={debug\|release\|profile) |  The type of build
| -DENABLE_CUDA=true                  | enable support for CUDA based GPUs
| -DENABLE_OPENCL=true                | use the intel opencl library for intel FPGA support
| -DENABLE_DOC=true                | add a doc target to build the documentation (see @ref publishing "publishing" section for more details)

If you are having trouble with some specific package, the cmake modules can be checked for documentation.
They will reside in either panda's cmake directory, or the default
cmake modules can be found in /usr/share/cmake/Modules.
