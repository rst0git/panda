#include "CommonDefs.h"
#include "panda/ConfigModule.h"
#include "panda/PoolSelector.h"
#include <string>

namespace foobar {

class FooConfig : public ska::panda::PoolSelector<PoolManagerType, ska::panda::ConfigModule>
{
        typedef ska::panda::PoolSelector<PoolManagerType, ska::panda::ConfigModule> BaseT;

    public:
        FooConfig(PoolManagerType& pool_manager)
            : BaseT(pool_manager, "foo")
            , _prefix("Hello from foo")
        {
        }

        std::string const& prefix() const
        {
            return _prefix;
        }

    protected:
        void add_options(ska::panda::ConfigModule::OptionsDescriptionEasyInit& add_options) override
        {
            add_options
            ("prefix", boost::program_options::value<std::string>(&_prefix)->default_value(_prefix), "msg to prefix to the foo string");
            BaseT::add_options(add_options);
        }

    private:
        std::string _prefix;
};

} // namespace foobar
