/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PACKETSTREAMIMPLINIT_H
#define SKA_PANDA_PACKETSTREAMIMPLINIT_H


namespace ska {
namespace panda {

/**
 * @brief class to start up a contextualised stream
 * @details by having this in a seprate class we can avoid
 *          having to have an is_initialised check when accessing the context
 *          It is important we only initialise the first context when we have 
 *          a packet, as information from the packet is required to initialise
 *          it.
 */
template<typename ParentType>
class PacketStreamImplInit
{
        typedef typename ParentType::KeepAliveContextPtr KeepAliveContextPtr;
        typedef typename ParentType::PacketType PacketType;
        typedef typename ParentType::BufferType BufferType;
        typedef typename ParentType::ResourceBufferType ResourceBufferType;

    public:
        PacketStreamImplInit(ParentType& parent);
        ~PacketStreamImplInit();

        void start_chunk(std::shared_ptr<ParentType> parent, ResourceBufferType buffer);

    private:
        void start_chunk(KeepAliveContextPtr);

        void get_packet( KeepAliveContextPtr parent
                    , panda::Error const& ec
                    , std::size_t bytes_read
                    , std::size_t total_bytes_read
                    , BufferType buffer
                    );

        void process_packet(KeepAliveContextPtr keep_alive);

    private:
        ParentType& _parent;
};

} // namespace panda
} // namespace ska
#include "PacketStreamImplInit.cpp"

#endif // SKA_PANDA_PACKETSTREAMIMPLINIT_H 
