/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ArchitectureUtilities.h"
#include "panda/ResourceUtilities.h"
#include "panda/Resource.h"
#include "panda/TupleUtilities.h"
#include <type_traits>

namespace ska {
namespace panda {

/// General case returns false
template <class Required, class ResourceType, class Enable>
bool ResourceSupports<Required, ResourceType, Enable>::compatible( PoolResource<ResourceType> const& ) {
    return false;
}

/// Provide a std::true type to always return true
template <class ResourceType, class Enable>
bool ResourceSupports<std::true_type, ResourceType, Enable>::compatible( PoolResource<ResourceType> const& ) {
    return true;
}

/// Special case if types are the same or resource is sub-type of required
template <class Required, class ResourceType>
struct ResourceSupports<Required, ResourceType,
                        typename std::enable_if<std::is_same<Required, ResourceType>::value ||
                                                std::is_base_of<Required, ResourceType>::value>::type> {
    static bool compatible( PoolResource<ResourceType> const& ) {
        return true;
    }
};


/// Specialisation for checking against a minimum capability
template<class Capability, class ResourceType>
struct ResourceSupports<MinimumVersion<Capability>, ResourceType> {
    static bool compatible( PoolResource<ResourceType> const& r) {
        return r >= Capability();
    }
};

/// Specialisation for checking against a maximum capability
template<class Capability, class ResourceType>
struct ResourceSupports<MaximumVersion<Capability>, ResourceType> {
    static bool compatible( PoolResource<ResourceType> const& r) {
        return r <= Capability();
    }
};

/// Specialisation for checking against a general capability range
template<class CapabilityLower, class CapabilityUpper, class ResourceType>
struct ResourceSupports<Range<CapabilityLower, CapabilityUpper>, ResourceType> {
    static bool compatible( PoolResource<ResourceType> const& r ) {
      return ResourceSupports<MinimumVersion<CapabilityLower>, ResourceType>::compatible(r) &&
          ResourceSupports<MaximumVersion<CapabilityUpper>, ResourceType>::compatible(r);
    }
};

/// specialisation for checking against a tuple of requirements
template<class ResourceType, class... Requirements>
struct ResourceSupports<Version<Requirements...>, ResourceType> {

    template<typename T>
    struct CompatabilityCheck {
        inline void operator()(PoolResource<ResourceType> const& r, bool &result) {
          // shortcut if we have already set result to true
          if(result) return;
          else result = ResourceSupports<T, ResourceType>::compatible(r);
        }
    };

    static bool compatible( PoolResource<ResourceType> const& r ) {
        typedef std::tuple<Requirements...> RequirementsList;
        // Start at false and find first thing to set this to true (if any)
        bool result(false);
        panda::ForEach<RequirementsList, CompatabilityCheck>::exec(r, result);
        return result;
    }
};

}
}
