# Force all include files to be prefixed with the project name
include_directories(${PROJECT_SOURCE_DIR} ${PROJECT_BINARY_DIR})
include_directories(SYSTEM ${BOOST_INCLUDE_DIRS})

# Project library
set(lib_src
    src/Architecture.cpp
    src/Abort.cpp
    src/BasicAppConfig.cpp
    src/Buffer.cpp
    src/CaseInsensitiveString.cpp
    src/ChannelInfo.cpp
    src/ChannelsConfig.cpp
    src/ConfigActive.cpp
    src/ConfigFile.cpp
    src/ConfigModule.cpp
    src/ConfigModuleIteratorSet.cpp
    src/ChannelId.cpp
    src/Cpu.cpp
    src/CumulativeDelayErrorPolicy.cpp
    src/DataSwitchConfig.cpp
    src/EndpointConfig.cpp
    src/Error.cpp
    src/Engine.cpp
    src/FactoryBase.cpp
    src/FileSearchPath.cpp
    src/IpAddress.cpp
    src/Log.cpp
    src/LogLine.cpp
    src/LogLineSentry.cpp
    src/LoggingThread.cpp
    src/OutputFileStreamer.cpp
    src/PassiveErrorPolicy.cpp
    src/PacketStream.cpp
    src/ProcessingEngine.cpp
    src/ProcessingEngineConfig.cpp
    src/Reservation.cpp
    src/Resource.cpp
    src/ResourceJob.cpp
    src/ServerConfig.cpp
    src/ServerSettings.cpp
    src/Thread.cpp
    src/ThreadConfig.cpp
    src/Version.cpp
)

# Architecture plugins
set(PANDA_LIBRARIES panda ${BOOST_LIBRARIES} ${DEPENDENCY_LIBRARIES})
add_subdirectory(arch)

add_library(panda ${lib_src} ${LIB_ARCH_SRC})

install(TARGETS panda DESTINATION ${LIBRARY_INSTALL_DIR})
file(GLOB include_files "${CMAKE_CURRENT_SOURCE_DIR}/*.h")
install(FILES ${include_files} DESTINATION ${INCLUDE_INSTALL_DIR}/panda)
install(DIRECTORY detail DESTINATION ${INCLUDE_INSTALL_DIR}/panda)
install(DIRECTORY concepts DESTINATION ${INCLUDE_INSTALL_DIR}/panda)

# Examples
add_subdirectory(examples)

# Testing
add_subdirectory(test)
