/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "FpgaTest.h"
#include "KernelTest.h"
#include "panda/arch/altera/Altera.h"
#include "panda/arch/altera/Fpga.h"
#include "panda/System.h"
#include "panda/test/gtest.h"
#include "panda/Error.h"
#include "panda/Log.h"


namespace ska {
namespace panda {
namespace test {


FpgaTest::FpgaTest()
{
}

FpgaTest::~FpgaTest()
{
}

void FpgaTest::SetUp()
{
}

void FpgaTest::TearDown()
{
}

TEST_F(FpgaTest, test_fpga)
{
    typedef altera::OpenCl Arch;
    panda::System<Arch> system;
    ASSERT_GT(system.count<Arch>(), std::size_t(0)) << "No devices found to test";
    for(auto const& fpga_device : system.resources<Arch>())
    {
        std::string str = fpga_device->device_name();
        PANDA_LOG << "Device name: " << str ;
        std::string driver_str = fpga_device->driver_version();
        PANDA_LOG << "Driver version: " << driver_str;
        cl_ulong const& mem = fpga_device->device_memory();
        PANDA_LOG << "Device memory: " << mem ;
        ASSERT_FALSE(str.empty());
        cl_ulong m = 0;
        ASSERT_GT(mem,m);
    }
}

} // namespace test
} // namespace panda
} // namespace ska
