/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_MIXINTIMER_H
#define SKA_PANDA_MIXINTIMER_H

#include "panda/TimeDurationFormatter.h"
#include "panda/MixInTimerTraitsConcept.h"

#include <chrono>
#include <type_traits>

namespace ska {
namespace panda {

/*
 * helper struct to determine if type T has an operator()()
 * Note this is all static type generation and has no implementation
 * value is set to true or false appropriately for th type T
 */
template<typename T>
struct has_operator_no_args
{
    #pragma GCC diagnostic push
    #ifdef __GNUC__
    #    pragma GCC diagnostic ignored "-Wpragmas"
    #    pragma GCC diagnostic ignored "-Wunused-comparison"
    #endif

    template<typename U> 
    static auto test(int) -> decltype(std::declval<U>()() == 1, std::true_type());

    #pragma GCC diagnostic pop

    template<typename U> 
    static std::false_type test(...);

    static constexpr bool value = std::is_same<decltype(test<T>(0)),std::true_type>::value;
};

/**
 * @brief
 *    A MixIn that times an underlying call to operator()(), or operator()(args...)  of the type T
 *    Timing information is passed on to the Handler
 *
 * @details
 *    Modifiable Template paramters MixInTimerTraits which specify:
 *       ClockType - use a different clock than the default high_resolution_clock. ClockType must support the now() function
 *       Handler   - The handler of timing information. The handler will be passed the start and end times of the operator. The default handler echos the elapsed time to std::out
 *
 * example 1
 * @code
 * struct MyClass {
 *     void operator()();
 * };
 *
 * MixInTimer<MyClass> my_timed_class;
 * my_timed_class(); // MyClass::operator() will now be timed
 * @endcode
 *
 * example 2
 * @code
 * struct MyClass {
 *     double operator()(int);
 * };
 *
 * MixInTimer<MyClass> my_timed_class;
 * double result = my_timed_class(99); // MyClass::operator(int) will now be timed
 * @endcode
 * \ingroup config
 * @{
 */

template<typename ClockT=std::chrono::high_resolution_clock, typename HandlerT=TimeDurationFormatter<ClockT> >
struct MixInTimerTraits {
    typedef ClockT ClockType;
    typedef HandlerT HandlerType;
};

template<typename T, typename Traits=MixInTimerTraits<std::chrono::high_resolution_clock, TimeDurationFormatter<std::chrono::high_resolution_clock>>, typename Enable=void*>
class MixInTimer : public T
{
    public:
        typedef typename Traits::HandlerType Handler;
        typedef typename Traits::ClockType Clock;

    public:
        // constructors
        MixInTimer(Handler const& f = Handler());
        MixInTimer(Handler f);
        template<typename... Args> MixInTimer(Handler const& handler, Args&&... args);
        template<typename... Args> MixInTimer(Args&&... args);

        virtual ~MixInTimer();

        /*
         * generate wrapper around any T::operator() with arguments if it exists
         */
        template<typename... Args>
        auto operator()(Args&&... args) -> decltype( std::declval<T>()(args...) );

    protected:
        Handler _output_handler;
};

/*
 * specialisation for operator() 
 */
template<typename T, typename MixInTimerTraitsType>
class MixInTimer<T, MixInTimerTraitsType, typename std::enable_if<has_operator_no_args<T>::value>::type>
 : public MixInTimer<T, MixInTimerTraitsType, void*>
{
        using MixInTimer<T, MixInTimerTraitsType, void*>::_output_handler;
        typedef typename MixInTimerTraitsType::HandlerT Handler;

    public:
        MixInTimer(Handler const& f = Handler()) : MixInTimer<T, MixInTimerTraitsType, void>(f) {}
        ~MixInTimer();

        auto operator()() -> decltype( std::declval<T>()() );
};

} // namespace panda
} // namespace ska
#include "panda/detail/MixInTimer.cpp"

#endif // SKA_PANDA_MIXINTIMER_H 
/**
 *  @}
 *  End Document Group
 **/
