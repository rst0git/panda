/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestResourcePool.h"


namespace ska {
namespace panda {
namespace test {


template<typename... ArchitecturesT>
TestResourcePool<ArchitecturesT...>::TestResourcePool()
    : _engine(std::make_shared<panda::ProcessingEngine>(_config))
    , _pool(*_engine)
{
}

template<typename... ArchitecturesT>
TestResourcePool<ArchitecturesT...>::TestResourcePool(panda::ProcessingEngine&)
    : TestResourcePool()
{
}

template<typename... ArchitecturesT>
TestResourcePool<ArchitecturesT...>::TestResourcePool(TestResourcePool&& pool)
    : _config(pool._config)
    , _engine(std::move(pool._engine))
    , _pool(std::move(pool._pool))
{
}

template<typename... ArchitecturesT>
TestResourcePool<ArchitecturesT...>::~TestResourcePool()
{
}

template<typename... ArchitecturesT>
template<unsigned Priority, typename TaskTraits, typename... DataTypes>
std::shared_ptr<ResourceJob> TestResourcePool<ArchitecturesT...>::submit(TaskTraits&& traits, DataTypes&&... data)
{
    return _pool.template submit<Priority>(std::forward<TaskTraits>(traits), std::forward<DataTypes>(data)...);
}

template<typename... ArchitecturesT>
template<typename TaskTraits, typename... DataTypes> inline
std::shared_ptr<ResourceJob> TestResourcePool<ArchitecturesT...>::submit(TaskTraits& traits, DataTypes&&... types) const
{
    return _pool.submit(traits, std::forward<DataTypes>(types)...);
}

template<typename... ArchitecturesT>
template<typename Arch>
void TestResourcePool<ArchitecturesT...>::add_resource(std::shared_ptr<Resource> r)
{
    _pool.template add_resource<Arch>(std::move(r));
}

template<typename... ArchitecturesT>
void TestResourcePool<ArchitecturesT...>::wait() const
{
    _pool.wait();
}

template<typename... ArchitecturesT>
template<typename Arch>
unsigned TestResourcePool<ArchitecturesT...>::free_resources_count() const {
    return _pool.template free_resources_count<Arch>();
}

template<typename... ArchitecturesT>
template<typename Arch>
std::deque<std::shared_ptr<PoolResource<Arch>>> const& TestResourcePool<ArchitecturesT...>::free_resources() const
{
    return _pool.template free_resources<Arch>();
}

} // namespace test
} // namespace panda
} // namespace ska
