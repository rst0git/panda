/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_LOGLINE_H
#define SKA_PANDA_LOGLINE_H

#include <sstream>
#include <iostream>
#include <ostream>
#include <thread>
#include <mutex>


namespace ska {
namespace panda {
class Logger;
class LogLineHolder;

/**
 * @brief
 *    storage type for log messages
 * @details
 *
 */

class LogLine
{
        friend class LogLineHolder;

    public:
        typedef std::stringstream Stream;

    public:
        LogLine();
        ~LogLine();

        void reset(Logger&);
        void rewind();

        /**
         * @brief write the message to the stream
         */
        template<typename StreamT>
        void write(StreamT& os) const;

        /**
         * @brief add into the current message
         */
        template<typename T>
        void add(T&& t);

    private:
        bool _active;
        bool _truncated;
        Stream _ss;
};

template<typename T>
void LogLine::add(T&& t) {
    if(_active) {
        try {
            _ss << std::forward<T>(t);
            if(!_ss.good()) {
                _truncated =true;
                _ss.clear();
            }
        } catch(...) {
            _truncated =true;
        }
    }
}

template<typename T>
LogLine& operator<<(LogLine& logline, T&& t)
{
    logline.add(std::forward<T>(t));
    return logline;
}

template<typename StreamT>
void LogLine::write(StreamT& os) const {
    os << _ss.str();
    if(_truncated) {
        os << " [truncated msg]";
    }
}

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_LOGLINE_H
