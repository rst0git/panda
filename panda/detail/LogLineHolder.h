/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_LOGLINEHOLDER_H
#define SKA_PANDA_LOGLINEHOLDER_H
#include "LogLine.h"
#include "LogLineHolderSentry.h"
#include <type_traits>

namespace ska {
namespace panda {

/**
 * @brief
 *     Ensure the logline is locked and release as it goes out of scope
 */
class LogLineHolder
{
    public:
        LogLineHolder();
        LogLineHolder(const char* line, const char* file);
        LogLineHolder(LogLineHolder const&) = delete;
        ~LogLineHolder();

        void lock();
        void unlock();

        bool active() const;

        /**
         * @brief return the location (file and line number) as a string
         */
        std::string const& location() const;

        /**
         * @brief send the LogLine to the current logger for printing
         */
        void send();

        void reset(Logger& l);

        void swap(LogLineHolder&);
        LogLine const& logline() const;
        LogLine& logline();

        /**
         * @brief add into the current message
         */
        template<typename T>
        void add(T&& t);

        /**
         * @brief standard streaming operator for most types
         * @details returna a LogLineHolderSentry which will field all further operator<< calls approprately
         */
        template<typename T>
        LogLineHolderSentry<LogLine::Stream, T>
        operator<<(T&& t);

        /**
         * @brief specialization for const char* to remove an unnessasary  alloc
         */
        LogLineHolder& operator<<(const char* msg);

    private:
        LogLine* _pimpl;
        Logger* _logger;
        const std::string _location;
        std::mutex _mutex;
};

#define PANDA_STATIC_LOGLINE_VARIABLE_NAME(line, file)  _panda_logline_ ## line
#define PANDA_DEFINE_STATIC_LOGLINE_VARIABLE(line, file) static ska::panda::LogLineHolder PANDA_STATIC_LOGLINE_VARIABLE_NAME(line, file) ( #line , file )

} // namespace panda
} // namespace ska
#include "LogLineHolder.cpp"

#endif // SKA_PANDA_LOGLINEHOLDER_H
