/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_SERVERTRAITS_H
#define SKA_PANDA_SERVERTRAITS_H

#include "panda/NetworkTypes.h"
#include "panda/ServerClient.h"
#include <boost/asio/ip/basic_endpoint.hpp>
#include <memory>

namespace ska {
namespace panda {

/**
 * @brief
 * 
 * @details
 * @param SocketType - must support the async_send_to and async_receive_from methods 
 */
template<typename ProtocolType,
         typename SocketType=void>
class ServerTraits
{
    public:
        typedef ServerTraits<ProtocolType, SocketType> TraitsType;
        typedef ProtocolType Protocol;
        typedef SocketType Socket;
        typedef boost::asio::ip::basic_endpoint<Protocol> EndPoint;
        typedef std::shared_ptr<ServerClient<TraitsType>> Session;
        typedef boost::system::error_code Error;
};

template<> 
class ServerTraits<Udp, void> : public ServerTraits<Udp, boost::asio::ip::udp::socket>
{
};

template<> 
class ServerTraits<Tcp, void> : public ServerTraits<Tcp, boost::asio::ip::tcp::socket>
{
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_SERVERTRAITS_H 
