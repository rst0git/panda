/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Thread.h"
#include "panda/Error.h"
#include "panda/Log.h"

#if HAS_PTHREAD_SET_AFFINITY
#include <sched.h>
#include <pthread.h>
#include <cstring>
#endif // HAS_PTHREAD_SET_AFFINITY

namespace ska {
namespace panda {

Thread::Thread()
    : _terminate(false)
    , _thread([this]() { wait(); })
{
}

Thread::Thread(std::vector<unsigned> const& affinities)
    : _terminate(false)
    , _thread([this, affinities]() {
                set_core_affinity(affinities);
                wait();
            })
{
}

Thread::Thread(ThreadConfig const& config)
    : _terminate(false)
    , _thread([this, &config]() {
                set_core_affinity(config.affinities());
                wait();
            })
{
}

Thread::~Thread()
{
    join();
}

void Thread::join()
{
    {
        std::lock_guard<std::mutex> lk(_mutex);
        if(_terminate) return;
        _terminate = true;
    }
    _cv.notify_one();
    _thread.join();
}

void Thread::set_core_affinity(std::vector<unsigned> const& affinities)
{
    set_core_affinity(affinities, _thread.native_handle());
}

void Thread::wait()
{
    {
        std::unique_lock<std::mutex> lk(_mutex);
        while(!_terminate) {
            while(!_jobs.empty()) {
                lk.unlock();
                _jobs[0]();
                lk.lock();
                _jobs.pop_front();
            }
            if(_terminate) break;
            _cv.wait(lk);
        }
    }
}

#if HAS_PTHREAD_SET_AFFINITY
void Thread::set_core_affinity(std::vector<unsigned> const& affinities, std::thread::native_handle_type thread_id)
{
    if(affinities.size())
    {
        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        for(auto const& affinity : affinities) {
            CPU_SET(affinity, &cpuset);
        }
        int rc = pthread_setaffinity_np(thread_id, sizeof(cpu_set_t), &cpuset);
        if (rc != 0) throw Error("Thread: Error calling pthread_setaffinity_np: ");
    }
}

bool Thread::has_core_affinity(std::vector<unsigned> const& affinities) const
{
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);

    cpu_set_t cpuset_ref;
    CPU_ZERO(&cpuset_ref);
    for(auto const& affinity : affinities) {
        CPU_SET(affinity, &cpuset_ref);
    }

    int rc = pthread_getaffinity_np(const_cast<Thread*>(this)->_thread.native_handle(), sizeof(cpu_set_t), &cpuset);
    if (rc != 0) {
        Error e("Thread: Error calling pthread_getaffinity_np: "); e << std::strerror(rc);
        throw e;
    }
    //return (bool)(CPU_ISSET(&cpuset_ref,&cpuset));
    return CPU_EQUAL(&cpuset_ref, &cpuset);
}

#else
void Thread::set_core_affinity(std::vector<unsigned> const&, std::thread::native_handle_type)
{
    // do nothing if no pthread
    PANDA_LOG_WARN << "unable to set thread affinities - recompile with pthread library to activate this functionality";
}

bool Thread::has_core_affinity(std::vector<unsigned> const&) const
{
    // always claim we are running on the requested core - makes operational and unit tests simpler
    return false;
}
#endif

} // namespace panda
} // namespace ska
