/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ChannelInfoTest.h"
#include "panda/ChannelInfo.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#include <boost/property_tree/ptree.hpp>
#pragma GCC diagnostic pop


namespace ska {
namespace panda {
namespace test {


ChannelInfoTest::ChannelInfoTest()
    : ::testing::Test()
{
}

ChannelInfoTest::~ChannelInfoTest()
{
}

void ChannelInfoTest::SetUp()
{
}

void ChannelInfoTest::TearDown()
{
}

TEST_F(ChannelInfoTest, test_parse_tree)
{
    boost::property_tree::ptree pt_channel;
    boost::property_tree::ptree sink_1;
    sink_1.put<std::string>("id", "sink_1_id");
    boost::property_tree::ptree sink_2;
    sink_2.put<std::string>("id", "sink_2_id");
    boost::property_tree::ptree sink_3;
    pt_channel.add_child("sink", sink_1);
    pt_channel.add_child("sink", sink_2);
    pt_channel.add_child("sink", sink_3); // empty sink should be ignored

    panda::ChannelId id("test_channel_id");
    {
        // test sink nodes
        ChannelInfo ci(id);
        ASSERT_FALSE(ci.active());
        boost::program_options::variables_map vm;
        ci.parse_property_tree(pt_channel, vm);
        boost::program_options::notify(vm);

        ASSERT_EQ(2U, ci.sinks().size());
        ASSERT_EQ("sink_1_id", ci.sinks()[0]);
        ASSERT_FALSE(ci.active());
    }
    {
        // test active node
        pt_channel.put<std::string>("active", "true");
        ChannelInfo ci(id);
        ASSERT_FALSE(ci.active());
        boost::program_options::variables_map vm;
        ci.parse_property_tree(pt_channel, vm);
        boost::program_options::notify(vm);
        ASSERT_TRUE(ci.active());
    }
}

} // namespace test
} // namespace panda
} // namespace ska
