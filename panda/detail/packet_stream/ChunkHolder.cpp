/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "IoFormatting.h"
#include "panda/Engine.h"
#include <cmath>
#include <iostream>

namespace ska {
namespace panda {
namespace packet_stream {

template<typename DerivedType, typename DataTraits, bool is_empty>
typename ChunkHolderBase<DerivedType, DataTraits, is_empty>::LimitType ChunkHolderBase<DerivedType, DataTraits, is_empty>::sequence_number(typename DataTraits::PacketInspector const& packet)
{
    return _data.sequence_number(packet);
}

template<typename DerivedType, typename DataTraits, bool is_empty>
std::size_t ChunkHolderBase<DerivedType, DataTraits, is_empty>::packet_alignment_offset(typename DataTraits::PacketInspector const& packet)
{
    return _data.packet_alignment_offset(packet);
}

template<typename DerivedType, typename DataTraits>
typename ChunkHolderBase<DerivedType, DataTraits, true>::LimitType ChunkHolderBase<DerivedType, DataTraits, true>::sequence_number(typename DataTraits::PacketInspector const& packet)
{
    return DataTraits::sequence_number(packet);
}

template<typename DerivedType, typename DataTraits, bool is_empty>
typename ChunkHolderBase<DerivedType, DataTraits, is_empty>::LimitType ChunkHolderBase<DerivedType, DataTraits, is_empty>::max_sequence_number()
{
    return _data.max_sequence_number();
}

template<typename DerivedType, typename DataTraits>
typename ChunkHolderBase<DerivedType, DataTraits, true>::LimitType ChunkHolderBase<DerivedType, DataTraits, true>::max_sequence_number()
{
    return DataTraits::max_sequence_number();
}

template<typename DataTraits>
ChunkHolder<DataTraits>::ChunkHolder(ChunkHolder& cpy)
    : BaseT(static_cast<BaseT const&>(cpy))
    , _reset_complete(true)
    , _engine(cpy._engine)
    , _packet_size(cpy._packet_size) // ensures first ctx requested will call get_next (initital context has no chunk)
    , _chunk(nullptr)
    , _prev(&cpy)
{
    cpy._next = this;
}

template<typename DataTraits>
template<typename... DataConfigArgs>
ChunkHolder<DataTraits>::ChunkHolder(panda::Engine& engine, DataConfigArgs&&... args)
    : BaseT(std::forward<DataConfigArgs>(args)...)
    , _reset_complete(true)
    , _engine(engine)
    , _packet_size(0) // ensures first ctx requested will call get_next (initital context has no chunk)
    , _chunk(nullptr)
    , _prev(nullptr)
{
}

template<typename DataTraits>
ChunkHolder<DataTraits>::~ChunkHolder()
{
}

template<typename DataTraits>
void ChunkHolder<DataTraits>::prev(ChunkHolder& ch)
{
    _prev = &ch;
    ch._next = this;
}

template<typename DataTraits>
std::mutex& ChunkHolder<DataTraits>::mutex()
{
    return _mutex;
}

template<typename DataTraits>
std::shared_ptr<typename ChunkHolder<DataTraits>::DataType> const& ChunkHolder<DataTraits>::chunk()
{
    return _chunk;
}

template<typename DataTraits>
bool ChunkHolder<DataTraits>::reset_complete() const
{
    return _reset_complete;
}

template<typename DataTraits>
typename ChunkHolder<DataTraits>::LimitType ChunkHolder<DataTraits>::offset() const
{
    return _first_offset;
}

template<typename DataTraits>
ChunkHolder<DataTraits>& ChunkHolder<DataTraits>::next()
{
    assert(_next != nullptr);
    return *_next;
}

template<typename DataTraits>
void ChunkHolder<DataTraits>::reset()
{
    // reset variables
    assert(_prev!=nullptr);
    if(!_chunk) return;
    if(_chunk.use_count() > 1 || _prev->_chunk.get() != nullptr || !_prev->_reset_complete)
    {
        // we need to wait until the previous ctx is released
        // to guarantee ordering of chunks
        std::shared_ptr<ChunkHolder> self(this->shared_from_this());
        _engine.post([self, this]()
                     {
                        // we must wait for a lock here. This could potentilly run
                        // before the completion of the reset() call that did the post
                        std::lock_guard<std::mutex> lk(self->_mutex);
                        reset();
                     });
        return;
    }
    _chunk.reset();
    _reset_complete=true; // marker that destructor of chunk has succesfully finished (i.e queued in DataManager)
}

template<typename DataTraits>
void ChunkHolder<DataTraits>::reset(std::shared_ptr<DataType> chunk)
{
    _reset_complete=false;
    _chunk = chunk;
    _packet_size = this->BaseT::packet_size();
    _chunk_size = this->BaseT::chunk_size(*_chunk);
}

template<typename DataTraits>
typename ChunkHolder<DataTraits>::PacketSizeType const& ChunkHolder<DataTraits>::packet_size() const
{
    return _packet_size;
}

template<typename DataTraits>
typename ChunkHolder<DataTraits>::ChunkSizeType ChunkHolder<DataTraits>::chunk_size() const
{
    return _chunk_size;
}

template<typename DataTraits>
void ChunkHolder<DataTraits>::debug_string(std::ostream& os) const
{
    os << "\n    " << "| prev(" << _prev << ")        <- chunk_holder(" << this << ") | next(" << _next << ")\n";
    row_output(os, "chunk", _prev->_chunk.get()?(void*)(_prev->_chunk.get()):(void*)0
                          , (this->_chunk?(void*)(_chunk.get()):(void*)0)
                          , (_next->_chunk?(void*)(_next->_chunk.get()):(void*)0));
    row_output(os, "reset_compl", _prev->_reset_complete, this->_reset_complete, _next->_reset_complete);;
}

template<typename DataTraits>
std::ostream& operator<<(std::ostream& os, ChunkHolder<DataTraits> const& ch)
{
    ch.debug_string(os);
    return os;
}

} // namespace packet_stream
} // namespace panda
} // namespace ska
