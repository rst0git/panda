/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "AggregationBufferTest.h"
#include "panda/AggregationBuffer.h"
#include "panda/Error.h"
#include "panda/test/TestChunk.h"


namespace ska {
namespace panda {
namespace test {

typedef ska::panda::AggregationBuffer<TestChunk<char>, panda::Buffer<char>> BufferT;

AggregationBufferTest::AggregationBufferTest()
    : ::testing::Test()
{
}

AggregationBufferTest::~AggregationBufferTest()
{
}

void AggregationBufferTest::SetUp()
{
}

void AggregationBufferTest::TearDown()
{
}

// geneates a TestChunk filled with sequentialy incrementing chars
static
TestChunk<char> get_chunk(std::size_t size, unsigned index = 0)
{
    // data
    std::string data;
    for(unsigned i=0; i< size; ++i)
    {
        data.push_back(char(((index * size) + i)%255));
    }

    TestChunk<char> chunk(data);
    return chunk;
}

TEST_F(AggregationBufferTest, empty)
{
    std::size_t max_size = 10;
    BufferT buffer(max_size);
    ASSERT_EQ(std::size_t(0), buffer.data_size());
    ASSERT_EQ(std::size_t(10), buffer.capacity());
    ASSERT_EQ(std::size_t(10), buffer.remaining_capacity());

    // Ensure no objects are recored
    auto const& chunks = buffer.composition();
    ASSERT_EQ(unsigned(0), chunks.size());
}

TEST_F(AggregationBufferTest, test_add_chunk_too_big)
{
    /**
     * @details request for an iterator that will overfill the buffer
     *          expect the buffer to fill max and leave the iterator pointing at the next data to be consumed
     */
    std::size_t max_size = 10;
    std::shared_ptr<TestChunk<char>> chunk(new TestChunk<char>(get_chunk(max_size + 1)));

    BufferT buffer( max_size );

    const void* data_1 = buffer.data();
    auto it = chunk->begin();
    char end_char = *(static_cast<char*>(buffer.data(max_size))); // test for buffer overflow
    buffer.insert(it, chunk->end(), chunk);
    ASSERT_NE(chunk->begin(),it);
    ASSERT_NE(chunk->end(),it);
    ASSERT_EQ(chunk->begin() + max_size , it);


    // Ensure data that exists
    ASSERT_EQ(data_1, buffer.data());
    ASSERT_EQ(max_size, buffer.data_size());
    ASSERT_EQ(std::size_t(10), buffer.capacity());
    ASSERT_EQ(std::size_t(0), buffer.remaining_capacity());
    ASSERT_EQ(0, std::memcmp(buffer.data(), chunk->data().c_str(), max_size));
    ASSERT_EQ(0, std::memcmp(&end_char, buffer.data(max_size), 1));

}

TEST_F(AggregationBufferTest, test_add_chunk_size_equal_to_buffer_size)
{
    std::size_t max_size = 10;
    std::shared_ptr<TestChunk<char>> chunk(new TestChunk<char>(get_chunk(max_size)));
    auto it=chunk->begin();

    BufferT buffer( max_size );
    buffer.insert(it, chunk->end(), chunk);
    ASSERT_EQ(max_size, buffer.data_size());
    ASSERT_EQ(std::size_t(10), buffer.capacity());
    ASSERT_EQ(std::size_t(0), buffer.remaining_capacity());
    ASSERT_EQ(std::size_t(0), buffer.offset_first_block());
    ASSERT_EQ(0, std::memcmp(buffer.data(), chunk->data().c_str(), 10));
    ASSERT_EQ(chunk->begin() + max_size, it);
    ASSERT_EQ(chunk->end(),it);
}

TEST_F(AggregationBufferTest, test_composition_retention_on_delete)
{
    BufferT::Composition store;
    unsigned object_count = 3;
    std::size_t chunk_size = 1;

    {
        // Buffer object and objects created here
        BufferT buffer( chunk_size * object_count );
        for(unsigned i=0; i < object_count; ++i) {
            std::shared_ptr<TestChunk<char>> chunk(new TestChunk<char>(get_chunk(chunk_size, object_count)));
            auto it=chunk->begin();
            buffer.insert(it, chunk->end(), chunk);
        }
        ASSERT_EQ(std::size_t(0), buffer.remaining_capacity());
        store = buffer.composition();
    }

    // objects should be made available outside of the creation scope
    ASSERT_EQ(object_count, store.size());
    for(auto const & object : store) {
       ASSERT_FALSE(object.get() == nullptr);
    }
}

TEST_F(AggregationBufferTest, test_transfer_no_data)
{
    std::size_t max_size = 10;
    BufferT buffer(max_size);
    BufferT buffer_2(max_size);

    buffer.transfer(max_size, buffer_2);
    ASSERT_EQ(std::size_t(0), buffer_2.data_size());
    ASSERT_EQ(0U, buffer.offset_first_block());
    ASSERT_EQ(0U, buffer_2.offset_first_block());
}

TEST_F(AggregationBufferTest, test_transfer_max_size_full_buffer)
{
    unsigned object_count = 3;
    std::size_t chunk_size = 2;
    std::size_t max_size = chunk_size * object_count;
    BufferT buffer(max_size);

    // fill the first buffer completely
    for(unsigned i=0; i < object_count; ++i) {
            std::shared_ptr<TestChunk<char>> chunk(new TestChunk<char>(get_chunk(chunk_size, i)));
            auto it=chunk->begin();
            buffer.insert(it, chunk->end(), chunk);
    }

    BufferT buffer_2(max_size);

    buffer.transfer(max_size, buffer_2);
    ASSERT_EQ(0, std::memcmp(buffer.data(), buffer_2.data(), max_size));
    ASSERT_EQ(max_size, buffer_2.data_size());
    ASSERT_EQ(buffer.composition(), buffer_2.composition());
}

TEST_F(AggregationBufferTest, test_transfer_incomplete_chunk)
{
    /*
     * tests the case where the final chunk has overflowed the buffer and so is incomplete
     * expect the corresponding composition() to reflect the actual objects the data refers to
     */
    unsigned object_count = 3;
    std::size_t chunk_size = 5;
    std::size_t max_size = chunk_size * object_count - 2; // buffer 2 bytes short of a whole num chunks

    // prepare buffer with data
    BufferT buffer(max_size);
    for(unsigned i=0; i < object_count; ++i) {
            std::shared_ptr<TestChunk<char>> chunk(new TestChunk<char>(get_chunk(chunk_size, i)));
            auto it=chunk->begin();
            buffer.insert(it, chunk->end(), chunk);
    }
    ASSERT_EQ(object_count, buffer.composition().size());
    ASSERT_EQ(0U, buffer.offset_first_block());

    BufferT buffer_2(max_size);
    buffer.transfer(chunk_size, buffer_2);
    ASSERT_EQ(unsigned(2), buffer_2.composition().size());
    ASSERT_EQ((buffer.composition().rbegin())->get(), (buffer_2.composition().rbegin())->get());
    ASSERT_EQ((++(buffer.composition().rbegin()))->get(), (buffer_2.composition().begin())->get());
    ASSERT_EQ(0U, buffer.offset_first_block());
    ASSERT_EQ(chunk_size - 2U, buffer_2.offset_first_block());
}

TEST_F(AggregationBufferTest, test_transfer_partial_chunk)
{
    unsigned object_count = 3;
    std::size_t chunk_size = 5;
    std::size_t max_size = chunk_size * object_count;

    // prepare buffer with data
    BufferT buffer(max_size);
    for(unsigned i=0; i < object_count; ++i) {
            std::shared_ptr<TestChunk<char>> chunk(new TestChunk<char>(get_chunk(chunk_size, i)));
            auto it=chunk->begin();
            buffer.insert(it, chunk->end(), chunk);
    }

    /**
     * the receiving buffer is of sufficient capacity and the request is for less than a single chunk
     * Expect the requested number of bytes only, and composition to contain the chunk
     */
    BufferT buffer_2(max_size);
    buffer.transfer(chunk_size -1, buffer_2);
    ASSERT_EQ(chunk_size - 1 , buffer_2.data_size());
    ASSERT_EQ(std::string(static_cast<char*>(buffer.data()) + chunk_size * (object_count -1) + 1, chunk_size -1), std::string(static_cast<char*>(buffer_2.data()), buffer_2.data_size()));
    ASSERT_EQ(unsigned(1), buffer_2.composition().size());
    ASSERT_EQ((buffer.composition().rbegin())->get(), (buffer_2.composition().begin())->get());

    /**
     * the receiving buffer is of sufficient capacity and the request is for slightly more than a single chunk
     * Expect the requested number of bytes only, and composition to contain two corresponding chunks
     */
    BufferT buffer_3(max_size);
    buffer.transfer(chunk_size + 1, buffer_3);
    ASSERT_EQ(chunk_size + 1 , buffer_3.data_size());
    ASSERT_EQ(std::string(static_cast<char*>(buffer.data()) + chunk_size * (object_count -1) - 1, chunk_size +1), std::string(static_cast<char*>(buffer_3.data()), buffer_3.data_size()));
    ASSERT_EQ(unsigned(2), buffer_3.composition().size());

    /**
     * the receiving buffer is of sufficient capacity and the request is for slightly more than two chunks
     * Expect the requested number of bytes only, and composition to contain the 3 relevant chunks in the correct order
     */
    BufferT buffer_4(max_size);
    buffer.transfer((2*chunk_size) + 1, buffer_4);
    ASSERT_EQ((2*chunk_size) + 1 , buffer_4.data_size());
    ASSERT_EQ(unsigned(3), buffer_4.composition().size());
    ASSERT_EQ(std::string(static_cast<char*>(buffer.data()) + chunk_size * (object_count -2) - 1, 2*chunk_size + 1), std::string(static_cast<char*>(buffer_4.data()), buffer_4.data_size()));

    /**
     * the receiving buffer is of sufficient capacity and the request is for exactly two chunks
     * Expect the requested number of bytes only, and composition to contain the 2 relevant chunks in the correct order
     */
    BufferT buffer_5(max_size);
    buffer.transfer((2*chunk_size), buffer_5);
    ASSERT_EQ((2*chunk_size) , buffer_5.data_size());
    ASSERT_EQ(unsigned(2), buffer_5.composition().size());
    ASSERT_EQ(std::string(static_cast<char*>(buffer.data()) + chunk_size * (object_count -2), 2*chunk_size), std::string(static_cast<char*>(buffer_5.data()), buffer_5.data_size()));

}

TEST_F(AggregationBufferTest, test_transfer_insufficent_capacity_buffer)
{
    /**
     * the receiving buffer is already partially filled and there is insufficient capacity
     * Execpt to retain existing data and to add only bytes that fill the buffer
     */
    unsigned object_count = 3;
    std::size_t chunk_size = 2;
    std::size_t max_size = chunk_size * object_count;
    BufferT buffer(max_size);

    // fill the first buffer completely
    std::string buffer_1_data;
    for(unsigned i=0; i < object_count; ++i) {
            std::shared_ptr<TestChunk<char>> chunk(new TestChunk<char>(get_chunk(chunk_size, i)));
            auto it=chunk->begin();
            buffer.insert(it, chunk->end(), chunk);
            buffer_1_data = chunk->data(); // we only want the last one
    }

    BufferT buffer_2(max_size);
    std::string buffer_2_data;
    for(unsigned i=0; i < object_count - 1; ++i) {
            std::shared_ptr<TestChunk<char>> chunk(new TestChunk<char>(get_chunk(chunk_size, i + 10)));
            auto it=chunk->begin();
            buffer_2.insert(it, chunk->end(), chunk);
            buffer_2_data += chunk->data();
    }
    auto buffer_2_comp = buffer_2.composition();

    buffer.transfer(max_size, buffer_2);
    ASSERT_EQ(max_size, buffer_2.data_size());
    ASSERT_EQ(buffer_2_data + buffer_1_data, std::string(static_cast<const char*>(buffer_2.data()), max_size ));
    //ASSERT_EQ(buffer_2_comp + buffer.composition().last(), buffer_2.composition());
}

TEST_F(AggregationBufferTest, test_transfer_chunk_fills_multiple_buffers)
{
    std::size_t chunk_size = 11;
    std::size_t max_size = 3;
    BufferT buffer(max_size);

    // fill the first buffer completely
    std::string buffer_1_data;
    std::shared_ptr<TestChunk<char>> chunk(new TestChunk<char>(get_chunk(chunk_size, 0)));
    auto it=chunk->begin();
    buffer.insert(it, chunk->end(), chunk);
    ASSERT_EQ(0U, buffer.offset_first_block());

    { // transfer size = 0
      // we expect no objects and so no offset in the chunks
        BufferT buffer_2(max_size);
        buffer.transfer(0, buffer_2);
        ASSERT_EQ(0U, buffer_2.offset_first_block());

        buffer_2.insert(it, chunk->end(), chunk);

        BufferT buffer_3(max_size);
        buffer_2.transfer(0, buffer_3);
        ASSERT_EQ(0U , buffer_3.offset_first_block());

        buffer_3.insert(it, chunk->end(), chunk);

        BufferT buffer_4(max_size);
        buffer_3.transfer(0, buffer_4);
        ASSERT_EQ(0U , buffer_4.offset_first_block());
    }

    { // transfer size = 1
        BufferT buffer_2(max_size);
        buffer.transfer(1, buffer_2);
        ASSERT_EQ(max_size - 1, buffer_2.offset_first_block());

        buffer_2.insert(it, chunk->end(), chunk);

        BufferT buffer_3(max_size);
        buffer_3.offset_first_block(buffer_2.offset_first_block()); // should not incorporate this info
        buffer_2.transfer(1, buffer_3);
        ASSERT_EQ(max_size - 1, buffer_3.offset_first_block());
    }
}

TEST_F(AggregationBufferTest, test_reset)
{
    unsigned chunk_size=1;
    std::size_t max_size = chunk_size * 4;
    BufferT buffer( max_size );

    // setup a buffer with some data
    for(unsigned i=0; i<3; ++i) {
        std::shared_ptr<TestChunk<char>> chunk(new TestChunk<char>(get_chunk(chunk_size, i)));
        auto it=chunk->begin();
        buffer.insert(it, chunk->end(), chunk);
    }
    ASSERT_EQ(unsigned(3), buffer.composition().size());
    ASSERT_NE(std::size_t(0), buffer.capacity());

    // reset the buffer
    buffer.reset();
    ASSERT_EQ(unsigned(0), buffer.composition().size());
    ASSERT_EQ(std::size_t(0), buffer.data_size());
    ASSERT_EQ(max_size, buffer.remaining_capacity());
    ASSERT_EQ(0U, buffer.offset_first_block());
}

TEST_F(AggregationBufferTest, test_transfer_multiple_partial_inserts)
{
    /*
     * The insert interface has no requirement that full objects be inserted. We verify that
     * the composition returned reflects this and no assumptions about the full data length propogate
     */
    unsigned chunk_size=4;
    std::size_t max_size = chunk_size * 2;
    BufferT buffer( max_size );

    // fill buffer with 3 partial TestChunks
    for(unsigned i=0; i<3; ++i) {
        std::shared_ptr<TestChunk<char>> chunk(new TestChunk<char>(get_chunk(chunk_size, 0)));
        auto it=chunk->begin();
        buffer.insert( it, chunk->end() - i, chunk ); // one less byte per chunk to ensure we test inhomogenous size inserts
    }
    ASSERT_EQ(max_size, buffer.data_size());

    BufferT buffer_2( max_size );
    buffer.transfer(chunk_size * 2, buffer_2); // should pick up all three chunks
    auto buffer_2_comp = buffer_2.composition();
    ASSERT_EQ(chunk_size * 2, buffer_2.data_size());
    ASSERT_EQ(unsigned(3), buffer_2.composition().size());

    BufferT buffer_3( max_size );
    buffer.transfer(chunk_size , buffer_3); // should pick up only 2 chunks
    auto buffer_3_comp = buffer_3.composition();
    ASSERT_EQ(unsigned(2), buffer_3.composition().size());
    ASSERT_EQ(chunk_size, buffer_3.data_size());
}

} // namespace test
} // namespace panda
} // namespace ska
