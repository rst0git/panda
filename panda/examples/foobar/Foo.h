#include "FooConfig.h"
#include "panda/Configuration.h"
#include "panda/arch/nvidia/Nvidia.h"

namespace foobar {

//! [Handler]
// Foo must always pass on its message to some HandlerT
// The HandlerT can be any type but must define a method:
//
// void operator()(std::string const&);
//
template<class HandlerT>
//! [Architectures Spec]
class Foo {
//! [Handler]
    public:
        // With this typedef
        // we tell the panda Resource manager
        // that we can use both CPU threads and
        // NVidia CUDA devices
        typedef std::tuple<ska::panda::Cpu
#ifdef SKA_PANDA_ENABLE_CUDA
                         , ska::panda::nvidia::Cuda
#endif // SKA_PANDA_ENABLE_CUDA
                          > Architectures;

        //! [Architectures Spec]

    public:
        Foo(FooConfig const& config, HandlerT const& handler)
            : _handler(handler)
            , _config(config)
        {}

        //! [Async operator]
        // launches an async task to process msg
        std::shared_ptr<ska::panda::ResourceJob> operator()(std::string const& msg)
        {
            return _config.pool().submit(*this, msg);
        }
        //! [Async operator]

        // these are the sync calls. They will be called by the resource pool with the first
        // resource that becomes available.

        //! [Sync operator]
        // This template method will match any resource
        // from our Architectures definition.
        // In a real module where we would actually want to use the
        // resource provided we would need a sepcialisation for
        // each type.
        template<class ArchT>
        void operator()(ska::panda::PoolResource<ArchT>&, std::string const& msg)
        {
            // we pass on our modfied message to our saved handler
            _handler( msg + " " + _config.prefix() + " with a " + ska::panda::template to_string<ArchT>() + " device");
        }
        //! [Sync operator]

    private:
        HandlerT _handler;
        FooConfig const& _config;
};

} // namespace foobar
