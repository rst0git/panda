/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DEVICEALLOCATOR_H
#define SKA_PANDA_DEVICEALLOCATOR_H
#include "Architecture.h"
#include "Device.h"
#include <memory>

namespace ska {
namespace panda {


/**
 * @brief Allocator for use in stdlib containers
 * @details e.g ```std::vector<float, DeviceAllocator<T, Cuda>>```
 * @tparam T the value type of the container
 * @tparam Arch The architecture tag (see Architecture.h)
 */
template<typename T, typename Arch>
class DeviceAllocator
{
    // specialisations should implement this interface
    public:
        template<typename OtherT>
        struct rebind
        {
            typedef DeviceAllocator<OtherT, Arch> other;
        };
        typedef Arch Architecture;
        typedef T value_type;
        typedef T* pointer;

    public:
        DeviceAllocator(Device<Arch> const&);
        template<typename U>
        DeviceAllocator(DeviceAllocator<U, Cpu> const&);
        ~DeviceAllocator();

        T* allocate(std::size_t n );
        void deallocate( T* p, std::size_t n );

        template<class Ptr, class... Args>
        void construct(Ptr*, Args&&...);
        void destroy( pointer );

};

template<typename T>
class DeviceAllocator<T, Cpu> : public std::allocator<T>
{
    public:
        template<typename OtherT>
        struct rebind
        {
            typedef DeviceAllocator<OtherT, Cpu> other;
        };
        typedef Cpu Architecture;
        typedef T value_type;

    public:
        DeviceAllocator();
        DeviceAllocator(Device<Cpu> const&);
        template<typename U>
        DeviceAllocator(DeviceAllocator<U, Cpu> const&);
        ~DeviceAllocator();
};

} // namespace panda
} // namespace ska
#include "detail/DeviceAllocator.cpp"

#endif // SKA_PANDA_DEVICEALLOCATOR_H
