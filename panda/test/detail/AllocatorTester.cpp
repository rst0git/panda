/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "panda/test/TestDevice.h"
#include <vector>
#include <algorithm>

namespace ska {
namespace panda {
namespace test {


template<typename TestTraits>
AllocatorTester<TestTraits>::AllocatorTester()
{
}

TYPED_TEST_P(AllocatorTester, allocate_deallocate)
{
    typedef TypeParam Traits;
    typedef typename Traits::Allocator Allocator;
    typedef typename Allocator::Architecture Arch;

    typedef panda::test::TestDevice<Arch> TestDevice;
    typedef typename std::allocator_traits<Allocator>::pointer Pointer;

    TestDevice system;
    for(auto& device : system.devices() ) {
        Allocator allocator(*device);

        // allocate single item
        std::size_t n=1;
        Pointer p = allocator.allocate( n );
        //ASSERT_NE(p, nullptr);
        allocator.deallocate(p, n);

        // multiple items
        ++n;
        p = allocator.allocate( n );
        //ASSERT_NE(p, nullptr);
        allocator.deallocate(p, n);
    }
}

TYPED_TEST_P(AllocatorTester, rebind)
{
    typedef TypeParam Traits;
    typedef typename Traits::Allocator Allocator;
    typedef typename Allocator::Architecture Arch;

    typedef panda::test::TestDevice<Arch> TestDevice;
    typedef typename std::allocator_traits<Allocator>::value_type value_type;

    struct T {
        int a;
        float b;
    };

    TestDevice system;
    for(auto& device : system.devices() ) {
        Allocator allocator(*device);
        typedef typename std::allocator_traits<Allocator>::template rebind_alloc<T> ReboundType;
        ReboundType other(allocator);
        typedef typename std::allocator_traits<ReboundType>::template rebind_alloc<value_type> ReReboundType;
        static_assert(std::is_same<Allocator, ReReboundType>::value, "rebind to T and back again should produce the same type");
        ReReboundType allocator_2(other);
        ASSERT_EQ(allocator, allocator_2);
    }
}

TYPED_TEST_P(AllocatorTester, test_vector_of_pod)
{/*
    typedef TypeParam Traits;
    typedef typename Traits::Allocator Allocator;
    typedef typename Allocator::Architecture Arch;

    typedef panda::test::TestDevice<Arch> TestDevice;

    TestDevice system;
    for(auto& device : system.devices() ) {
        Allocator allocator(*device);
        // test allocation
        std::vector<float, Allocator> vector(10, 0, allocator);
        ASSERT_EQ(vector.size(), std::size_t(10));

        // test resize
        vector.resize(5);
        ASSERT_EQ(vector.size(), std::size_t(5));
    }*/
}

REGISTER_TYPED_TEST_CASE_P(AllocatorTester, allocate_deallocate, rebind, test_vector_of_pod);

} // namespace test
} // namespace panda
} // namespace ska
