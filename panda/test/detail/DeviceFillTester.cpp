/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "panda/test/TestDevice.h"
#include "panda/DeviceMemory.h"
#include "panda/Fill.h"
#include <vector>

namespace ska {
namespace panda {
namespace test {


template<typename TestTraits>
DeviceFillTester<TestTraits>::DeviceFillTester()
{
}

TYPED_TEST_P(DeviceFillTester, test_fill)
{
    typedef typename TypeParam::Architecture Arch;
    typedef typename TypeParam::value_type value_type;
    typedef panda::test::TestDevice<Arch> TestDevices;

    TestDevices system;
    std::vector<value_type> vec(10, 0);
    std::vector<value_type> host_vec(10, 0);
    for(auto & device : system.devices()) {
        // setup an initialised vector on the device
        panda::DeviceMemory<Arch, value_type> device_vec(vec.size(), *device);
        panda::copy(vec.begin(), vec.end(), device_vec.begin());

        // perform the fill
        // We do not fill to the end so we can test for overwrites
        panda::fill(device_vec.begin(), device_vec.end() -1, 22);

        // check the memory size is not changed
        ASSERT_EQ(device_vec.size(), vec.size());

        // return the result to host so we can verify them
        panda::copy(device_vec.begin(), device_vec.end(), host_vec.begin());

        for(unsigned i=0; i<host_vec.size()-1; ++i)
        {
            ASSERT_EQ(host_vec[i], 22);
        }

        // ensure no overflow has occured
        ASSERT_EQ(vec.back(), 0);

    }
}

REGISTER_TYPED_TEST_CASE_P(DeviceFillTester, test_fill);

} // namespace test
} // namespace panda
} // namespace ska
