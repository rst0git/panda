/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/detail/Task.h"
#include "panda/CompilerInfo.h"
#include "panda/ResourceUtilities.h"
#include "panda/TupleDispatch.h"
#include "panda/TupleUtilities.h"

#include <functional>
#include <cassert>

namespace ska {
namespace panda {
namespace detail {

// helper struct to extract the Architecture types from a tuple, and create a suitable function to call for each one

#ifdef broken_lambda_parameter_pack_expansion
template<typename TaskClass, typename DataType, typename Arch>
struct broken_param_pack_helper_launch_with_data {
        typename TaskClass::template LauncherFunction<Arch> operator()(TaskClass* ptr) {
        return [ptr](PoolResource<Arch>& r) { ptr->launch_with_data(r, TupleUnpackSequence<DataType>()); };
    }
};

template<typename TaskClass, typename Arch>
struct broken_param_pack_helper_launch {
    typename TaskClass::template LauncherFunction<Arch> operator()(TaskClass* ptr) {
        return [ptr](PoolResource<Arch>& r) { ptr->launch(r); };
    }
};

  template<typename TaskClass, typename Arch, typename Capability>
struct broken_param_pack_helper_compatible {
    typename TaskClass::template CompatabilityFunction<Arch> operator()(TaskClass* ptr) {
      return [ptr](PoolResource<Arch> const& r) -> bool { return ptr->template is_compatible<Capability, Arch>(r); };
    }
};
#endif

template<typename... TupleArgs>
struct expand_tuple {};

template<typename TaskClass, typename DataType, typename... Architectures, typename... Capabilities>
struct expand_tuple<TaskClass, DataType, std::tuple<Architectures...>, std::tuple<Capabilities...>>  {
    typedef typename TaskClass::LauncherLookupType LauncherTuple;
    typedef typename TaskClass::CompatabilityLookupType CompatabilityTuple;
    typedef std::tuple<LauncherTuple, CompatabilityTuple> FunctionTuple;

    static FunctionTuple create(TaskClass* ptr) {
      return std::tuple<LauncherTuple, CompatabilityTuple>(
        std::make_tuple<typename TaskClass::template LauncherFunction<Architectures>...>(
#ifdef broken_lambda_parameter_pack_expansion
           detail::broken_param_pack_helper_launch_with_data<TaskClass, DataType, Architectures>()(ptr)...),
        std::make_tuple<typename TaskClass::template CompatabilityFunction<Architectures>...>(detail::broken_param_pack_helper_compatible<TaskClass, Architectures, Capabilities>()(ptr)...));
#else
      [ptr](PoolResource<Architectures>& r) { ptr->launch_with_data(r, TupleUnpackSequence<DataType>()); }...,
        [ptr](PoolResource<Arch> const& r) -> bool { return ptr->template is_compatible<Capability, Arch>(r); }...);
#endif
    }
};

template<typename TaskClass, typename... Architectures, typename... Capabilities>
struct expand_tuple<TaskClass, std::tuple<>, std::tuple<Architectures...>, std::tuple<Capabilities...>>  {
    typedef typename TaskClass::LauncherLookupType LauncherTuple;
    typedef typename TaskClass::CompatabilityLookupType CompatabilityTuple;
    typedef std::tuple<LauncherTuple, CompatabilityTuple> FunctionTuple;

    static FunctionTuple create(TaskClass* ptr) {
      return std::tuple<LauncherTuple, CompatabilityTuple>(
          std::make_tuple<typename TaskClass::template LauncherFunction<Architectures>...>(
#ifdef broken_lambda_parameter_pack_expansion
              detail::broken_param_pack_helper_launch<TaskClass, Architectures>()(ptr)...),
          std::make_tuple<typename TaskClass::template CompatabilityFunction<Architectures>...>(detail::broken_param_pack_helper_compatible<TaskClass, Architectures, Capabilities>()(ptr)...));
#else
      [ptr](PoolResource<Architectures>& r) { ptr->launch(r); }...,
        [ptr](PoolResource<Arch> const& r) { ptr->is_compatible<Capability, Arch>(r); }...);
#endif
    }
};


template<typename TaskTraits, typename... DataTypes>
template<typename... DataTypeArgs>
Task<TaskTraits, DataTypes...>::Task(TaskTraits& traits, DataTypeArgs&&...data)
  : BaseT( expand_tuple<Task<TaskTraits, DataTypes...>, DataType, Architectures, Capabilities>::create(this) )
    , _tasks(traits)
    , _data(std::forward<DataTypeArgs>(data)...)
{
}

template<typename TaskTraits, typename... DataTypes>
Task<TaskTraits, DataTypes...>::~Task()
{
}

template<typename TaskTraits, typename... DataTypes>
template<typename Capability, typename Arch>
bool Task<TaskTraits, DataTypes...>::is_compatible(PoolResource<Arch> const& r) const {
    return ResourceSupports<Capability, Arch>::compatible(r);
}

template<typename TaskTraits, typename... DataTypes>
template<typename Arch, int... S>
void Task<TaskTraits, DataTypes...>::launch_with_data(PoolResource<Arch>& r, seq<S...> )
{
    _tasks(std::forward<PoolResource<Arch>&>(r), std::move(std::get<S>(_data))...);
}

template<typename TaskTraits, typename... DataTypes>
template<typename Arch>
void Task<TaskTraits, DataTypes...>::launch(PoolResource<Arch>& r)
{
    _tasks(std::forward<PoolResource<Arch>&>(r));
}

template<typename TaskTraits, typename... DataTypes>
template<typename... DataTypeArgs>
Task<std::shared_ptr<TaskTraits>, DataTypes...>::Task(std::shared_ptr<TaskTraits> traits_ptr, DataTypeArgs&&... data)
    : BaseT( *traits_ptr, std::forward<DataTypeArgs>(data)...)
    , _traits_ptr(std::move(traits_ptr))
{
    assert(_traits_ptr);
}

} // namespace detail
} // namespace panda
} // namespace ska
