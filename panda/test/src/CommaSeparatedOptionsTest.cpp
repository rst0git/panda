/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "CommaSeparatedOptionsTest.h"
#include "panda/CommaSeparatedOptions.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <boost/program_options.hpp>
#pragma GCC diagnostic pop


namespace ska {
namespace panda {
namespace test {


CommaSeparatedOptionsTest::CommaSeparatedOptionsTest()
    : ::testing::Test()
{
}

CommaSeparatedOptionsTest::~CommaSeparatedOptionsTest()
{
}

void CommaSeparatedOptionsTest::SetUp()
{
}

void CommaSeparatedOptionsTest::TearDown()
{
}

static
boost::program_options::variables_map parse(std::vector<const char*> const& options, boost::program_options::options_description const& desc) 
{
    char** argv = const_cast<char**>(options.data());
    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::command_line_parser(options.size(), argv)
                                  .options(desc).run(), vm);
    
    boost::program_options::notify(vm);
    return vm;
}

TEST_F(CommaSeparatedOptionsTest, test_comma_sep_values)
{
    boost::program_options::options_description desc;
    CommaSeparatedOptions<int> values;
    desc.add_options()
        ("my_opts", boost::program_options::value<decltype(values)>(&values), "comma seperated values");
    std::vector<const char*> options = {"app_name", "--my_opts", "1,2,34" };
    auto vm = parse(options, desc);
    ASSERT_EQ(std::vector<int>({1,2,34}), static_cast<std::vector<int>>(values)); 
}

TEST_F(CommaSeparatedOptionsTest, test_multi_comma_sep_values)
{
    boost::program_options::options_description desc;
    CommaSeparatedOptions<int> values;
    desc.add_options()
        ("my_opts", boost::program_options::value<decltype(values)>(&values)->multitoken(), "comma seperated values");
    std::vector<const char*> options = {"app_name", "--my_opts", "1,2,34", "--my_opts", "5,6,7" };
    auto vm = parse(options, desc);
    ASSERT_EQ(std::vector<int>({1,2,34, 5,6,7}), static_cast<std::vector<int>>(values)); 
}

TEST_F(CommaSeparatedOptionsTest, test_multi_values_otheroption)
{
    boost::program_options::options_description desc;
    CommaSeparatedOptions<int> values;
    desc.add_options()
        ("other_opts", boost::program_options::value<decltype(values)>()->multitoken(), "comma seperated values")
        ("my_opts", boost::program_options::value<decltype(values)>(&values)->multitoken(), "comma seperated values");
    std::vector<const char*> options = {"app_name", "--my_opts", "1,2,34", "--other_opt", "5,6,7" };
    auto vm = parse(options, desc);
    ASSERT_EQ(std::vector<int>({1,2,34}), static_cast<std::vector<int>>(values)); 
}

TEST_F(CommaSeparatedOptionsTest, test_single_values_otheroption)
{
    boost::program_options::options_description desc;
    CommaSeparatedOptions<int> values;
    desc.add_options()
        ("other_opts", boost::program_options::value<decltype(values)>()->multitoken(), "comma seperated values")
        ("my_opts", boost::program_options::value<decltype(values)>(&values)->multitoken(), "comma seperated values");
    std::vector<const char*> options = {"app_name", "--my_opts", "1", "--other_opt", "5,6,7" };
    auto vm = parse(options, desc);
    ASSERT_EQ(std::vector<int>({1}), static_cast<std::vector<int>>(values)); 
}
} // namespace test
} // namespace panda
} // namespace ska
