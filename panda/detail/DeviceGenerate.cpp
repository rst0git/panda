/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "panda/Copy.h"
#include "panda/Buffer.h"


namespace ska {
namespace panda {


// default is to perform generate on the host and then copy to the device
// This is very inefficient so specialise as required
template<class ArchT, typename GeneratorT, typename Enable>
template<typename SrcIteratorT, typename EndIteratorT, typename GeneratorArgT>
void DeviceGenerate<ArchT, GeneratorT, Enable>::generate(
                                                 SrcIteratorT&& begin
                                               , EndIteratorT&& end
                                               , GeneratorArgT&& generator)
{
    typedef typename std::iterator_traits<
        typename std::remove_reference<SrcIteratorT>::type
        >::value_type ValT;
    panda::Buffer<ValT> buffer(std::distance(begin, end));
    std::generate(buffer.begin()
                , buffer.end()
                , std::forward<GeneratorArgT>(generator));
    panda::copy(buffer.begin(), buffer.end()
              , std::forward<SrcIteratorT>(begin));
}

// specialisation for CPU
template<typename GeneratorT, typename Enable>
class DeviceGenerate<Cpu, GeneratorT, Enable>
{
    public:
        template<typename SrcIteratorT
               , typename EndIteratorT
               , typename GeneratorArgT>
        static inline
        void generate(SrcIteratorT&& begin, EndIteratorT&& end, GeneratorArgT&& generator)
        {
            std::generate(std::forward<SrcIteratorT>(begin)
                         ,std::forward<EndIteratorT>(end)
                         ,std::forward<GeneratorArgT>(generator));
        }
};

} // namespace panda
} // namespace ska
