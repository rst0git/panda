/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTCLIENT_H
#define SKA_PANDA_TESTCLIENT_H

#include <string>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#include <boost/asio.hpp>
#pragma GCC diagnostic pop

namespace ska {
namespace panda {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief
 *    A simple synchronous client that attatches to a Server
 */
template<class ServerType>
class TestClient
{
    private:
        typedef typename ServerType::SocketType SocketType;
        
    public:
        TestClient(ServerType& server);
        ~TestClient();

        /**
         * @brief establish a connection to the server
         * @return 0 for success, 1 otherwise
         */
        bool connect();

        /**
         * @brief send a message to the server (connection has to be already established)
         */
        void send(std::string const& msg);

        /**
         * @brief drop connection to the server
         */
        void disconnect();

    private:
        boost::asio::io_service _service;
        ServerType& _server;
        SocketType _socket;
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace panda
} // namespace ska

#include "panda/test/detail/TestClient.cpp"

#endif // SKA_PANDA_TESTCLIENT_H
