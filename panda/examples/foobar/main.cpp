#include "FooBarApp.h"

int main(int argc, char** argv)
{
    // The application user configuration
    foobar::Config config;
    int rv=config.parse(argc, argv);
    if( rv ) return rv;

    // construct a FooBarApp object
    foobar::FooBarApp app(config);

    // run the app
    app.run("Hello World!");

    return 0;
}
