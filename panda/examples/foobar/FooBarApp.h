#include "CommonDefs.h"
#include "Foo.h"
#include "Bar.h"

/**
 * @brief An example application to demonstrate building flexible async modules
 * @details
 * - application supports command line and configuration file user settings
 *   - allows the full user configuration of pools
 *   - default pool definition if no user selection
 *   - user assignment of pools to async tasks
 *   - command line help option
 * - Foo represents an async task with a pre-defined handler (very efficient)
 *   - FooConfig is Foos configuration options and provides the user selected pool
 * - Bar represents an async task which, because it does not support a handler terminates an async chain
 *   - BarConfig is Bars configuration options and delivers the user selected pool
 */

namespace foobar {

class Config : public ConfigBaseT
{
    public:
        Config()
            : ConfigBaseT("foobar", "sample app to foobar a message")
            , _foo_config(pool_manager())
            , _bar_config(pool_manager())
        {
            // add the foo and bar nodes as leaves of this config node
            add(_foo_config);
            add(_bar_config);
        }

        FooConfig const& foo() const { return _foo_config; }
        BarConfig const& bar() const { return _bar_config; }

    private:
        FooConfig _foo_config;
        BarConfig _bar_config;
};

class FooBarApp {
    public:
        //! [FooBarApp Constructor]
        FooBarApp(Config const& config)
           : _bar(config.bar())
           , _foo(config.foo(), _bar)   // <---- make _foo call _bar when its done processing
           , _config(config)
        {
        }
        //! [FooBarApp Constructor]

        // the destructor must keep this object alive while any thread is using, or is
        // scheduled to use this object
        ~FooBarApp()
        {
            // we assume we are the only user of the
            // the pool_manager and so we can simply wait until all pools become inactive
            // More complex scenarios will require more complex solutions to determine
            // if this object is in use, such as reference counting.
            // A more invovlved pipeline should delegate this responsibility to
            // the async modules and then ensure that these are destroyed in
            // the correct order
            _config.pool_manager().wait();
        }

        //! [FooBarApp run]
        /**
         * @brief start the foobar pipeline
         * @details this is an asyncronous method and will
         *          return immediately without waiting for
         *          the pipeline to finish.
         */
        void run(std::string const& msg) {
            // start the pre-defined async chains processing
            _foo(msg);
        }
        //! [FooBarApp run]

    private:
        // modules that form our pipeline
        // n.b. these need to be listed in reverse order to that they will be called
        Bar _bar;
        Foo<decltype(_bar)&> _foo;
        Config const& _config;
};

} // namespace foobar
