/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ServerClient.h"
#include "panda/Error.h"
#include "panda/Buffer.h"
#include <boost/asio.hpp>

namespace ska {
namespace panda {

template<typename ServerTraits>
ServerClient<ServerTraits>::ServerClient(SocketType& socket)
    : _socket(socket)
{
}

template<typename ServerTraits>
ServerClient<ServerTraits>::~ServerClient()
{
}

template<typename ServerTraits>
void ServerClient<ServerTraits>::read(BufferType&)
{
    throw Error("ServerClient<ServerTraits>::read() not yet implemented");
}

template<typename ServerTraits>
void ServerClient<ServerTraits>::send(BufferType const buffer)
{
    // TODO log any errors
    do_write(buffer, 0, [buffer](Error const&) {} ); // keep the buffer alive by making a copy in the handler
}

template<typename ServerTraits>
void ServerClient<ServerTraits>::send(BufferType const buffer, SendHandler const& handler)
{
    do_write(buffer, 0, [buffer, handler](Error const& e) { handler(e); });
}

// udp specialization
template<typename ServerTraits>
void ServerClient<ServerTraits>::do_write(BufferType const& buffer, unsigned offset, SendHandler const& handler)
{
    try {
        _socket.async_send(
            boost::asio::buffer(buffer.data(offset), buffer.size() - offset),
            [this, handler, offset, buffer](boost::system::error_code ec, std::size_t length)
            {
                try {
                    if(ec) { 
                        //_server.error();
                        throw ec;
                    }
                    if( buffer.size() - offset < length ) {
                        do_write(buffer, buffer.size() - offset + length, handler);
                    }
                    else {
                        handler(Error());
                    }
                } catch(...) {
                    // TODO send exception in Error
                    handler(Error("unknown exception caught"));
                }
            });
    } catch(...) {
        // TODO send exception in Error
        handler(Error("exception caught"));
    }
}

template<typename ServerTraits>
std::string ServerClient<ServerTraits>::id() const
{
    return _socket.remote_endpoint().address().to_string();
}    

template<typename ServerTraits>
typename ServerClient<ServerTraits>::EndPointType& ServerClient<ServerTraits>::end_point() 
{
    return _end_point;
}
        
} // namespace panda
} // namespace ska
