# Anatomy of a Panda Data Processing Pipeline {#PDPPipeline}

Panda is a framework explicitly desinged to assist building high performance data processing pipelines.
The PANDA model iof a pipeline is shown below and these components should all be considered to exist within the same process.

\verbatim
                                                Data Flow Diagram
                                                -----------------

                Input                      |         Processing           |                Output
                                           |                              |
                                           |                              |
    +------------+       +-------------+   |   +----------------------+   |   +----------------+       +-----------------+
    | DataStream |  ->   |             |   |   |                      |   |   |                |   ->  | DataStream Sink |
    |------------+       | DataManager |   |   | Processing Pipeline  |   |   |  Data Switch   |       +-----------------+
                         |             |  -->  |                      |  -->  |                |
    +------------+       |             |   |   |                      |   |   |                |       +-----------------+
    | DataStream |  ->   |             |   |   |                      |   |   |                |   ->  | DataStream Sink |
    +------------+       +-------------+   |   +----------------------+   |   +----------------+       +-----------------+
                                           |                              |
     Read in data           Syncronise     |      Process data in         |    Create and manage        Convert data structures
     from a source          DataStreams    |      the data structures     |    flow of output data      to sinks (e.g files, 
     (e.g. from file        and memory     |                              |    (routing)                UDP streams)
      or UDP stream)        management
     and pack it into       
     a data structure     

\endverbatim

## Data Sources
The code to handle a DataStream should be encapsulated in a Producer class.
There is a base class for all Producers @ref ska::panda::Producer that should be used. 
For an example see the [Producers Tutorial](@ref ProducersTutorial)

Useful templates for developing DataStreams
- @ref ska::panda::PacketStream "PacketStream"  a template class for building multi-threaded streams based on incoming packet (UDP or TCP) based protocols

The DataManager uses std::shared_ptr objects to hide memory management aspects from the pipeline writer.
Because of this data can be retained between invocations of the pipeline processing by creating a copy via the
method shared_from_this() call of the data structure provided.

## The Processing Pipeline
This can be any code that you wish to analyse the data, before submitting output to the DataSwitch.

### Processing Tasks and Allocation of Compute Resources
High Performance Computing often uses accelerators, such as GPU cards, FPGA'a, or threads locked to specific cores.
PANDA offers the concept of Tasks and ResourcePools to facilitate their use.

The power of using Panda tasks is that they:
- enable you to easily move system resource, via a configuration file, to different parts of your compute platform without having to recode.
- easily support multiple algorithms to perform the same task
   - maximise the use of heterogeneous resources - dynamic load balancing 
   - for comparative analysis

![Tasks and Pools](tasks.png "Panda Tasks")

To show the power and flexibility of Panda tasks consider a pipeline that has to perform two
fairly complex processing tasks, Foo followed by Bar. For both Foo and Bar we have algorithms that
are targeted at Gpu accelerators as well as slower, standard Cpu based code.

\verbatim

   +-----------------+      +-------+    +-------+      +--------------+
   |   DataManager   | ---> |  Foo  | -> |  Bar  | ---> |  Data Switch |
   +-----------------+      +-------+    +-------+      +--------------+ 

\endverbatim

Defining Foo and Bar as tasks enables us to have fine grained control and tuning of where our
system resources can be targeted. Each task can be dedicated its own resource, or share it with other
tasks as required.
This is done by submitting these tasks to different, or the same, pools of compute resources. 
In the simplest case we can use the default pool that contains all resources not allocated elsewhere.


\verbatim
                                Sequence Diagram of task submission to a pool
                                ---------------------------------------------

         +---------+                +------------------------+                         +-------------------+
         |   Foo   |                | ResourcePool<Gpu, Cpu> |                         | FooTask<Gpu, Cpu> |
         +---------+                +------------------------+                         +-------------------+
              |                                 |                                                |
  do_foo()--->|    submit(FooTask)              |                                                |
              +-------------------------------->|                                                |
                                                + match_available_resource()                     |
                                                |                                                |
                                                | yes?                                           |
                                                +------------- execute_task(Resource) ---------->+
                                                | no?
                                                +--- queue_task() -+ 
                                                |                  | 
                                                +<-----------------+


\endverbatim
As tasks are submitted to the pool, they are matched against the available resources in that pool. If a suitable
resource is idle then the resource is allocated to perform that task. At the end of the task the resource is returned to the pool
to execute the next task in the queue. Tasks of many types can be submitted to the same pool, but the compiler will ensure
only resources of a suitable type can be allocated to a specific task.

So in the above case, both Foo and Bar are allocated either a Gpu device, or a Cpu core to do their work.
Now say you want to dedicate a Gpu accelerator device to perform Foo tasks only. With some simple configuration
options you can define a new pool containing a single Gpu and submit all Foo tasks to this Pool. No rewriting or recompiling required.

Pools also support the assignment of priority queues for different tasks. 
This might be useful in the case where you were spawning many Bar tasks for each Foo
and wanted to ensure Foo tasks were not getting waylaid by crowds of Bars.

For more details please see the documentation in the following classes:
- [ResourcePool](@ref ska::panda::ResourcePool) - job scheduling and resource management
- [System](@ref ska::panda::System) - accelerator device discovery
- [PoolManager](@ref ska::panda::PoolManager) - device allocation (to resource pools)


## The Data Switch
The [DataSwitch](@ref ska::panda::DataSwitch) class allows you to define output streams and route them to different data
sinks (e.g. output files, TCP/UDP streming)
