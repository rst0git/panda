/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ALTERA_DEVICEITERATOR_H
#define SKA_PANDA_ALTERA_DEVICEITERATOR_H

#include "panda/DeviceIterator.h"
#include "panda/arch/altera/Architecture.h"
#include "panda/arch/altera/Fpga.h"
#include <iterator>
#include <type_traits>

namespace ska {
namespace panda {
template<typename, class> class DeviceAllocator;
template<class, typename, typename> class DeviceMemory;
namespace altera {
template<typename DataType> class DevicePointer;
} // namespace altera

/**
 * @brief
 *    Specialisation of the panda:DeviceIterator for altera openCL
 *
 * @details
 *
 */
template<typename DataType>
class DeviceIterator<altera::OpenCl, DataType> : public std::iterator<
                                std::random_access_iterator_tag,
                                DataType,
                                std::size_t
                                >
{
        friend class panda::DeviceAllocator<DataType, altera::OpenCl>;

        template<class, typename, typename>
        friend class panda::DeviceMemory;

        typedef std::iterator<
            std::random_access_iterator_tag,
            DataType,
            std::size_t
            > BaseT;

        typedef typename BaseT::reference Reference;
        typedef typename std::conditional<std::is_const<DataType>::value
                                         , const altera::DevicePointer<typename std::remove_const<DataType>::type>
                                         , altera::DevicePointer<DataType>
                                         >::type Pointer;

    public:
        explicit DeviceIterator(std::size_t offset, Pointer&);
        ~DeviceIterator();
        DeviceIterator& operator=(DeviceIterator const&);

        /**
         * @brief conversion operator
         * @details allows a static_cast to a cl_mem type
         *          note that in debug mode there will be an assert to ensure
         *          offset is zero. Non zero offset will result in underfined behaviour
         */
        operator cl_mem const&() const;

        /**
         * @brief pre increment operator
         */
        DeviceIterator& operator++();

        /**
         * @brief post increment operator
         */
        DeviceIterator operator++(int);

        bool operator==(DeviceIterator const&) const;
        bool operator!=(DeviceIterator const&) const;

        DeviceIterator& operator+=(std::size_t);

        /**
         * @details valid on the same memory object
         */
        std::size_t operator-(DeviceIterator const&) const;

        /**
         * @brief return a relative offset from the current pointer
         */
        DeviceIterator operator+(std::size_t) const;
        DeviceIterator operator-(std::size_t) const;

        std::size_t distance(DeviceIterator const&) const;

        cl_int write_to_device(DataType const& data, std::size_t size) const;
        cl_int read_from_device(typename std::remove_const<DataType>::type& data, std::size_t size) const;

    private:
        Pointer* _dev_ptr;
        std::size_t _offset;
};

} // namespace panda
} // namespace ska
#include "detail/DeviceIterator.cpp"

#endif // SKA_PANDA_ALTERA_DEVICEITERATOR_H
