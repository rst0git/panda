#========================================================
# A collection of cmake macros and functions for
# the abstraction of dependency management
#
# Copyright 2021 Chris Williams
#
# Overall Usage:
# --------------
#
# Specify your projects dependencies with:
#
#     required_dependency(<dep_name>)
#     optional_dependency(<dep_name>)
#
# When all deps are specified, you can generate other
# products such as configuration files and cmake files
# with the appropriate functions, e.g.
#
#     generate_configure_include("MyProjectName_")
#     generate_export_project_configuration()
#========================================================

#========================================================
# upper_dependency_name
#
# brief: function to convert a CamelCase dependency name
#        into a capitalised version with underscores
#
# usage: upper_dependency_name(dependency_name upper_dependency_name)
#========================================================
function(upper_dependency_name dependency_name result)
    # converts aB -> A_B
    string(REGEX REPLACE "(.)([A-Z][a-z]+)" "\\1_\\2" value "${dependency_name}")
    string(REGEX REPLACE "([a-z0-9])([A-Z])" "\\1_\\2" value "${value}")
    string(TOUPPER "${value}" res)
    set(${result} ${res} PARENT_SCOPE)
endfunction(upper_dependency_name)

#========================================================
# load_dependency
#
# brief: load in a cmake file to describe a dependency configuration
#
# usage: load_dependency(dependency_name)
#
# details: Note that the DEPENDENCY_NAME will be the capitalised version of the dependency name passed.
#
#          The function will:
#
#              1) search for a suitable "dependency_name.cmake" file in CMAKE_CURRENT_SOURCE_DIR,
#                 CMAKE_CURRENT_SOURCE_DIR/cmake/, and then the usual cmake module loading rules.
#
#              2) ACTIVE_DEPENDENCIES variable in the parent scope will be appended with the dependency_name.
#
#              3) DEPENDENCY_LIBRARIES variable in the parent scope will be appended
#                 with the contents of the ${${DEPENDENCY_NAME}_LIBRARIES} variable.
#
# developer note: has to be a macro as dependency configurations may
#                 set variables in the environment that need to propagate
#========================================================
macro(load_dependency DEPENDENCY_NAME)

    upper_dependency_name(${DEPENDENCY_NAME} UPPER_DEPENDENCY_NAME)
    set(_guard_var "_SKA_PANDA_DEP_GUARD_${UPPER_DEPENDENCY_NAME}")
    if(NOT DEFINED ${_guard_var}) # only try once
        set(${_guard_var} TRUE)
        set(_option_name "ENABLE_${UPPER_DEPENDENCY_NAME}")
        set(_found FALSE)
        foreach(_dir ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_MODULE_PATH})
            set(_include_file_search
                "${_dir}/${DEPENDENCY_NAME}.cmake"
                "${_dir}/cmake/${DEPENDENCY_NAME}.cmake"
            )
            foreach(_filename ${_include_file_search})
               if(EXISTS "${_filename}")
                   include("${_filename}")
                   message("Loading dependency configuration for ${DEPENDENCY_NAME} from ${_filename}")
                   set(_found TRUE)
                   break()
               endif()
            endforeach()
            if(_found)
                break()
            endif()
        endforeach()
        if(NOT _found)
            find_package("${DEPENDENCY_NAME}" REQUIRED)
        endif()
        if(${_option_name})
            list(APPEND ACTIVE_DEPENDENCIES ${DEPENDENCY_NAME})
        endif()
        set(LIBS_VAR "${UPPER_DEPENDENCY_NAME}_LIBRARIES")
        if(DEFINED ${LIBS_VAR})
            list(APPEND DEPENDENCY_LIBRARIES "${${LIBS_VAR}}")
        endif()
    endif()

endmacro(load_dependency)

#========================================================
# required_dependency
#
# brief: specify a required dependency
#
# usage: required_dependency(dependency_name)
#
# details: sets ENABLE_<UPPERCASE_DEPENDENCY_NAME> flag to true and calls load_dependency
#========================================================
macro(required_dependency dependency_name)
    upper_dependency_name(${dependency_name} UPPER_DEPENDENCY_NAME)
    set(OPTION_NAME "ENABLE_${UPPER_DEPENDENCY_NAME}")
    if(NOT DEFINED ${OPTION_NAME})
        set("${OPTION_NAME}" TRUE CACHE INTERNAL "PANDA_dep_required" FORCE)
    endif()
    load_dependency(${dependency_name})
endmacro(required_dependency)

#========================================================
# optional_dependency
#
# brief: specify an optional dependency
#
# usage: optional_dependency(dependency_name)
#
# details: will define a user option for the dependency ENABLE_<DEPENDENCY_NAME> which will default to
#          OFF unless the dependency has already been activated with a required_dependency() call.
#
#          Note that the DEPENDENCY_NAME will be the capitalised version of the dependency name passed.
#
#          If this option is set, the function will:
#
#              1) then search for a suitable "dependency_name.cmake" file in the CMAKE_CURRENT_SOURCE_DIR,
#                 CMAKE_CURRENT_SOURCE_DIR/cmake/, and then the usual cmake module loading rules.
#
#              2) ACTIVE_DEPENDENCIES variable in the parent scope will be appended with the dependency_name.
#                 This variable will be used when generate_configure_include() function is called.
#
#              3) OPTIONAL_ACTIVE_DEPENDENCIES variable in the parent scope will be appended with the dependency_name.
#
#              4) DEPENDENCY_LIBRARIES variable in the parent scope will be appended
#                 with the contents of ${${DEPENDENCY_NAME}_LIBRARIES} variable.
#
# developer note: has to be a macro as dependency configurations may
#                 set variables in the environment that need to propagate
#========================================================
macro(optional_dependency DEPENDENCY_NAME)

    upper_dependency_name(${DEPENDENCY_NAME} UPPER_DEPENDENCY_NAME)

    # Set an option for the dependency
    set(_option_name "ENABLE_${UPPER_DEPENDENCY_NAME}")
    set(_guard_var "_SKA_PANDA_DEP_GUARD_${UPPER_DEPENDENCY_NAME}")
    set(_option_default OFF)
    if(DEFINED ${_guard_var})
        if(NOT DEFINED ${_option_name})
            set(_option_default ON)
        endif()
    endif()
    option(${_option_name} "Enable functionality associated with the ${DEPENDENCY_NAME} dependency." ${_option_default})

    # If this dependency is marked as required elsewhere, default to ON
    if(${_option_name}) # If option is set...

        # Find the dependencies settings
        load_dependency(${DEPENDENCY_NAME})

        # Update dependency info for selected dependency
        list(APPEND OPTIONAL_ACTIVE_DEPENDENCIES ${DEPENDENCY_NAME})

    endif(${_option_name})

endmacro(optional_dependency)

#========================================================
# message_deps
#
# brief: generate messages as a column of activated optional
#        dependency names and the configured version
#
# usage: message_deps("MyPrefix_" ${DEPENDENCY_LIST})
#
# detail: version strings are taken from variable names
#         that have the form <CAPITALISED_DEPENDENCY>_VERSION
#========================================================
function(message_deps prefix_string)
    foreach(dep ${ARGN})
        upper_dependency_name(${dep} UPPER_DEPENDENCY_NAME)
        set(version_var "${UPPER_DEPENDENCY_NAME}_VERSION")
        if(DEFINED ${version_var})
            message(STATUS "${prefix_string}${dep} ${${version_var}}")
        else()
            message(STATUS "${prefix_string}${dep}")
        endif()
    endforeach()
endfunction(message_deps)

#========================================================
# message_active_deps
#
# brief: generate messages as a column of activated optional
#        dependency names and the configured version
#
# usage: message_active_deps(${DEPENDENCY_LIST})
#
# detail: The OPTIONAL_ACTIVE_DEPENDENCIES variable is used to specify which dependencies are activated.
#         Version strings are taken from variable names that have the form <CAPITALISED_DEPENDENCY>_VERSION.
#========================================================
function(message_active_deps prefix_string)
    message_deps(${prefix_string} ${OPTIONAL_ACTIVE_DEPENDENCIES})
endfunction(message_active_deps)

#========================================================
# generate_configuration_header
#
# brief: function to create a Configuration.h file that
#        sets a number of #defines that specify which
#        optional paremeters are present.
#
# usage: generate_configuration_header(DEFINE_PREFIX)
#
# detail: each define will have the form:
#             #define ${DEFINE_PREFIX}ENABLE_<CAPITALISED_DEPENDENCY_NAME>
#
#         The generated file will be placed in the ${PROJECT_NAME}
#         directory so it can be included by, for example,
#             #include "MyProject/Configuration.h"
#========================================================
function(generate_configuration_header DEFINE_PREFIX)
    set(out_file "${PROJECT_NAME}/Configuration.h")
    set(out_file_tmp "${PROJECT_BINARY_DIR}/${out_file}.in")
    set(guard_name "${DEFINE_PREFIX}${PROJECT_NAME}_CONFIGURATION_H")
    file(WRITE "${out_file_tmp}" "// ---- configuration parameters generated by cmake -----\n")
    file(APPEND "${out_file_tmp}" "// - Do not edit (otherwise you risk binary incompatabilities) -\n")
    file(APPEND "${out_file_tmp}" "#ifndef ${guard_name}\n")
    file(APPEND "${out_file_tmp}" "#define ${guard_name}\n")
    file(APPEND "${out_file_tmp}" "// ---- begin option configuration parameters -----\n")
    foreach(dep ${ACTIVE_DEPENDENCIES})
        upper_dependency_name(${dep} UPPER_DEPENDENCY_NAME)
        set(option_name "ENABLE_${UPPER_DEPENDENCY_NAME}")
        get_property(result CACHE ${option_name} PROPERTY HELPSTRING)
        # If the help message is not set by required_dep(), we know that this a configurable option
        if(NOT ${result} STREQUAL "PANDA_dep_required")
            set(define_var "${DEFINE_PREFIX}ENABLE_${UPPER_DEPENDENCY_NAME}")
            file(APPEND "${out_file_tmp}" "#ifndef ${define_var}\n")
            file(APPEND "${out_file_tmp}" "#define ${define_var}\n")
            file(APPEND "${out_file_tmp}" "#endif // ${define_var}\n")
        endif()
    endforeach()
    file(APPEND "${out_file_tmp}" "// ---- end option configuration parameters -----\n")
    file(APPEND "${out_file_tmp}" "#endif // ${guard_name}\n")
    configure_file(${out_file_tmp} ${out_file} COPYONLY)
    install(FILES ${PROJECT_BINARY_DIR}/${out_file} DESTINATION "${INCLUDE_INSTALL_DIR}/${PROJECT_NAME}")
endfunction(generate_configuration_header)

#========================================================
# generate_export_project_configuration
#
# brief: function to create a ${PROJECT_NAME}Configuration.cmake file
#        and install it in the MODULES_INSTALL_DIR/cmake directory.
#
# usage: generate_export_project_configuration()
#
# detail: this file will contain the required_dependency list for the
#         project as a convenience for dependents.
#
#         All optional_dependencies that have been activated will also
#         be included as required_dependency specifications.
#========================================================
function(generate_export_project_configuration)
    set(out_file "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Configuration.cmake")
    set(out_file_tmp "${out_file}.in")
    file(WRITE "${out_file_tmp}" "# Generated file for configuration of ${PROJECT_NAME}\n")
    foreach(dep ${ACTIVE_DEPENDENCIES})
        file(APPEND "${out_file_tmp}" "required_dependency(${dep})\n")
    endforeach()
    configure_file(${out_file_tmp} ${out_file} COPYONLY)
    install(FILES ${out_file} DESTINATION "${MODULES_INSTALL_DIR}/cmake")
endfunction(generate_export_project_configuration)
