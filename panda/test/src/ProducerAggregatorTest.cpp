/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ProducerAggregatorTest.h"
#include "panda/ProducerAggregator.h"
#include "panda/test/TestStream.h"


namespace ska {
namespace panda {
namespace test {


ProducerAggregatorTest::ProducerAggregatorTest()
    : ::testing::Test()
{
}

ProducerAggregatorTest::~ProducerAggregatorTest()
{
}

void ProducerAggregatorTest::SetUp()
{
}

void ProducerAggregatorTest::TearDown()
{
}

struct DataA : public std::string {
    DataA() {}
    DataA(std::string&& s) : std::string(std::move(s)) {}
};

struct DataB : public std::string {
    DataB() {}
    DataB(std::string&& s) : std::string(std::move(s)) {}
};

template<class Producer> using StreamATmpl = TestStreamTemplate<Producer, DataA>;
template<class Producer> using StreamBTmpl = TestStreamTemplate<Producer, DataB>;

TEST_F(ProducerAggregatorTest, test_combined_stream)
{

    typedef CombinedStream<StreamATmpl, StreamBTmpl> AggregatorType;
    typedef StreamATmpl<AggregatorType> StreamA;
    bool init_a = false;
    DataA s1("a");
    std::unique_ptr<StreamA> a(new StreamA([&](StreamA& s){ 
        init_a = true; 
        s.data(s1);
    }));

    typedef StreamBTmpl<AggregatorType> StreamB;
    bool init_b = false;
    DataB s2("b");
    std::unique_ptr<StreamB> b(new StreamB([&](StreamB& s){ 
        init_b = true;
        s.data(s2);
    }));

    AggregatorType agg(std::move(a), std::move(b));
    panda::DataManager<AggregatorType> dm(agg);
    ASSERT_TRUE(init_a);
    ASSERT_TRUE(init_b);
    
    std::tuple<std::shared_ptr<DataA>, std::shared_ptr<DataB>> res = dm.next();
    ASSERT_EQ(*std::get<0>(res), s1);
    ASSERT_EQ(*std::get<1>(res), s2);
}

} // namespace test
} // namespace panda
} // namespace ska
