/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DEVICECONFIG_H
#define SKA_PANDA_DEVICECONFIG_H

#include "panda/ConfigModule.h"


namespace ska {
namespace panda {

/**
 * @brief
 *    specialisation required for each Architecture to describe configuration
 *
 * @details
 *    each template class must inherit from ConfigModule and
 *    define
 *    @code
 *    struct Dervived<MyArch> : public DerivedBase<Dervied<MyArch>>
 *    {
 *        ststic std::string const& tag_name();
 *    };
 *    @endcode
 *    You can also override the add_options mehtod to extend the options for the specific device
 */
template<typename Architecture>
struct DeviceConfig;

template<typename Derived>
struct DeviceConfigBase : public ConfigModule
{
    public:
        DeviceConfigBase(std::string const& tag = Derived::tag_name());
        DeviceConfigBase(DeviceConfigBase const&);
        virtual ~DeviceConfigBase();

        void add_options(OptionsDescriptionEasyInit& ) override;
        
        /**
         * @brief return the number of dvices to associuate with this configuration
         */
        std::size_t device_count() const;
        
    protected:
        std::size_t _device_count;
};

} // namespace panda
} // namespace ska
#include "panda/detail/DeviceConfig.cpp"

#endif // SKA_PANDA_DEVICECONFIG_H 
