/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_RESOURCEMANAGER_H
#define SKA_PANDA_RESOURCEMANAGER_H

#include "panda/Engine.h"
#include "panda/ResourceTracker.h"
#include <deque>
#include <vector>
#include <mutex>

namespace ska {
namespace panda {

/**
 * @class ResourceManager
 *
 * @brief
 *    An asyncronous Resource Manager
 *
 * @details
 *    Allows you to submit jobs to a queue of Compute resources
 *    As the resource becomes available, the next item from the queue
 *    is taken and executed.
 *
 *    Use the @code add_resource() method to add Resources to be managed.
 *
 *    call @code async_request() to request a resource. When the resource becomes
 *    available the handler will be called
 */

template<class Resource>
class ResourceManager
{
    public:
        typedef std::function<void(ResourceTracker<Resource>)> Handler;

    public:
        ResourceManager(Engine& engine);
        ~ResourceManager();

        /// add a Resource (e.g. an NVidia card, or a Processor Core) to manage
        void add_resource(Resource r);

        /// return the number of idle resources
        ///  available
        int free_resources() const;

        /// return the number of jobs that are in the queue
        int requests_queued() const;

        /// set a handler to be called when a resource becomes available
        void async_request( Handler );

        /// return the engine used for async tasks
        Engine& engine() const;

        /// stop the engine. Will block until all resources are returned
        void stop();

    private: //functions
        void _match_resources();
        void _run_job( Resource, Handler );

        friend class ResourceTracker<Resource>;
        void _resource_free( Resource );

    private:
        bool _destructor;
        Engine& _engine;
        mutable std::mutex _resource_mutex;
        std::deque<Handler> _queue;
        std::deque<Resource> _free_resource;
        std::size_t _total_res;
};

} // namespace panda
} // namespace ska
#include "panda/detail/ResourceManager.cpp"

#endif // SKA_PANDA_RESOURCEMANAGER_H
