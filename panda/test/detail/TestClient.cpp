/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestClient.h"
#include "panda/Version.h"
#include <vector>
#include <type_traits>

namespace ska {
namespace panda {


template<class ServerType>
TestClient<ServerType>::TestClient(ServerType& server)
    : _server(server)
    , _socket(_service)
{
}

template<class ServerType>
TestClient<ServerType>::~TestClient()
{
    _service.stop();
}

template<class ServerType>
bool TestClient<ServerType>::connect()
{
    boost::system::error_code ec;
    _socket.connect(_server.end_point(), ec);
    if( ec ) {
        std::cerr << "TestClient::connect() : unexpected error " << ec << std::endl;
        return true;
    }
    _socket.set_option(boost::asio::socket_base::reuse_address(true), ec);
    return false;
}

template<class ServerType>
void TestClient<ServerType>::disconnect()
{
    _socket.close();
    _service.run();
    _service.reset();
}

template<bool StreamSocket>
struct SendHelper 
{
    template<class SocketType>
    inline static void send(SocketType& socket, std::string& msg) 
    {
        boost::asio::write(socket, boost::asio::buffer(msg));
    }
};

template<>
struct SendHelper<false>
{
    template<class SocketType>
    inline static void send(SocketType& socket, std::string const& msg) 
    {
        socket.send(boost::asio::buffer(msg));
    }
};

template<class ServerType>
void TestClient<ServerType>::send(std::string const& msg)
{
    try {
#if PANDA_BOOST_MAJOR_VERSION <= 1
#if PANDA_BOOST_MINOR_VERSION < 66
        SendHelper<std::is_base_of<boost::asio::basic_stream_socket<typename SocketType::protocol_type, typename SocketType::service_type>, SocketType>::value>::send(_socket, msg);
#else
        SendHelper<std::is_base_of<boost::asio::basic_stream_socket<typename SocketType::protocol_type>, SocketType>::value>::send(_socket, msg);
#endif // PANDA_BOOST_MINOR_VERSION < 66
#endif //PANDA_BOOST_MAJOR_VERSION <= 1
        _service.run();
        _service.reset();
    }
    catch( std::exception const& e) {
        std::cerr << "TestClient::send() : unexpected exception " << e.what() << std::endl;
    }
}


} // namespace panda
} // namespace ska
