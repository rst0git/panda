/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/EndpointConfig.h"


namespace ska {
namespace panda {


EndpointConfig::EndpointConfig(std::string const& tag_name)
    : ConfigModule(tag_name)
    , _ip_address(0, "127.0.0.1")
{
}

EndpointConfig::~EndpointConfig()
{
}

void EndpointConfig::add_options(OptionsDescriptionEasyInit& add_options)
{
    std::string port_help("the port of the endpoint (default=");
                port_help += std::to_string(_ip_address.port());
                port_help += ")";

    std::string address_help("the endpoint IP address (default=");
                address_help += _ip_address.address().to_string();
                address_help += ")";

    add_options
    ("port", boost::program_options::value<unsigned>()->default_value(_ip_address.port())->notifier(
         [this](unsigned port) {
             _ip_address.port(port);
         })
         , port_help.c_str())
    ("ip_address", boost::program_options::value<std::string>()->default_value(_ip_address.address().to_string())->notifier(
         [this](std::string const& address) {
             _ip_address = IpAddress(address);
         })
         , address_help.c_str())
    ;
}

IpAddress const& EndpointConfig::address() const
{
    return _ip_address;
}

void EndpointConfig::address(IpAddress const& address)
{
    _ip_address = address;
}

} // namespace panda
} // namespace ska
