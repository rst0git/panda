#=======================================================================
#
# Finds the OpenCL development toolkit and sets includes and library
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
# This will define the following variables:
#
#    ``OPENCL_FOUND``
#      TRUE if OpenCL libs and include directory are found.
#
#    ``OPENCL_LIBRARIES``
#      List of libraries for OpenCL.
#
#    ``OPENCL_INCLUDE_DIR``
#      Directory where rabbit includes were found.
#
# Location Variables
# ^^^^^^^^^^^^^^^^^^
#
# The following variables can be set to explicitly specify the locations of OpenCL components:
#
#    ``OPENCL_INCLUDE_DIR``
#      Explicitly define in which directory OpenCL includes may be found.
#
#    ``OPENCL_INSTALL_DIR``
#      Where OpenCL framwork has been installed (lib/ and include/ directories included).
#
#    ``OPENCL_LIBRARY_DIRS``
#      Explicitly define in which directories OpenCL libraries may be found.
#
#=======================================================================
option(ENABLE_OPENCL "Enable OpenCL architecture" FALSE)

if(ENABLE_OPENCL)


    if(OPENCL_INCLUDE_DIR)
        set(opencl_inc_dirs ${OPENCL_INCLUDE_DIR})
        unset(OPENCL_INCLUDE_DIR CACHE)
    endif()

    find_path(OPENCL_INCLUDE_DIR
              NAMES
                  CL/cl.h OpenCL/cl.h
              PATHS
                  ${opencl_inc_dirs}
                  ${OPENCL_INSTALL_DIR}
                  ${OPENCL_INSTALL_DIR}/hld
                  ${ALTERAOCLSDKROOT}
                  ${INTELFPGAOCLSDKROOT}
                  ENV "PROGRAMFILES(X86)"
                  ENV AMDAPPSDKROOT
                  ENV NVSDKCOMPUTE_ROOT
                  ENV ATISTREAMSDKROOT
                  /usr/local
                  /usr
              PATH_SUFFIXES
                  include
                  host/include
                  OpenCL/common/inc
                  "AMD APP/include"
    )
find_library(OPENCL_LIBRARIES
                 NAMES
                     OpenCL
                 PATHS
                     ${OPENCL_LIBRARY_DIRS}
                     ${OPENCL_INSTALL_DIR}/host
                     ${OPENCL_INSTALL_DIR}/hld/host
                     ${ALTERAOCLSDKROOT}
                     ${INTELFPGAOCLSDKROOT}
                 PATH_SUFFIXES
                     lib64
                     lib
                     "AMD APP/lib/x86"
                     lib/x86
                     lib/Win32
                     linux64/lib
                     host/linux64/lib
                     OpenCL/common/lib/Win32
    )

    include(FindPackageHandleCompat)
    find_package_handle_standard_args(OPENCL DEFAULT_MSG OPENCL_LIBRARIES OPENCL_INCLUDE_DIR)

    if(NOT OPENCL_FOUND)
        set(OPENCL_LIBRARIES)
        set(OPENCL_TEST_LIBRARIES)
    endif()

    mark_as_advanced(OPENCL_LIBRARIES OPENCL_TEST_LIBRARIES OPENCL_INCLUDE_DIR)

    include_directories(${OPENCL_INCLUDE_DIR})

    list(APPEND DEPENDENCY_LIBRARIES ${OPENCL_LIBRARIES})
    message("OpenCL Libraries: ${OPENCL_LIBRARIES}")

endif(ENABLE_OPENCL)
