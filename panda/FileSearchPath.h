/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_FILESEARCHPATH_H
#define SKA_PANDA_FILESEARCHPATH_H

#include <boost/filesystem.hpp>
#include <list>
#include <utility>

namespace ska {
namespace panda {

/**
 * @brief A list of directory paths to search for files
 *
 * @details
 *
 */
class FileSearchPath
{
    public:
        FileSearchPath();
        FileSearchPath(boost::filesystem::path const&);

        /**
         * @brief add a path at the end of the list of paths to search
         */
        FileSearchPath& push_back(boost::filesystem::path const&);

        /**
         * @brief insert a path at the beginning of the list of paths to search
         */
        FileSearchPath& push_front(boost::filesystem::path const&);

        /**
         * @brief search for the specified file in the paths
         * @return the first path that matches, or an empty string if there are no matches
         * @details this is differs from find_path() only in the type returned
         */
        std::string find(boost::filesystem::path const& filename) const;

        /**
         * @brief search for the specified file in the paths
         * @return a pair with first being the first path that matches
         *         if the second element is true.
         *         If the second element if false then no matching file was found
         *         and the first element will be the default boost::filesystem::path object
         */
        std::pair<boost::filesystem::path, bool> find_path(boost::filesystem::path const& filename) const;

        /**
         * @brief return a list of file paths
         */
        std::list<boost::filesystem::path> const& paths() const;

    private:
        std::list<boost::filesystem::path> _paths;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_FILESEARCHPATH_H
