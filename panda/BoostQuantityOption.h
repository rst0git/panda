/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_BOOSTQUANTITYOPTION_H
#define SKA_PANDA_BOOSTQUANTITYOPTION_H

#include <boost/any.hpp>
#include <boost/units/quantity.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <string>
#include <vector>

namespace boost {
namespace program_options {

// overide function to integrate with boost::program_options
template<typename UnitType, typename NumericalType>
void validate(boost::any& v,
              const std::vector<std::string>& values,
              boost::units::quantity<UnitType, NumericalType>*, int);

// speicization for boost::program_options internal class typed_value
template<typename UnitType, typename T, class charT>
class typed_value<boost::units::quantity<UnitType, T>, charT>
    : public typed_value<T>
{
        typedef typed_value<T> BaseT;

    public:
        typed_value() {}
        typed_value(boost::units::quantity<UnitType, T>* v)
            : BaseT((T*)v)
        {
        }

        typed_value* default_value(const boost::units::quantity<UnitType, T>& v)
        {
            BaseT::default_value(v.value());
            return this;
        }
};

} // namespace boost
} // namespace program_options
#include "panda/detail/BoostQuantityOption.cpp"

#endif // SKA_PANDA_BOOSTQUANTITYOPTION_H
