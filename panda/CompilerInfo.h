/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_COMPILERINFO_H
#define SKA_PANDA_COMPILERINFO_H

/**
 * @brief
 *    Central place to record the quirks of a compiler and set flags based on behaviour
 *    to enable workarounds in the code.
 * @details
 * 
 */

#ifdef __GNUC__
#ifndef __clang__
#if __GNUC__ < 5
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#include <boost/type_traits.hpp>
#pragma GCC diagnostic pop
namespace std {
    template<class T>
    struct is_trivially_copyable : public boost::has_trivial_copy<T>
    {
    };
    template<class T>
    struct is_trivially_copy_constructible : public boost::has_trivial_copy_constructor<T>
    {
    };
} // namespace std
#endif // __GNUC__ < 5
#endif // __clang__
//#if __GNUC__ < 4
#define broken_lambda_parameter_pack_expansion
//#endif
#endif

#endif // SKA_PANDA_COMPILERINFO_H 
