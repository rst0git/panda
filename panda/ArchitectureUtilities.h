/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCHITECTUREUTILITIES_H
#define SKA_PANDA_ARCHITECTUREUTILITIES_H

#include <boost/mpl/find_if.hpp>
#include <boost/mpl/vector.hpp>

#include <tuple>
#include <type_traits>

namespace ska {
namespace panda {

/**
 * @brief
 *     Type to identify given capabilities as a range
 * @details
 *
 *     @tparam Capability Defines the minimum version required. Check is inclusive of this version
 */
template<class Capability>
struct MinimumVersion {
    typedef Capability value;
};

/**
 * @brief
 *     Type to identify given capabilities as a range
 * @details
 *
 *     @tparam Capability Defines the maximum version required. Check is inclusive of this version
 */
template<class Capability>
struct MaximumVersion {
    typedef Capability value;
};

/**
 * @brief
 *     Type to identify given capabilities as a range
 * @details
 *
 *     @tparam lower Lower bound of range. Versions must be greater or equal to this.
 *     @tparm upper Upper bound of range. Versions must be lower or equal to this
 */
template<class CapabilityLower, class CapabilityUpper>
struct Range {
    typedef CapabilityLower lower;
    typedef CapabilityUpper upper;
};

/**
 * @brief
 *     Type to identify a set of version requirements
 * @details
 *
 *     @tparam lower Lower bound of range. Versions must be greater or equal to this.
 *     @tparm upper Upper bound of range. Versions must be lower or equal to this
 */
template<typename... VersionRequirements>
struct Version {
  typedef std::tuple<VersionRequirements...> value;
};

/* @brief Compile-time numerical meta-function to check if two architectures
 * are compatible. IsCompatible<A, B> does not necessarily equal IsCompatible<B, A>
 * @details specialise for any specific architectures. Default behaviour returns
 * false
 * @tparam Required Type considered as requirement for compatability
 * @tparam Actual Type checked against Required for compatability
 * @tparam Enable Allows selection of template specialisations through SFINAE
 */
template<class Required, class Actual,
         class Enable = void>
struct IsCompatible : std::false_type {
};

/**
 * IsCompatible specialisation for the same types returns true always
 */
template<class T>
struct IsCompatible<T, T> : std::true_type {
};

namespace detail {
/**
 * Helper struct to check for a compatible architecture in a set of architectures
 */
template<class T, class U, class... TTypes>
struct IsCompatibleMultiHelper {
  typedef boost::mpl::vector<T, TTypes...> requires_list;
  typedef typename boost::mpl::find_if<
      requires_list, typename boost::mpl::lambda<IsCompatible<
                                                   boost::mpl::placeholders::_1, U>>::type>::type req_iter;

  static constexpr bool value =
    !boost::is_same<req_iter, typename boost::mpl::end<requires_list>::type>::value;
};

}

/**
 * IsCompatible specialisation for a tuple of requirements.
 */
template<class T, class U, class... TTypes>
struct IsCompatible<std::tuple<T, TTypes...>, U>
  : std::integral_constant<bool, detail::IsCompatibleMultiHelper<T, U, TTypes...>::value> {
};

} // namespace panda
} // namespace ska


#endif // SKA_PANDA_ARCHITECTUREUTILITIES_H
