@page for_cpp_developers Panda for C++ developers
# The use of C++ in Panda
This project uses some fairly advanced C++.
Understanding the following idioms and libraries will prove invaluable in understanding
this code.

- RAII - Resource Allocation Is Initialisation
- CRTP - The Continuously Recuring Template Pattern
- SFINAE - substitution failure is not an error
- std::shared_ptr, std::unique_ptr
- lambda functions
- Iterators, and std::iterator_traits
- Factory Design Pattern
- Move semantics
- Type Traits
- Perfect Forwarding and Universal References
- STL standard template library
- boost::asio
- boost::mpl

## Recommended Reading
- Essential Modern C++, Scott Meyers
- Hands-On Design Patterns With C++, Fedor G. Pikus

# Panda Coding Standards and Conventions
- @ref developers_guide Developers Guide
- @ref cmake-conventions cmake guidelines

# Contributing To Panda
  Any changes should be submitted in a feature branch via a merge request.
  Please ensure you follow the @ref developers_guide "rules and guidelines" in the various coding standards.
