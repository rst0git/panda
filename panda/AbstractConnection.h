/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ABSTRACTCONNECTION_H
#define SKA_PANDA_ABSTRACTCONNECTION_H

#include "panda/Buffer.h"

namespace ska {
namespace panda {
class Error;

/**
 * @brief
 *    Base class for connections at the level of sending/receiving bytes asyncronously
 *
 * @details
 * 
 */
class AbstractConnection
{
    public:
        //typedef Buffer<uint8_t> BufferType;
        typedef Buffer<char> BufferType;
        typedef std::function<void(Error)> SendHandler;
        typedef std::function<void(Error, std::size_t bytes_read, BufferType)> ReceiveHandler;

    public:
        AbstractConnection() {};
        virtual ~AbstractConnection() {};
        
        virtual void send(BufferType const buffer, SendHandler const& handler) = 0;
        virtual void receive(BufferType buffer, ReceiveHandler handler) = 0;
        virtual void close() = 0;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_ABSTRACTCONNECTION_H 
