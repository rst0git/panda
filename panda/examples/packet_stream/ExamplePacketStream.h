/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_EXAMPLEPACKETSTREAM_H
#define SKA_PANDA_EXAMPLEPACKETSTREAM_H

#include "panda/examples/packet_stream/ExampleDataTraits.h"
#include "panda/PacketStream.h"
#include "panda/ConnectionTraits.h"

namespace ska {
namespace panda {
namespace examples {

/**
 * @brief An example PacketStream implementation.
 */
template<typename Producer>
class ExamplePacketStreamImpl :
    public panda::PacketStream<ExamplePacketStreamImpl<Producer>, panda::ConnectionTraits<panda::Udp>, ExampleDataTraits, Producer>
{
        typedef panda::PacketStream<ExamplePacketStreamImpl<Producer>, panda::ConnectionTraits<panda::Udp>, ExampleDataTraits, Producer> BaseT;

    public:
        ExamplePacketStreamImpl();
        ~ExamplePacketStreamImpl();

        // required method 
        // The packet provided is the first packet received that corresponds to the new chunk but its 
        // does not necessarily correspond to the first packet of this chunk.
        // The first_sequence_number provides the sequence number of the packet that would be the first in the block.
        template<typename DataType>
        std::shared_ptr<DataType> get_chunk( typename BaseT::LimitType first_sequence_number
                                           , typename ExampleDataTraits::PacketInspector const& packet);

};

class ExamplePacketStream : public ExamplePacketStreamImpl<ExamplePacketStream>
{
        typedef ExamplePacketStreamImpl<ExamplePacketStream> BaseT;

    public:
        ExamplePacketStream();
        ~ExamplePacketStream();

};

} // namespace examples
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_EXAMPLEPACKETSTREAM_H 
