/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_FINISHTASK_H
#define SKA_PANDA_FINISHTASK_H

#include "panda/Resource.h"
#include <memory>

namespace ska {
namespace panda {


/**
 * @brief
 *   Wraps a ResourcePool Task, but setting a flag to indicate completion. Allows you to wait() for this event
 *
 * @details
 *   Replace a Task with this wrapped version to allow you to wait for completion when
 *   the handler has been called. Useful in destructors etc. when you need to ensure the queue is clear
 *   before destroying objects.
 */
template<typename TaskType>
class FinishTask
{
    public:
        typedef typename TaskType::Architectures Architectures;

    public:
        FinishTask(TaskType&);
        ~FinishTask();

        /**
         * @brief calls to the underlying task
         */
        template<typename Arch, typename... DataTypes>
        void operator()(panda::PoolResource<Arch>& device, DataTypes&&... data);

        /**
         * @brief wait until the task has has been completed
         */
        void wait();

        /**
         * @brief mark as having finished so that wait will return
         */
        void finished();

    private:
        bool _finished;
        TaskType& _task;
};

/**
 * Same as finishTask but will keep a copy of the shared_ptr to ensure the Task
 *  remains in scope
 */
template<typename TaskType>
class FinishTask<std::shared_ptr<TaskType>> : public FinishTask<TaskType>
{
    private:
        typedef FinishTask<TaskType> BaseT;

    public:
        typedef typename BaseT::Architectures Architectures;

    public:
        FinishTask(std::shared_ptr<TaskType> task);

    private:
        std::shared_ptr<TaskType> _task_ptr;
};

} // namespace panda
} // namespace ska
#include "panda/detail/FinishTask.cpp"
#endif // SKA_PANDA_FINISHTASK_H
