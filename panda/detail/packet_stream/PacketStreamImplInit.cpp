/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace ska {
namespace panda {


template<class ParentType>
PacketStreamImplInit<ParentType>::PacketStreamImplInit(ParentType& parent)
    : _parent(parent)
{
}

template<class ParentType>
PacketStreamImplInit<ParentType>::~PacketStreamImplInit()
{
}

template<class ParentType>
void PacketStreamImplInit<ParentType>::start_chunk(std::shared_ptr<ParentType> parent, ResourceBufferType buffer)
{
    if(!_parent._halt) {
        try {
            KeepAliveContextPtr keep_alive = std::make_shared<typename ParentType::KeepAliveContext>(std::move(parent), std::move(buffer));
            start_chunk(keep_alive);
        }
        catch(panda::Error const& e) {
            // cancellations can occur when shutting down - ignore them
        }
        catch(...) {
            PANDA_LOG_ERROR << "unexpected error";
        }
    }
}

template<class ParentType>
void PacketStreamImplInit<ParentType>::start_chunk(KeepAliveContextPtr keep_alive)
{
    if(!_parent._halt) {
        try {
            _parent._connection.receive(
                    keep_alive->buffer()
                    , std::bind(&PacketStreamImplInit<ParentType>::get_packet, this, keep_alive
                        , std::placeholders::_1
                        , std::placeholders::_2
                        , 0
                        , std::placeholders::_3)
                    ); // end std::bind
        }
        catch(panda::Error const& e) {
            // cancellations can occur when shutting down - ignore them
        }
        catch(...) {
            PANDA_LOG_ERROR << "unexpected error";
        }
    }
}

template<class ParentType>
void PacketStreamImplInit<ParentType>::get_packet(
            KeepAliveContextPtr keep_alive
          , panda::Error const& ec
          , std::size_t bytes_read
          , std::size_t total_bytes_read
          , BufferType buffer
          )
{
    try {
        if(!ec) {
            total_bytes_read += bytes_read;
            // ensure we have a full datagram to process
            if(total_bytes_read < sizeof(PacketType) && !_parent._halt) {
                // n.b. we use std::bind as lambdas do not yet support move semantics
                _parent._connection.socket().async_receive( boost::asio::buffer(buffer.data(total_bytes_read), sizeof(PacketType) - total_bytes_read),
                      std::bind(&PacketStreamImplInit::get_packet, this, keep_alive, std::placeholders::_1, std::placeholders::_2, total_bytes_read, std::move(buffer)));
                      return;
            }
            else {
                // process_packet
                typename ParentType::PacketInspectorType packet(*reinterpret_cast<typename ParentType::PacketType const*>(buffer.data()));
                if(packet.ignore()) {
                    start_chunk(keep_alive);
                    return;
                }

                // initialise the first context
                _parent._context->init(packet);
                _parent.start_chunk(); // this is the earliest it is safe to start catching other packets in the main loop
                // the remaining work to be carried out can now be performed by another thread
                _parent._engine.post(std::bind(&PacketStreamImplInit::process_packet, this, std::move(keep_alive)));
                return;
            }
        } else {
            throw ec;
        }
    }
    catch(std::exception const& e) {
        _parent._connection.policy().error(e);
    }
    catch(...) {
        _parent._connection.policy().error(panda::Error("PacketStreamImpl: unknown exception"));
    }
    // schedule the next packet read if there has been an error
    start_chunk(keep_alive);
}

template<class ParentType>
void PacketStreamImplInit<ParentType>::process_packet(KeepAliveContextPtr keep_alive)
{
    typename ParentType::PacketInspectorType packet(*reinterpret_cast<PacketType const*>(keep_alive->buffer().data()));
    _parent._context = _parent._context->deserialise_packet(packet);
}

} // namespace panda
} // namespace ska
