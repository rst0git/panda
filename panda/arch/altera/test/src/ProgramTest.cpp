/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ProgramTest.h"
#include "panda/arch/altera/test/GetHelloAocx.h"
#include "panda/arch/altera/Program.h"
#include "panda/arch/altera/Altera.h"
#include "panda/System.h"

#include "panda/test/gtest.h"

namespace ska {
namespace panda {
namespace test {


ProgramTest::ProgramTest()
    : BaseT()
{
}

ProgramTest::~ProgramTest()
{
}

void ProgramTest::SetUp()
{
}

void ProgramTest::TearDown()
{
}

TEST_F(ProgramTest, test_program_hello)
{
    typedef altera::OpenCl Arch;
    panda::System<Arch> system;
    ASSERT_GT(system.count<Arch>(), std::size_t(0)) << "No devices found to test";
    for (auto const& device : system.resources<Arch>())
    {
        boost::filesystem::path p = get_hello_word_aocx(*device);
        if (p.empty()) {
            std::cout << "skipping test for device " << device->device_name() << "\n";
            continue;
        }
        ska::panda::altera::Aocx aocx(p);
        ska::panda::altera::Program program(aocx, *device);
    }
}

} // namespace test
} // namespace panda
} // namespace ska
