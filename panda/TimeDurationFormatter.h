/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TIMEDURATIONFORMATTER_H
#define SKA_PANDA_TIMEDURATIONFORMATTER_H

#include "panda/ClockTypeConcept.h"
#include "panda/Log.h"
#include <iostream>
#include <chrono>

namespace ska {
namespace panda {

/**
 * @brief
 *     Writes to the stream the time as a time difference
 *
 * @details
 * 
 */

template<typename ClockType, typename StreamType=panda::Logger>
class TimeDurationFormatter
{
        BOOST_CONCEPT_ASSERT((ClockTypeConcept<ClockType>));

    public:
        TimeDurationFormatter(StreamType& stream = panda::log);
        ~TimeDurationFormatter();

        /**
         * @brief write out the time difference to the output stream
         */
        void operator()(std::chrono::time_point<ClockType> const& start, std::chrono::time_point<ClockType> const& time) ;

    private:
        typedef std::chrono::duration<double, std::micro> DurationType;
        StreamType& _stream;
        unsigned _count;
        std::chrono::time_point<ClockType> _last_start;
        DurationType _running_average;
        DurationType _call_running_average;
};

} // namespace panda
} // namespace ska
#include "panda/detail/TimeDurationFormatter.cpp"

#endif // SKA_PANDA_TIMEDURATIONFORMATTER_H 
