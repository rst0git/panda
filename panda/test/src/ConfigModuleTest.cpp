/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ConfigModuleTest.h"
#include "panda/ConfigModule.h"
#include <boost/property_tree/xml_parser.hpp>


namespace ska {
namespace panda {
namespace test {


ConfigModuleTest::ConfigModuleTest()
    : ::testing::Test()
{
}

ConfigModuleTest::~ConfigModuleTest()
{
}

void ConfigModuleTest::SetUp()
{
}

void ConfigModuleTest::TearDown()
{
}

class ConfigModuleTestClass : public ConfigModule {
    public:
        ConfigModuleTestClass(std::string name, int seq = 0 )
            : ConfigModule(std::move(name))
            , _seq(seq)
            , _val(255)
            , _val_other(254)
        {}
        void add_options(ConfigModule::OptionsDescriptionEasyInit& options) override
        {
            options
            ("test_option,t",  boost::program_options::value<int>(&_val), "some test option in factory generated module")
            ("another_test_option",  boost::program_options::value<int>(&_val_other), "some other test option");
        }

        std::string const& id() const { return ConfigModule::id(); } // bring in to public


    int _seq;
    int _val;
    int _val_other;
};

// class to generate a tree structure of depth Depth, each node in the tree will have a leaf and a complex sub-tree of depth Depth-1.
template<unsigned Depth>
class ConfigModuleComplexTestClass : public ConfigModuleTestClass {
    public:
        static const unsigned depth = Depth;

    public:
        ConfigModuleComplexTestClass(std::string const name, int seq = 0 )
            : ConfigModuleTestClass(name, seq)
            , _sub_node(name, seq)
        {
            add(_sub_node);
        }

        void add_options(ConfigModule::OptionsDescriptionEasyInit& options) override
        {
            options
            ("test_option,t",  boost::program_options::value<int>(&_val), "some test option in factory generated module")
            ("another_test_option",  boost::program_options::value<int>(&_val_other), "some other test option");
        }

        std::string const& id() const { return ConfigModule::id(); } // bring in to public


    public:
        ConfigModuleComplexTestClass<Depth-1> _sub_node;
};

template<>
class ConfigModuleComplexTestClass<1> : public ConfigModuleTestClass {
    public:
        ConfigModuleComplexTestClass(std::string name, int seq = 0 )
            : ConfigModuleTestClass(name, seq)
        {}
};


TEST_F(ConfigModuleTest, test_options)
{
    {
        // single module
        ConfigModuleTestClass m("test");
        boost::program_options::options_description desc = m.command_line_options();
        ASSERT_EQ(2U, desc.options().size());
    }
    {
        // single child - anonymous root
        ConfigModuleTestClass m("");
        ConfigModuleTestClass m2("test_1");
        m.add(m2);
        boost::program_options::options_description desc = m.command_line_options();
        ASSERT_EQ(4U, desc.options().size());
        ASSERT_EQ("test_1.test_option", desc.options()[2]->long_name());
        ASSERT_EQ("test_1.another_test_option", desc.options()[3]->long_name());
    }
    {
        // single child
        ConfigModuleTestClass m("test_0");
        ConfigModuleTestClass m2("test_1");
        m.add(m2);
        boost::program_options::options_description desc = m.command_line_options();
        ASSERT_EQ(4U, desc.options().size());
        ASSERT_EQ("test_0.test_1.test_option", desc.options()[2]->long_name());
        ASSERT_EQ("test_0.test_1.another_test_option", desc.options()[3]->long_name());
    }
    {
        // multi chileren -same type
        ConfigModuleTestClass m("test_0");
        ConfigModuleTestClass m2("test_1");
        ConfigModuleTestClass m3("test_2");
        m.add(m2);
        m.add(m3);
        boost::program_options::options_description desc = m.command_line_options();
        ASSERT_EQ(6U, desc.options().size()) << desc;
    }
    {
        // multi-level tree
        ConfigModuleTestClass m("test_0");
        ConfigModuleTestClass m2("test_1");
        ConfigModuleTestClass m3("test_2");
        ConfigModuleTestClass m4("test_3");
        m3.add(m4);
        m2.add(m3);
        m.add(m2);
        boost::program_options::options_description desc = m.command_line_options();
        boost::program_options::options_description desc_2 = m2.command_line_options();
        boost::program_options::options_description desc_3 = m3.command_line_options();
        ASSERT_EQ(4U, desc_3.options().size());
        ASSERT_EQ(6U, desc_2.options().size());
        ASSERT_EQ(8U, desc.options().size());
        ASSERT_EQ("test_0.test_1.test_option", desc.options()[2]->long_name()) << desc;
        ASSERT_EQ("test_0.test_1.another_test_option", desc.options()[3]->long_name());
        ASSERT_EQ("test_0.test_1.test_2.test_option", desc.options()[4]->long_name());
        ASSERT_EQ("test_0.test_1.test_2.another_test_option", desc.options()[5]->long_name());
        ASSERT_EQ("test_0.test_1.test_2.test_3.test_option", desc.options()[6]->long_name());
        ASSERT_EQ("test_0.test_1.test_2.test_3.another_test_option", desc.options()[7]->long_name());
    }
}

TEST_F(ConfigModuleTest, test_add_options_parse)
{
    ConfigModuleTestClass m("test");

    // test by parsing a command line
    char cmd[] = "panda_test";
    char config_option[] = "--test.test_option";
    char config_option_short[] = "--test.t";
    char config_val[] ="180";

    boost::property_tree::ptree config_tree;
    config_tree.put<int>(boost::property_tree::ptree::path_type("test_option"), 188);
    config_tree.put<int>(boost::property_tree::ptree::path_type("sub_test.test_option"), 98);

    {
        // test command line for just a single module
        char* argv[3] = { cmd, config_option, config_val };

        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::command_line_parser(3, argv)
                                      .options(m.command_line_options()).run(), vm);
        boost::program_options::notify(vm);
        ASSERT_EQ(180, m._val);
    }
    m._val=0;
    {
        // test command line for just a single module
        char* argv[3] = { cmd, config_option_short, config_val };

        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::command_line_parser(3, argv)
                                      .options(m.command_line_options()).run(), vm);
        boost::program_options::notify(vm);
        ASSERT_EQ(180, m._val);
    }
    {
        // test ptree for just a single module
        boost::program_options::variables_map vm;
        m.parse_property_tree(config_tree, vm);
        boost::program_options::notify(vm);
        ASSERT_EQ(188, m._val);
    }
    // add a sub-module
    ConfigModuleTestClass sub_m("sub_test");
   // int sub_val = 255;
    char sub_config_option[] = "--test.sub_test.test_option";
    char sub_config_val[] ="90";
    m.add(sub_m);

    {
        // test command line for just a single module
        char* argv[5] = { cmd, config_option, config_val, sub_config_option, sub_config_val };

        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::command_line_parser(5, argv)
                                      .options(m.command_line_options()).run(), vm);
        boost::program_options::notify(vm);
        ASSERT_EQ(180, m._val);
        ASSERT_EQ(90, sub_m._val);
    }
    {
        // test ptree for just a single module
        boost::program_options::variables_map vm;
        m.parse_property_tree(config_tree, vm);
        boost::program_options::notify(vm);
        ASSERT_EQ(188, m._val);
        ASSERT_EQ(98, sub_m._val);
    }
}

TEST_F(ConfigModuleTest, test_add_deep_node)
{
    // add a sub-module
    typedef ConfigModuleComplexTestClass<3> ConfigModuleType;
    ConfigModuleType root("module");
    boost::property_tree::xml_parser::write_xml(std::cout, root.config_tree());
    std::cout << "\n";
   // int sub_val = 255;
   /*
    char sub_config_option[] = "--test.sub_test.test_option";
    char sub_config_val[] ="90";

    {
        // test command line for just a single module
        char* argv[5] = { cmd, config_option, config_val, sub_config_option, sub_config_val };

        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::command_line_parser(5, argv)
                                      .options(m.command_line_options()).run(), vm);
        boost::program_options::notify(vm);
        ASSERT_EQ(180, m._val);
        ASSERT_EQ(90, sub_m._val);
    }
    */

    std::vector<int> test_option_values = { 0, 1, 2 , 3}; // store the values of the test_option for each level in the tree
    boost::property_tree::ptree config_tree;
    std::string test_option_str="test_option";
    unsigned i=0;
    do {
        config_tree.put<int>(boost::property_tree::ptree::path_type(test_option_str), test_option_values[i]);
        test_option_str="module." + test_option_str;
    }
    while( ++i < ConfigModuleType::depth);
    boost::property_tree::xml_parser::write_xml(std::cout, config_tree); std::cout << "\n";

    {
        // initiate the parse
        boost::program_options::variables_map vm;
        root.parse_property_tree(config_tree, vm);
        boost::program_options::notify(vm);

        // test we got the expected values
        unsigned i=0;
        const ConfigModuleTestClass* node = &root;
        do {
            // test the node value
            ASSERT_EQ(test_option_values[i], node->_val) << "i=" << i;
            // get the next node
            auto it=node->subsection("module");
            if(it != node->subsection_end()) {
                node = &static_cast<const ConfigModuleTestClass&>(*it);
                ASSERT_NE(node, nullptr);
            }
        } while(++i < ConfigModuleType::depth);
    }
}

TEST_F(ConfigModuleTest, test_factories)
{
    ConfigModuleTestClass m("test");

    int fac_count = 0;
    m.add_factory("gen_option", [&fac_count]() { return new ConfigModuleTestClass("generated_by_factory", ++fac_count); }); // n.b. tag name different form nmae()

    boost::property_tree::ptree config_tree;
    config_tree.put<int>(boost::property_tree::ptree::path_type("test_option"), 188);
    boost::property_tree::ptree config_tree_1;
    config_tree_1.put<int>(boost::property_tree::ptree::path_type("test_option"), 100);
    config_tree.add_child(boost::property_tree::ptree::path_type("gen_option"), config_tree_1);
    boost::property_tree::ptree config_tree_2;
    config_tree_2.put<int>(boost::property_tree::ptree::path_type("test_option"), 2*100);
    config_tree_2.put<std::string>(boost::property_tree::ptree::path_type("id"), "id2"); // explicit id set
    config_tree.add_child(boost::property_tree::ptree::path_type("gen_option"), config_tree_2);
    boost::property_tree::ptree config_tree_3;
    config_tree_3.put<int>(boost::property_tree::ptree::path_type("test_option"), 3*100);
    config_tree.add_child(boost::property_tree::ptree::path_type("gen_option"), config_tree_3);

    {
        // test ptree for just a single module
        boost::program_options::variables_map vm;
        m.parse_property_tree(config_tree, vm);
        boost::program_options::notify(vm);
        ASSERT_EQ(188, m._val);
        ASSERT_EQ(3, fac_count);
        auto mod_it = m.subsection("gen_option");
        int count=0;
        while(mod_it != m.subsection_end()) {
            ConfigModuleTestClass const& mod = dynamic_cast<ConfigModuleTestClass const&>(*mod_it);
            ASSERT_EQ(100* mod._seq, mod._val) << mod._seq;
            if(mod._seq == 2) {
                ASSERT_EQ("id2", mod.id()); // ensure we get explicitly set id
            }
            else {
                ASSERT_NE("id2", mod.id());
                ASSERT_NE("", mod.id()); // ensure we get an id, but not the explicitly set one
            }
            ++mod_it;
            ++count;
        }
        ASSERT_EQ(3, count);
    }
}

TEST_F(ConfigModuleTest, test_parrallel_trees_independence)
{
    // Use case: we should be able to place a ConfigNode of the same type in multple places inside a tree
    //           structure and they should function independantly from each other

    ConfigModuleTestClass m("test");

    int test_node_fac_count = 0;
    int factory_tag_fac_count = 0;
    m.add_factory("TestNode", [&test_node_fac_count, &factory_tag_fac_count]()
                              { ConfigModuleTestClass* m = new ConfigModuleTestClass("TestNode", ++test_node_fac_count);
                                               m->add_factory("factory_tag",
                                                        [&factory_tag_fac_count]() {
                                                            return new ConfigModuleTestClass("generated_by_factory", ++factory_tag_fac_count);
                                                        });
                                               return m;
                                             });

    // setup the propert_tree with the correct structure
    boost::property_tree::ptree config_tree;
    boost::property_tree::ptree first_config_tree;
    first_config_tree.put<std::string>(boost::property_tree::ptree::path_type("id"), "tn_id_1"); // explicit id set
    boost::property_tree::ptree second_config_tree;
    second_config_tree.put<std::string>(boost::property_tree::ptree::path_type("id"), "tn_id_2"); // explicit id set

    config_tree.put<int>(boost::property_tree::ptree::path_type("test_option"), 99);
    first_config_tree.put<int>(boost::property_tree::ptree::path_type("test_option"), 188);
    second_config_tree.put<int>(boost::property_tree::ptree::path_type("test_option"), 288);
    boost::property_tree::ptree first_config_tree_factory;
    boost::property_tree::ptree second_config_tree_factory;

    first_config_tree_factory.put<int>(boost::property_tree::ptree::path_type("test_option"), 100);
    first_config_tree_factory.put<std::string>(boost::property_tree::ptree::path_type("id"), "test_id"); // explicit id set
    first_config_tree.add_child(boost::property_tree::ptree::path_type("factory_tag"), first_config_tree_factory);
    second_config_tree_factory.put<int>(boost::property_tree::ptree::path_type("test_option"), 200);
    second_config_tree_factory.put<std::string>(boost::property_tree::ptree::path_type("id"), "test_id"); // identical Ids should not get confused
    second_config_tree.add_child(boost::property_tree::ptree::path_type("factory_tag"), second_config_tree_factory);

    config_tree.add_child(boost::property_tree::ptree::path_type("TestNode"), first_config_tree);
    config_tree.add_child(boost::property_tree::ptree::path_type("TestNode"), second_config_tree);

    // debug output
    boost::property_tree::xml_parser::write_xml(std::cout, config_tree); std::cout << "\n";
    std::cout << m.command_line_options();

    // parse the tree
    boost::program_options::variables_map vm;
    m.parse_property_tree(config_tree, vm);
    boost::program_options::notify(vm);

    // make sure all nodes we expect are generated
    ASSERT_EQ(test_node_fac_count, 2);
    ASSERT_EQ(factory_tag_fac_count, 2);

    ASSERT_EQ(99, dynamic_cast<ConfigModuleTestClass const&>(m)._val);
    auto tn_it = m.subsection("TestNode");
    ASSERT_FALSE(tn_it == m.subsection_end());
    int tn_count=0;
    int tn_val=188;
    int val=100;
    do {
        auto it = tn_it->subsection("factory_tag");
        int count=0;
        while(it != tn_it->subsection_end()) {
            ASSERT_EQ(it->name(), "factory_tag");
            ASSERT_EQ(it->id(), "test_id");
            ASSERT_EQ(val, dynamic_cast<ConfigModuleTestClass const&>(*it)._val) << count;
            ++it;
            ++count;
        }
        ASSERT_EQ(count, 1);
        val += 100;
        ASSERT_EQ(tn_val, dynamic_cast<ConfigModuleTestClass const&>(*tn_it)._val) << count;
        tn_val += 100;
        ++tn_count;
    } while(++tn_it != m.subsection_end());
    ASSERT_EQ(tn_count, 2);
}

} // namespace test
} // namespace panda
} // namespace ska
