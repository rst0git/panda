/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ALGORITHMCONCEPT_H
#define SKA_PANDA_ALGORITHMCONCEPT_H

#include "panda/ArchitectureConcept.h"



/**
 * @brief The AlgorithmConcept class check the requirements on an Algorithm type
 *
 * @details The AlgorithmTuple class extends a std::tuple by adding Architecture type information. The AlgorithmConcept checks that the Architecture type member is present and that it satisfies the requirements of the ArchitectureConcept
 *
 * \ingroup concepts
 * @{
 */

namespace ska {
namespace panda {

template <class X>
struct AlgorithmConcept: Concept<X> {
        typedef Concept<X> Base;

        BOOST_CONCEPT_ASSERT((ArchitectureConcept<typename X::Architecture>));

    public:
        BOOST_CONCEPT_USAGE(AlgorithmConcept) {
            /// An Algorithm concept class must provide an Architecture type member that satisfies the ArchitectureConcept requirements
            Base::template type<typename X::Architecture>::exists();
        }
};

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#endif // SKA_PANDA_ALGORITHMCONCEPT_H
