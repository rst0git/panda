/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_THREADCONFIG_H
#define SKA_PANDA_THREADCONFIG_H

#include "panda/ConfigModule.h"
#include "panda/CommaSeparatedOptions.h"
#include <thread>
#include <string>

namespace ska {
namespace panda {

/**
 * @brief Configuration options for the Thread class
 */
class ThreadConfig : public ConfigModule
{
        typedef ConfigModule BaseT;

    public:
        ThreadConfig(std::string const& tag="cpu");
        ~ThreadConfig();

        /**
         * @brief copy the configured values from the object
         */
        ThreadConfig& operator=(ThreadConfig const&);

        /**
         * @brief get the core affinities of the thread
         */
        std::vector<unsigned> const& affinities() const;

        /**
         * @brief set the core affinities of the thread
         */
        void affinities(std::vector<unsigned> const& affinities);

        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        CommaSeparatedOptions<unsigned> _affinities;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_THREADCONFIG_H
