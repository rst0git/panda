/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "IpAddressTest.h"
#include "panda/IpAddress.h"
#include <boost/asio/ip/udp.hpp>


namespace ska {
namespace panda {
namespace test {


IpAddressTest::IpAddressTest()
    : ::testing::Test()
{
}

IpAddressTest::~IpAddressTest()
{
}

void IpAddressTest::SetUp()
{
}

void IpAddressTest::TearDown()
{
}

TEST_F(IpAddressTest, test_port_string_constructor)
{
    unsigned port = 5000;
    std::string address="88.99.100.101";
    IpAddress ip(port, address);
    ASSERT_EQ(ip.port(), port);
    ASSERT_EQ(ip.address().to_string(), address);
}

TEST_F(IpAddressTest, test_string_constructor)
{
    std::string address="88.99.100.101";
    IpAddress ip(address);
    ASSERT_EQ(ip.port(), 0U);
    ASSERT_EQ(ip.address().to_string(), address);
}

TEST_F(IpAddressTest, test_udp_end_point)
{
    unsigned port = 5000;
    std::string address="88.99.100.101";
    IpAddress ip(port, address);
    boost::asio::ip::udp::endpoint endpoint = ip.end_point<boost::asio::ip::udp::endpoint>();
    ASSERT_EQ(endpoint.address().to_string(), address);
    ASSERT_EQ(endpoint.port(), port);
}

TEST_F(IpAddressTest, test_udp_end_point_with_hostname)
{
    unsigned port = 9021;
    std::string hostname = "localhost";
    IpAddress ip(port, hostname);

    boost::asio::ip::udp::endpoint endpoint = ip.end_point<boost::asio::ip::udp::endpoint>();
    ASSERT_EQ(endpoint.address(), ip.address());
    ASSERT_EQ(endpoint.port(), port);
}


} // namespace test
} // namespace panda
} // namespace ska
