/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "PoolLimitsTest.h"
#include "panda/test/TestSystem.h"
#include "panda/test/TestResourcePool.h"
#include "panda/PoolLimits.h"


namespace ska {
namespace panda {
namespace test {

PoolLimitsTest::PoolLimitsTest()
    : BaseT()
{
}

PoolLimitsTest::~PoolLimitsTest()
{
}

void PoolLimitsTest::SetUp()
{
}

void PoolLimitsTest::TearDown()
{
}

TEST_F(PoolLimitsTest, test_minimum_memory_unsupported_arch)
{
    TestResourcePool<ArchA, ArchB> pool;
    ASSERT_EQ(0U, PoolLimits::minimum_memory<ArchC>(pool));
}

TEST_F(PoolLimitsTest, test_minimum_memory_empty_pool)
{
    TestResourcePool<ArchA, ArchB> pool;
    ASSERT_EQ(0U, PoolLimits::minimum_memory<ArchA>(pool));
}

TEST_F(PoolLimitsTest, test_minimum_memory_pool)
{
    TestResourcePool<ArchA, ArchB> pool;
    pool.add_resource<ArchA>(std::make_shared<PoolResource<ArchA>>(100));
    pool.add_resource<ArchA>(std::make_shared<PoolResource<ArchA>>(50));
    pool.add_resource<ArchB>(std::make_shared<PoolResource<ArchB>>(20));
    ASSERT_EQ(50U, PoolLimits::minimum_memory<ArchA>(pool));
}

} // namespace test
} // namespace panda
} // namespace ska
