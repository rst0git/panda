/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PACKETSTREAMIMPL_H
#define SKA_PANDA_PACKETSTREAMIMPL_H

#include "panda/detail/packet_stream/ChunkerContext.h"
#include "panda/detail/packet_stream/ContextBlockManager.h"
#include "panda/Producer.h"
#include "panda/Connection.h"
#include "panda/ProcessingEngine.h"
#include "panda/Buffer.h"
#include "panda/ResourceManager.h"
#include <memory>
#include <vector>

namespace ska {
namespace panda {
    class IpAddress;
    template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType>
    class PacketStream;

namespace packet_stream {

namespace detail {
    // template generator helpers
    // n.b we expect Producer to be the PacketStreamImplType as this puts no constraints
    // on the DerivedClass having any typedef's exposed public.
    template<typename ProducerType, typename DataTraits>
    class ContextDataFactoryBase {
            typedef std::shared_ptr<typename ProducerType::DataType> Type;

        public:
            // factory for getting new data chunks
            ContextDataFactoryBase(typename ProducerType::DerivedClass* ps)
                : _ps(ps) {}

            Type operator()(typename DataTraits::PacketInspector const& p) { return _ps->template get_chunk<typename ProducerType::DataType>(p); }

            template<typename... Args>
            Type operator()(Args&&... args) { return _ps->template get_chunk<typename DataTraits::DataType>(std::forward<Args>(args)...); }

        protected:
            typename ProducerType::DerivedClass* _ps;
    };

    template<typename ProducerType, typename DataTraits, std::size_t = sizeof(DataTraits)>
    class ContextDataFactory : public ContextDataFactoryBase<ProducerType, DataTraits>
    {
            typedef ContextDataFactoryBase<ProducerType, DataTraits> BaseT;
            typedef DataTraits ContextDataType;

        public:
            ContextDataFactory(typename ProducerType::DerivedClass* ps) : BaseT(ps) {}
    };

    // specialisation will be used only if the DataTraits::ContextData is defined
    template<typename ProducerType, typename DataTraits>
    class ContextDataFactory<ProducerType, DataTraits, 1> : public ContextDataFactoryBase<ProducerType, DataTraits>
    {
            typedef ContextDataFactoryBase<ProducerType, DataTraits> BaseT;

        public:
            ContextDataFactory(typename ProducerType::DerivedClass* ps) : BaseT(ps) {}
    };

} // namespace detail


/**
 * @class PacketStreamImpl
 */
template<class DerivedType, typename ConnectionTraits, typename DataTraits, typename ProducerType=DerivedType>
class PacketStreamImpl : public Producer<ProducerType, typename DataTraits::DataType>
                       , public std::enable_shared_from_this<PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerType>>
{
        /// DataTraits must define the PacketType - a struct representing the bit pattern of the Packet
        //  and supporting the size() method
    private:
        typedef PacketStreamImpl<DerivedType, ConnectionTraits, DataTraits, ProducerType> SelfType;
        typedef std::shared_ptr<SelfType> SelfTypePtr;

        // constructor tags
        struct ConnectionArgsTag { ConnectionArgsTag() {} };

    protected:
        typedef panda::packet_stream::ChunkerContext<DataTraits, detail::ContextDataFactory<SelfType, DataTraits>> Context;
        typedef ContextBlockManager<Context> ContextManager;
        typedef typename Context::LimitType LimitType;
        typedef typename DataTraits::PacketInspector PacketInspectorType;
        typedef typename PacketInspectorType::Packet PacketType;
        typedef typename DataTraits::DataType DataType;

    public:
        typedef std::shared_ptr<Context> ContextPtr;
        typedef DerivedType DerivedClass; // required for factory generator

    private:
        friend class panda::PacketStream<DerivedType, ConnectionTraits, DataTraits, ProducerType>;

        // forward arguments to the connection object
        template<typename... ConnectionArgs>
        PacketStreamImpl(DerivedType& derived, DataTraits const& data_traits_init, ConnectionArgs&&... args);

        /// @brief Contructor accepting Connection Arguments Only
        template<typename... ConnectionArgs>
        PacketStreamImpl(DerivedType& derived, ConnectionArgsTag, ConnectionArgs&&... args);

        PacketStreamImpl(PacketStreamImpl&&);


    public:
        void init() override;
        ~PacketStreamImpl();

        /**
         * @brief set the number of missing packets that are allowed (i.e will receive missing_packet notifiactions)
         *        before the current chunk should be resized and flushed and a new one started
         */
        void missing_packets_before_realign(LimitType num);

        /*
         * @brief setup the purge cadence (number of contexts to wait before a context is purged)
         * @param cadence 0=prev ctx, 1=ctx prior to this etc.
         */
        void purge_cadence(LimitType cadence);

        /**
         * @brief start listening for data
         */
        void start();

        /**
         * @brief stop listening for incomming packets
         */
        void stop();

        /**
         * @brief set the number of ingest buffers
         * @details should roughly match the number of threads available
         */
        void number_of_buffers(unsigned n);

        /**
         * @brief set the endpoint of the peer to connect to
         */
        void set_remote_end_point(typename ConnectionTraits::EndPointType const&);
        void set_remote_end_point(IpAddress const&);

        /**
         * @brief  return the local end point (i.e. port bound to)
         */
        typename ConnectionTraits::EndPointType local_end_point() const;

        /**
         * @brief execute a single event in the event loop before returning
         * @details non-blocking. If there are no events will return immediately
         */
        bool process();

    private:
        friend class detail::ContextDataFactoryBase<SelfType, DataTraits>;

    private:
        typedef Connection<ConnectionTraits> ConnectionType;
        typedef typename ConnectionType::BufferType BufferType;
        typedef ResourceTracker<BufferType> ResourceBufferType;

    private:
        class KeepAliveContext {
            public:
                KeepAliveContext(SelfType* , ResourceBufferType&& b)
                    : _buffer(std::move(b))
                {
                }
                ResourceBufferType& buffer() { return _buffer; }

            private:
                ResourceBufferType _buffer;
                ContextPtr _context;
        };

        typedef std::shared_ptr<KeepAliveContext> KeepAliveContextPtr;

    private:
        void init_buffers();
        void start_chunk();
        void get_packet(KeepAliveContextPtr, panda::Error const& ec, std::size_t bytes_read, std::size_t total_bytes_read, BufferType buffer);
        void process_packet(KeepAliveContextPtr, BufferType buffer);

        /**
         * @brief provide access to the connection
         */
        ConnectionType& connection();

    private:
        bool _halt;
        std::mutex _halt_mutex;
        ConnectionType _connection;
        typename ConnectionTraits::EngineType& _engine;
        ContextBlockManager<Context> _context_mgr;
        std::size_t _max_bmgr;
        std::size_t _post_thread;

        // must be last member as will ensure that the object is
        // not destroyed before all the buffers are returned
        std::vector<std::unique_ptr<ResourceManager<BufferType>>> _buffer_mgr;
};

} // namespace packet_stream
} // namespace panda
} // namespace ska
#include "PacketStreamImpl.cpp"

#endif // SKA_PANDA_PACKETSTREAMIMPL_H
