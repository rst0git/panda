/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CHUNKHOLDER_H
#define SKA_PANDA_CHUNKHOLDER_H

#include "panda/Engine.h"
#include <memory>
#include <utility>
#include <mutex>

namespace ska {
namespace panda {
namespace packet_stream {

/**
 * @brief
 *    A context for use in chucked stream readers, where the packet has an indicator of ordering.
 *    The context retains all the state regarding operations on a specific chunk.
 *
 * @tparam DataTraits : The Data class that defines the payload context. An object of this type will be created if it has any members
 *                      Required to implement the method:
 *
 *                  std::pair<QuotiantType. RemainderType> packets_per_chunk(DataType const&) const;  // QuotiantType & RemainderType = unsigned, size_t etc.
 *                  QuotiantType sequence_number(Packet const&)
 *
 *
 * @details
 *    This context manages the lifetime of a chunk of data whilst it is being filled after its creation by the DataManager.
 *    Chunk sizes are expected to be larger than the packet size.
 *    Chunks are not released to the pipeline for processing until they are marked as full.
 *    Where there are missing packets the chunk is abandoned, and recycled within the
 *    context mechanism without being passed back to the DataManager.
 */

template<typename DerivedType, typename DataTraits, bool is_empty>
class ChunkHolderBase : public std::enable_shared_from_this<DerivedType>
{
    protected:
        template<typename... DataConfigArgs>
        ChunkHolderBase(DataConfigArgs&&... args) : _data(std::forward<DataConfigArgs>(args)...) {}
        typedef double PacketPerChunkType;
        typedef decltype(std::declval<DataTraits>().sequence_number(std::declval<typename DataTraits::PacketInspector const&>())) LimitType;

        // use the return value from packet_size() to set the PacketSizeType
        typedef decltype(std::declval<DataTraits>().packet_size()) PacketSizeType;

        // use the return value from chunk_size() to set the ChunkSizeType
        typedef decltype(std::declval<DataTraits>().chunk_size(std::declval<typename DataTraits::DataType const&>())) ChunkSizeType;

    public:

        /**
         * @brief if the DataTraits has any members then an object of that tyoe will be instantiated
         * @returns the reference to this object
         */
        DataTraits& data() { return _data; }
        DataTraits const& data() const { return _data; }

        /**
         * @brief return the size of the packets
         */
        inline PacketSizeType packet_size()
        {
            return _data.packet_size();
        }

        /**
         * @brief return the size of the chunks to be filled
         */
        inline ChunkSizeType chunk_size(typename DataTraits::DataType const& data)
        {
            return _data.chunk_size(data);
        }

        /**
         * @brief return the offset from the chunk start, used for packet realigment
         */
        inline std::size_t packet_alignment_offset(typename DataTraits::PacketInspector const& packet);

        /**
         * @brief call to find the context sequence number (forwarded to the DataTraits class)
         */
        LimitType sequence_number(typename DataTraits::PacketInspector const&);

        /**
         * @brief call to find the maximum value the sequence number can take
         */
        LimitType max_sequence_number();

    private:
        DataTraits _data;
};


template<typename DerivedType, typename DataTraits>
class ChunkHolderBase<DerivedType, DataTraits, true> : public std::enable_shared_from_this<DerivedType>
{
    protected:
        typedef decltype(std::declval<DataTraits&>().max_sequence_number()) LimitType;
        typedef decltype(std::declval<DataTraits>().packet_size()) PacketSizeType;
        typedef decltype(std::declval<DataTraits>().chunk_size(std::declval<typename DataTraits::DataType const&>())) ChunkSizeType;
        typedef double PacketPerChunkType;

        template<typename... DataConfigArgs>
        ChunkHolderBase(DataConfigArgs&&...) {}

    public:
        /**
         * @brief return the size of the packets
         */
        inline PacketSizeType packet_size()
        {
            return DataTraits::packet_size();
        }

        /**
         * @brief return the size of the chunks to be filled
         */
        inline ChunkSizeType chunk_size(typename DataTraits::DataType const& data)
        {
            return DataTraits::chunk_size(data);
        }

        /**
         * @brief return the offset from the chunk start, used for packet realigment
         */
        inline std::size_t packet_alignment_offset(typename DataTraits::PacketInspector const& packet)
        {
            return DataTraits::packet_alignment_offset(packet);
        }

        LimitType sequence_number(typename DataTraits::PacketInspector const& packet);

        /**
         * @brief call to find the maximum value the sequence number can take
         */
        LimitType max_sequence_number();

};

template<typename DataTraits>
class ChunkHolder : public ChunkHolderBase<ChunkHolder<DataTraits>, DataTraits, std::is_empty<DataTraits>::value>
{
    private:
        typedef ChunkHolder<DataTraits> SelfType;
        typedef ChunkHolderBase<SelfType, DataTraits, std::is_empty<DataTraits>::value> BaseT;

    public:
        typedef typename BaseT::LimitType LimitType;
        typedef typename BaseT::PacketSizeType PacketSizeType;
        typedef typename BaseT::ChunkSizeType ChunkSizeType;

    public:
        typedef typename DataTraits::DataType DataType;

    public:
        template<typename... DataConfigArgs>
        ChunkHolder(panda::Engine& engine, DataConfigArgs&&... args);
        ChunkHolder(ChunkHolder&);
        ChunkHolder(ChunkHolder&&) = delete;
        ~ChunkHolder();

        /**
         * @brief reset for a new chunk
         */
        void reset(std::shared_ptr<DataType>);
        void reset();

        /**
         * @brief mutex to enable locking of a chunk
         * @details this mutex is not used internally
         */
        std::mutex& mutex();

        /**
         * @brief return the chunk data associated with this context
         */
        std::shared_ptr<DataType> const& chunk();

        /**
         * @brief return the chunk size of the current chunk
         */
        inline ChunkSizeType chunk_size() const;

        /**
         * @brief return true if the data chunk has been succesfully reset
         */
        bool reset_complete() const;

        /**
         * @brief return the position of the data offset to write to in the chunk
         * @details this will be either a reference to the offset of the packet start value from the chunk start
         *          or in the case of sequential access (via next()) the next location to be written to.
         */
        inline LimitType offset() const;

        inline PacketSizeType const& packet_size() const;

        /// set the previous ChunkHolder (to allow for ordered data release: previous contexts will always be released first)
        void prev(ChunkHolder&);

        void debug_string(std::ostream& os) const;

        // return the next ChunkHolder
        ChunkHolder& next();

    private:
        volatile bool _reset_complete;

        panda::Engine& _engine;
        PacketSizeType _packet_size;
        ChunkSizeType _chunk_size;
        LimitType _first_offset;
        std::shared_ptr<DataType> _chunk;
        std::mutex _mutex;
        ChunkHolder const* _prev;
        ChunkHolder* _next;
};

} // namespace packet_stream
} // namespace panda
} // namespace ska
#include "ChunkHolder.cpp"

#endif // SKA_PANDA_CHUNKHOLDER_H
