/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/nvidia/DeviceCapability.h"

namespace ska {
namespace panda {
namespace nvidia {

// Static definition of major_version
template<int Major, int Minor, std::size_t Mem>
constexpr int DeviceCapability<Major, Minor, Mem>::major_version;

// Static definition of minor_version
template<int Major, int Minor, std::size_t Mem>
constexpr int DeviceCapability<Major, Minor, Mem>::minor_version;

// Static definition of memory
template<int Major, int Minor, std::size_t Mem>
constexpr std::size_t DeviceCapability<Major, Minor, Mem>::total_memory;

template<int MajorT1, int MinorT1, std::size_t MemT1,
         int MajorT2, int MinorT2, std::size_t MemT2>
constexpr bool operator>(DeviceCapability<MajorT1, MinorT1, MemT1> const& c1,
                         DeviceCapability<MajorT2, MinorT2, MemT2> const& c2)
{
    return (c1.total_memory > c2.total_memory) && (c1.compute_capability() > c2.compute_capability());
}

template<int Major, int Minor, std::size_t TotalMemory>
typename DeviceCapability<Major, Minor, TotalMemory>::CapabilityVersion DeviceCapability<Major, Minor, TotalMemory>::compute_capability()
{
  static CapabilityVersion capability(major_version, minor_version);
  return capability;
}

template<int MajorT1, int MinorT1, std::size_t MemT1,
         int MajorT2, int MinorT2, std::size_t MemT2>
constexpr bool operator>=(DeviceCapability<MajorT1, MinorT1, MemT1> const& c1,
                          DeviceCapability<MajorT2, MinorT2, MemT2> const& c2)
{
    return (c1.total_memory >= c2.total_memory) && (c1.compute_capability() >= c2.compute_capability());
}

} // namespace nvidia
} // namespace panda
} // namespace ska
