/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_NVIDIA_DEVICEGENERATE_H
#define SKA_PANDA_ARCH_NVIDIA_DEVICEGENERATE_H

#include "panda/DeviceGenerate.h"
#include "panda/arch/nvidia/Nvidia.h"

#ifdef SKA_PANDA_ENABLE_CUDA
namespace ska {
namespace panda {

/**
 * @brief Specialisation of DeviceGenerate for thrust
 * @details thrust requires the generator to be a device function
 */
template<typename GeneratorT>
struct DeviceGenerate<nvidia::Cuda, GeneratorT, EnableIfIsArchitectureType<GeneratorT, nvidia::Cuda>>
{
    template<typename SrcIteratorT
           , typename EndIteratorT
           , typename GeneratorArgT>
    static
    void generate(SrcIteratorT&& begin, EndIteratorT&& end, GeneratorArgT&& generator);
};

} // namespace panda
} // namespace ska
#include "detail/DeviceGenerate.cpp"
#endif // SKA_PANDA_ENABLE_CUDA

#endif // SKA_PANDA_ARCH_NVIDIA_DEVICEGENERATE_H
