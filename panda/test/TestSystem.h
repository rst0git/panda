/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTSYSTEM_H
#define SKA_PANDA_TESTSYSTEM_H
#include "panda/System.h"
#include "panda/test/TestResource.h"
#include "panda/DeviceConfig.h"

namespace ska {
namespace panda {
namespace test {

  /**
   * \ingroup testutil
   * @{
   */

struct ArchA {};
struct ArchB {};
struct ArchC {};
} //namespace test

template<>
struct DeviceConfig<test::ArchA> : public DeviceConfigBase<DeviceConfig<test::ArchA>> {
    static std::string const& tag_name();
};

template<>
struct DeviceConfig<test::ArchB> : public DeviceConfigBase<DeviceConfig<test::ArchB>> {
    static std::string const& tag_name();
};

template<>
struct DeviceConfig<test::ArchC> : public DeviceConfigBase<DeviceConfig<test::ArchC>> {
    static std::string const& tag_name();
};

template<>
struct PoolResource<test::ArchA> : public test::TestResource {
    typedef test::TestResource BaseT;

    PoolResource(std::size_t memory=0)
        : BaseT(memory) {}

    ~PoolResource() {}
};

template<>
struct PoolResource<test::ArchB> : public test::TestResource {
    typedef test::TestResource BaseT;

    PoolResource(std::size_t memory=0)
        : BaseT(memory) {}

    ~PoolResource() {}
};

template<>
struct DiscoverResources<test::ArchA>
{
    inline std::vector<std::shared_ptr<PoolResource<test::ArchA>>> operator()() { return { std::make_shared<PoolResource<test::ArchA>>(), std::make_shared<PoolResource<test::ArchA>>()}; }
};

template<>
struct DiscoverResources<test::ArchB>
{
    inline std::vector<std::shared_ptr<PoolResource<test::ArchB>>> operator()() { return { std::make_shared<PoolResource<test::ArchB>>(), std::make_shared<PoolResource<test::ArchB>>()}; }
};

template<>
struct DiscoverResources<test::ArchC>
{
    // none available
    inline std::vector<std::shared_ptr<PoolResource<test::ArchC>>> operator()() { return {}; }
};

namespace test {

  /**
   * @brief
   *   A preconfigured System with architecture ArchA, ArchB and ArchC defined
   *   ArchA - 2 resources
   *   ArchB - 2 resources
   *   ArchC - 0 resources
   *
   * @details
   *
   */
class TestSystem : public panda::System<ArchA, ArchB, ArchC>
{
    public:
        TestSystem();
        ~TestSystem();

    private:
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_TESTSYSTEM_H
