/*
 * The MIT License (MIT)
 *
 * Fillright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR FILLRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DEVICEFILL_H
#define SKA_PANDA_DEVICEFILL_H

#include "panda/Configuration.h"
#include "panda/Architecture.h"

namespace ska {
namespace panda {

/**
 * @brief provide specialisations of this class to extend panda::fill
 * @details
 *    specialise by the Architectres
 */
template<class ArchT>
struct DeviceFill
{
    public:
        /**
         * @brief perform the fill on the destination architecture @Arch
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename ValueT>
        static inline
        void fill(SrcIteratorType&& begin, EndIteratorType&& end, ValueT const&);

};

// specialization to extract Architecture from the DeviceFiller type
template<class ArchT>
struct ArchitectureType<DeviceFill<ArchT>>
{
    typedef ArchT type;
};

} // namespace panda
} // namespace ska

#ifdef SKA_PANDA_ENABLE_CUDA
#include "panda/arch/nvidia/DeviceFill.h"
#endif // SKA_PANDA_ENABLE_CUDA
#ifdef SKA_PANDA_ENABLE_OPENCL
//#include "panda/arch/altera/DeviceFill.h"
#endif // SKA_PANDA_ENABLE_OPENCL
#include "detail/DeviceFill.cpp"

#endif // SKA_PANDA_DEVICEFILL_H
