/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DeviceConfig.h"


namespace ska {
namespace panda {


template<typename Derived>
DeviceConfigBase<Derived>::DeviceConfigBase(std::string const& tag)
    : ConfigModule(tag)
    , _device_count(1U)
{
}

template<typename Derived>
DeviceConfigBase<Derived>::~DeviceConfigBase()
{
}

template<typename Derived>
DeviceConfigBase<Derived>::DeviceConfigBase(DeviceConfigBase const& copy)
    : ConfigModule(copy.name())
    , _device_count(copy._device_count)
{
}

template<typename Derived>
void DeviceConfigBase<Derived>::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("devices", boost::program_options::value<std::size_t>(&_device_count)->default_value(_device_count), "the number fo devices with this configuration");
}

template<typename Derived>
std::size_t DeviceConfigBase<Derived>::device_count() const
{
    return _device_count;
}

} // namespace panda
} // namespace ska
