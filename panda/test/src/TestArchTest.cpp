/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "TestArchTest.h"
#include "panda/test/TestArch.h"
#include "panda/test/DeviceMemoryTester.h"
#include "panda/test/AllocatorTester.h"


namespace ska {
namespace panda {
namespace test {


TestArchTest::TestArchTest()
    : BaseT()
{
}

TestArchTest::~TestArchTest()
{
}

void TestArchTest::SetUp()
{
}

void TestArchTest::TearDown()
{
}

// run DeviceAllocator Tests
typedef ::testing::Types<test::AllocatorTesterTraits<DeviceAllocator<float, TestArch>>> TestArchAllocatorTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(TestArchDeviceAllocator, AllocatorTester, TestArchAllocatorTraitsTypes);

// run DeviceMemory Tests
typedef ::testing::Types<test::DeviceMemoryTesterTraits<DeviceMemory<TestArch, float>>> TestArchDeviceMemoryTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(TestArchDeviceMemory, DeviceMemoryTester, TestArchDeviceMemoryTraitsTypes);


} // namespace test
} // namespace panda
} // namespace ska
