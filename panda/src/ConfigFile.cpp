/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/PropertyTreeUtils.h"
#include "panda/ConfigFile.h"
#include "panda/Error.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#pragma GCC diagnostic ignored "-Wtype-limits"
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/algorithm/string.hpp> // iequals
#pragma GCC diagnostic pop
#include <iostream>
#include <fstream>

namespace ska {
namespace panda {

ConfigFile::ConfigFile(std::string filename)
    : _filename(std::move(filename))
{
    // determine the file type from extension
    if(boost::iequals(_filename.extension().c_str(), ".ini") ) {
        _type = FileType::Ini; 
    }
    else if(boost::iequals(_filename.extension().c_str(), ".json") ) {
        _type = FileType::Json; 
    }
    else if(boost::iequals(_filename.extension().c_str(), ".xml") ) {
        _type = FileType::Xml; 
    }
    else if(_filename.has_extension()){
        std::cerr << "unknown config file extension " << _filename.extension() << std::endl;
        _type = FileType::None; 
    }
    else {
        // no extension - default to xml
        _type = FileType::Xml; 
    }
}

ConfigFile::~ConfigFile()
{
}

void ConfigFile::set_xml()
{
    _type = FileType::Xml;
}

void ConfigFile::set_json()
{
    _type = FileType::Json;
}

void ConfigFile::set_ini()
{
    _type = FileType::Ini;
}

boost::filesystem::path const& ConfigFile::path() const 
{
    return _filename;
}

boost::property_tree::ptree const& ConfigFile::property_tree() const
{
    return _dom;
}

ConfigFile::FileType ConfigFile::type() const
{
    return _type;
}

void ConfigFile::parse()
{
    if(boost::filesystem::exists(_filename) && boost::filesystem::is_regular_file(_filename)) 
    {
        std::ifstream is(_filename.native(), std::ifstream::in);
        parse(is);
    }
    else {
        panda::Error e("not a configuration file :");
        e << _filename;
        throw e;
    }
}

void ConfigFile::parse(std::istream& is)
{
    // read in ptree
    switch(_type) {        
        case FileType::Xml:
            boost::property_tree::xml_parser::read_xml(is, _dom);
            break;
        case FileType::Json:
            boost::property_tree::json_parser::read_json(is, _dom);
            break;
        case FileType::Ini:
            boost::property_tree::ini_parser::read_ini(is, _dom);
            break;
        case FileType::None:
            break;
    }
    // map to program options
}

} // namespace panda
} // namespace ska
