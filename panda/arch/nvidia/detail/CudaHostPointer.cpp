/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Configuration.h"
#include "panda/arch/nvidia/CudaHostPointer.h"
#include "panda/Error.h"
#include <cuda_runtime.h>


namespace ska {
namespace panda {
namespace nvidia {


template<typename DataType>
CudaHostPointer<DataType>::CudaHostPointer(std::size_t size)
    : _h_data(nullptr)
    , _size(size)
{
#ifdef SKA_PANDA_ENABLE_CUDA
    auto error = cudaMallocHost((void**)&_h_data, size*sizeof(DataType));
    if(error != cudaSuccess) {
        throw panda::Error(cudaGetErrorString(error));
    }
#else
    _h_data = malloc(size * sizeof(DataType));
    if(!_h_data) {
        throw panda::Error("panda::CudaHostPointer: unable to allocate memory");
    }
#endif
}

template<typename DataType>
CudaHostPointer<DataType>::CudaHostPointer(CudaHostPointer&& c)
{
    _h_data = c._h_data;
    c._h_data=nullptr;
}

template<typename DataType>
CudaHostPointer<DataType>::~CudaHostPointer()
{
#ifdef SKA_PANDA_ENABLE_CUDA
    cudaFreeHost(_h_data);
#else
    free(_h_data);
#endif
}

template<typename DataType>
DataType* CudaHostPointer<DataType>::operator()() { return _h_data; }

template<typename DataType>
std::size_t CudaHostPointer<DataType>::size() const
{
    return _size;
}

template<typename DataType>
DataType* CudaHostPointer<DataType>::begin() { return _h_data; }

template<typename DataType>
DataType* CudaHostPointer<DataType>::end() { return _h_data + _size; }

template<typename DataType>
const DataType* CudaHostPointer<DataType>::begin() const { return _h_data; }

template<typename DataType>
const DataType* CudaHostPointer<DataType>::end() const { return _h_data + _size; }

} // namespace nvidia
} // namespace panda
} // namespace ska
