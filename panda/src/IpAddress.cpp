/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/IpAddress.h"
#include "panda/Log.h"
#include "panda/Error.h"
#include <stdlib.h>

namespace ska {
namespace panda {


IpAddress::IpAddress(std::string const& address)
    : _port(0)
{
    init(address);
}

IpAddress::IpAddress(boost::asio::ip::address const& address, unsigned port)
    : _port(port)
    , _address(address)
{
}

IpAddress::IpAddress(unsigned port, std::string const& address)
    : _port(port)
{
    init(address);
}

IpAddress::~IpAddress()
{
}

void IpAddress::init(std::string const& address)
{
    boost::system::error_code ec;
    boost::asio::ip::address::from_string(address, ec);
    if (ec) {
        _hostname = address;
    }
    else {
        _address = boost::asio::ip::address::from_string(address);
    }
}

boost::asio::ip::address const & IpAddress::address() const
{
    return _address;
}

void IpAddress::address(boost::asio::ip::address const & address)
{
    _address = address;
}

unsigned IpAddress::port() const
{
    return _port;
}

void IpAddress::port(unsigned port)
{
    _port = port;
}

bool IpAddress::operator==(IpAddress const& addr) const {return _port==addr._port && _address==addr._address;}
bool IpAddress::operator!=(IpAddress const& addr) const {return _port!=addr._port || _address!=addr._address;}

} // namespace panda
} // namespace ska
