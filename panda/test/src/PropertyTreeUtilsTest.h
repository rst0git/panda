/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TEST_PROPERTYTREEUTILSTEST_H
#define SKA_PANDA_TEST_PROPERTYTREEUTILSTEST_H

#include <gtest/gtest.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#include <boost/property_tree/ptree.hpp>
#pragma GCC diagnostic pop


namespace ska {
namespace panda {
namespace test {

/**
 * @brief
 *   unit test for PropertyTreeUtils
 * @details
 * 
 */
class PropertyTreeUtilsTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        PropertyTreeUtilsTest();
        ~PropertyTreeUtilsTest();

    protected:
        const int _int_val_1=999;
        const int _int_val_2=1999;
        const int _int_val_3=3999;
        const std::string _str_val_1;
        boost::property_tree::ptree _tree;
    private:
};

} // namespace test
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_TEST_PROPERTYTREEUTILSTEST_H 
