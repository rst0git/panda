/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_LOGLINESENTRY_H
#define SKA_PANDA_LOGLINESENTRY_H

#include "panda/detail/LogLine.h"
#include "panda/detail/LogLineHolder.h"
#include "panda/detail/LogLineHolderSentry.h"

namespace ska {
namespace panda {

/**
 * @brief
 *    Sentry object for pushing Loglines to the output channel
 */
class LogLineSentry
{
    private:
        LogLineHolder& _logline;

    public:
        LogLineSentry(Logger& logger, LogLineHolder& logline);
        ~LogLineSentry();

        inline LogLineHolder& logline() const { return _logline; }

        template<typename T>
        inline auto send(T&& t) const
        -> decltype( _logline << std::forward<T>(t) )
        {
            return _logline << std::forward<T>(t);
        }

        template<typename T>
        inline auto operator<<(T&& t) const
        -> decltype( send(std::forward<T>(t)) )
        {
            return send(std::forward<T>(t));
        }
};

#define PANDA_GENERATE_LOG_ENTRY_IMPL(logger, line, file) PANDA_DEFINE_STATIC_LOGLINE_VARIABLE( line , file ); \
                                                          ska::panda::LogLineSentry(logger,PANDA_STATIC_LOGLINE_VARIABLE_NAME(line, file)).logline()
#define PANDA_GENERATE_LOG_ENTRY(logger) PANDA_GENERATE_LOG_ENTRY_IMPL(logger, __LINE__, __FILE__)

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_LOGLINESENTRY_H 
