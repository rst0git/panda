/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/altera/OpenClPlatform.h"
#include "panda/arch/altera/OpenClError.h"
#include "panda/Error.h"
#include "panda/Log.h"


namespace ska {
namespace panda {
namespace altera {


OpenClPlatform::OpenClPlatform(cl_platform_id const& platform_id)
    : _platform_id(platform_id)
{
}

OpenClPlatform::~OpenClPlatform()
{
    #ifndef CL_USE_DEPRECATED_OPENCL_1_2_APIS
        PANDA_LOG_DEBUG << "unloading opencl compiler " << name();
        clUnloadPlatformCompiler(_platform_id);
    #endif // CL_USE_DEPRECATED_OPENCL_1_2_APIS
}

const char* OpenClPlatform::name() const
{
    if(_platform_name.empty()) {
        std::size_t sz;
        OpenClError status = clGetPlatformInfo(_platform_id, CL_PLATFORM_NAME, 0, NULL, &sz);
        if (status) {
            panda::Error e("Query for platform name size failed!");
            e << status.what();
            throw e;
        }

        _platform_name.resize(sz+1);
        _platform_name[sz]='\0';
        status = clGetPlatformInfo(_platform_id, CL_PLATFORM_NAME, sz, _platform_name.data(), NULL);
        if (status) {
            panda::Error e("Query for platform name failed");
            e << status.what();
            throw e;
        }
    }
    return _platform_name.data();
}

std::vector<cl_device_id> OpenClPlatform::device_ids() const
{
    cl_uint num_devices = 0;
    std::vector<cl_device_id> dids(num_devices); // = new cl_device_id[num_devices];
    OpenClError status = clGetDeviceIDs(_platform_id, CL_DEVICE_TYPE_ALL, 0, NULL, &num_devices);
    if (status) {
        if(status == CL_DEVICE_NOT_FOUND) return dids;
        panda::Error e("Query for number of devices failed for platform: ");
        if(_platform_name.size()>0)
        {
            e << name();
        } else {
            e << _platform_id;
        }
        e << " " << status.what();
        throw e;
    }

    dids.resize(num_devices);
    status = clGetDeviceIDs(_platform_id, CL_DEVICE_TYPE_ALL, num_devices, &dids[0], NULL);
    if (status) {
        panda::Error e("Query for device ids failed on platform: ");
        if(_platform_name.size()>0)
        {
            e << name();
        } else {
            e << _platform_id;
        }
        e << " " << status.what();
        throw e;
    }

    return dids;
}

} // namespace altera
} // namespace panda
} // namespace ska
