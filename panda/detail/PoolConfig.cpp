/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/PoolConfig.h"
#include "panda/TupleUtilities.h"


namespace ska {
namespace panda {
namespace detail {

    // counts devices form underlying DeviceConfig objects
    template<typename Arch>
    struct DeviceAccumulator {
        DeviceAccumulator() {}

        template<typename... Architectures>
        void operator()(PoolConfig<Architectures...> const& pc, std::size_t& result) {
            result += pc.template device_count<Arch>();
        }
    };

    template<typename Arch>
    struct FactoryHelper {
        template<typename... Architectures>
        inline void operator()(PoolConfig<Architectures...>& pc) const {
            pc.add_factory( DeviceConfig<Arch>::tag_name(), [](){ return new DeviceConfig<Arch>(); });
        }
    };
} // namespace detail


template<typename... Architectures>
PoolConfig<Architectures...>::PoolConfig(std::string name)
    : ConfigModule(std::move(name))
{
    panda::ForEach<std::tuple<Architectures...>, detail::FactoryHelper>::exec(*this);
}

template<typename... Architectures>
PoolConfig<Architectures...>::~PoolConfig()
{
}

template<typename... Architectures>
void PoolConfig<Architectures...>::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("concurrent_jobs", boost::program_options::value<unsigned>()->default_value(0U)->notifier(
        [this](unsigned val) {
            if(val) {
                _engine_config.number_of_threads(val);
            } else {
                _engine_config.number_of_threads(total_device_count());
            }
        })
        ,"restrict the number of dedicated threads (simultaneous jobs) that each pool can run (0=unlimited).");
}

template<typename... Architectures>
unsigned PoolConfig<Architectures...>::total_device_count() const
{
    // n.b. each each node can represent the configuration for more than one device
    std::size_t result = 0;
    panda::ForEach<std::tuple<Architectures...>, detail::DeviceAccumulator>::exec(*this, result);
    return result;
}

template<typename... Architectures>
template<typename Architecture>
std::size_t PoolConfig<Architectures...>::device_count() const
{
    unsigned result=0U;
    auto it = subsection(tag<Architecture>());
    while( it != subsection_end())
    {
        result += static_cast<DeviceConfig<Architecture> const&>(*it).device_count();
        ++it;
    }
    return result;
}

template<typename... Architectures>
ProcessingEngineConfig& PoolConfig<Architectures...>::engine_config()
{
    return _engine_config;
}

template<typename... Architectures>
ProcessingEngineConfig const& PoolConfig<Architectures...>::engine_config() const
{
    return _engine_config;
}

template<typename... Architectures>
template<typename Architecture>
std::string const& PoolConfig<Architectures...>::tag()
{
    return DeviceConfig<Architecture>::tag_name();
}

} // namespace panda
} // namespace ska
