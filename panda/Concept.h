/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONCEPTBASE_H
#define SKA_PANDA_CONCEPTBASE_H

#include <boost/concept_check.hpp>


/**
 * @brief Provides functionality useful to the development of Concept classes
 *
 * @details For usage examples look in *Concept.h
 *
 * \ingroup concepts
 * @{
 */

namespace ska {
namespace panda {

template<typename X>
struct Concept {
    /// Provide a non-const reference without relying in construction of any kind
    template<typename T=X> T& reference(); // no impl, because never called
        
    /// A simple way to use a variable in case compiler options do not permit unused variables
    template<typename T> static void use(T); // no impl

    /// A simple way to use a type in case compiler options do not permit unused type
    static void use();// {}

    /// This function will not compile if its arguments have different types
    template<typename T> static void is_same(T,T); // no impl

    /// This function will not compile if 'T' does not exist. It is a tidy way to check the existence of type you do not
    /// intend to use when compiler options specify types must be used.
    template<typename T> struct type {
        T* t=nullptr;

        static void exists();
    };

};

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#include "panda/detail/Concept.cpp"



#endif // SKA_PANDA_CONCEPTBASE_H
