if(NOT SKA_PANDA_GIT_PRE_COMMIT_HOOKS_GUARD_VAR)
    set(SKA_PANDA_GIT_PRE_COMMIT_HOOKS_GUARD_VAR TRUE)
else()
    return()
endif()

#========================================================
# join
#
# brief: join the values in a list into a single string
#
# usage: join(values_to_join field_separator output_string)
#========================================================
function(join values glue OUTPUT)
    string(REPLACE ";" "${glue}" tmp_str "${values}")
    set(${OUTPUT} "${tmp_str}" PARENT_SCOPE)
endfunction(join)

#========================================================
# path_difference
#
# brief: find the difference between two paths
#
# usage: path_difference(result path_a path_b)
#
# detail: path_a should be the higher-level directory
#========================================================
function(path_difference RESULT path_a path_b)
    set(test_path ${path_b})
    while(NOT (("${test_path}" STREQUAL "${path_a}") OR ("${test_path}" STREQUAL "")))
        get_filename_component(dir ${test_path} NAME)
        list(APPEND dpath ${dir})
        get_filename_component(test_path ${test_path} DIRECTORY)
    endwhile()
    if(dpath)
        list(REVERSE dpath)
        join("${dpath}" "/" ${RESULT})
        set(RESULT ${RESULT} PARENT_SCOPE)
    endif(dpath)
endfunction(path_difference)

# Set up the git pre-commit hook
set(hooks_path "${PROJECT_SOURCE_DIR}/.git/hooks")
if(NOT EXISTS "${hooks_path}/pre-commit")
    if(EXISTS "${hooks_path}/pre-commit.sample")
        configure_file("${hooks_path}/pre-commit.sample" "${hooks_path}/pre-commit" COPYONLY)
    else()
        # We are probably a submodule
        path_difference(SUBMODULE_PATH "${CMAKE_SOURCE_DIR}" "${PROJECT_SOURCE_DIR}")
        set(hooks_path "${CMAKE_SOURCE_DIR}/.git/modules/${SUBMODULE_PATH}/hooks")
        if(EXISTS "${hooks_path}/pre-commit.sample")
            if(NOT EXISTS "${hooks_path}/pre-commit")
                configure_file("${hooks_path}/pre-commit.sample" "${hooks_path}/pre-commit" COPYONLY)
            endif()
        endif()
    endif()
endif()
