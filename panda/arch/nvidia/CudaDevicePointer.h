/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CUDADEVICEPOINTER_H
#define SKA_PANDA_CUDADEVICEPOINTER_H

#include "panda/arch/nvidia/Architecture.h"
#include "panda/arch/nvidia/DeviceAllocator.h"
#include "panda/arch/nvidia/DeviceIterator.h"
#include <cstdlib>

namespace ska {
namespace panda {
namespace nvidia {

/**
 * @brief
 *    manages a Cuda malloced pointer
 * @details
 *
 */
template<typename DataType, class Allocator=DeviceAllocator<DataType, Cuda>>
class CudaDevicePointer
{
    public:
        CudaDevicePointer(std::size_t size);
        CudaDevicePointer(std::size_t size, Allocator const& allocator);
        CudaDevicePointer(CudaDevicePointer const&) = delete;
        CudaDevicePointer(CudaDevicePointer&&);
        ~CudaDevicePointer();

        /**
         * @brief return the underlying raw pointer
         */
        inline DataType* operator()();
        std::size_t size() const;

        /**
         * @brief copy from the device to the iterator destination on the host
         * @details IteratorType has to be of category std::random_access_iterator_tag
         */
        template<typename IteratorType>
        void read(IteratorType&& destintation) const;

        /**
         * @brief copy from the host to the device data from the iterator range
         * @details IteratorType has to be of category std::random_access_iterator_tag
         */
        template<typename IteratorType, typename EndIteratorType>
        void write(IteratorType&& begin, EndIteratorType&& end);

        /**
         * @brief return a (cuda device) pointer to the first element
         */
        panda::DeviceIterator<Cuda, DataType*> begin();

        /**
         * @brief return a (cuda device) pointer to the last element + 1
         */
        panda::DeviceIterator<Cuda, DataType*> const end() const;

    private:
        Allocator _allocator;
        DataType* _d_ptr;
        std::size_t _size;
};

} // namespace nvidia

// template to determining arch types
template<typename... T>
struct ArchitectureType<nvidia::CudaDevicePointer<T...>>
{
    typedef nvidia::Cuda type;
};

} // namespace panda
} // namespace ska
#include "panda/arch/nvidia/detail/CudaDevicePointer.cpp"

#endif // SKA_PANDA_CUDADEVICEPOINTER_H
