/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CPU_H
#define SKA_PANDA_CPU_H

#include "panda/Architecture.h"
#include "panda/CommaSeparatedOptions.h"
#include "panda/DiscoverResources.h"
#include "panda/DeviceConfig.h"
#include "panda/Resource.h"
#include "panda/Thread.h"
#include <string>

namespace ska {
namespace panda {

/**
 * @brief
 *    Cpu - maps roughly on to a thread - one per core
 * @details
 *
 */
class Cpu : public Architecture
{
    public:
        Cpu();
        ~Cpu();
};

template<>
struct DeviceConfig<Cpu> : public DeviceConfigBase<DeviceConfig<Cpu>>
{
        typedef DeviceConfigBase<DeviceConfig<Cpu>> BaseT;

    public:
        DeviceConfig();
        DeviceConfig(DeviceConfig const&);
        DeviceConfig& operator=(DeviceConfig const&);
        static std::string const& tag_name();

        void add_options(OptionsDescriptionEasyInit& add_options);

        std::vector<unsigned> const& affinities() const;
        void affinities(std::vector<unsigned> const &);

    private:
        CommaSeparatedOptions<unsigned> _affinities;
};

/**
 * @brief
 *    CPU thread conforming to ResourcePool requirements
 */
template<>
class PoolResource<Cpu> : public Resource
{
    public:
        PoolResource(unsigned int id);
        virtual ~PoolResource();

        void abort() override {};

        template<typename TaskType>
        void run(TaskType job) { job(*this); }

        template<typename TaskType>
        void exec(TaskType job) { _thread.exec(std::forward<TaskType>(job)); }

        void configure(DeviceConfig<Cpu> const&);

        std::vector<unsigned> const& affinities() const;

        std::size_t device_memory() const;

    private:
        std::vector<unsigned> _affinities;
        Thread _thread;
};

/**
 * @brief
 *    CPU thread resource for each core
 */
template<>
struct DiscoverResources<Cpu> {
    std::vector<std::shared_ptr<PoolResource<Cpu>>> operator()();
};

/**
 * @brief specialisation for determining Architecture type
 */
template<>
struct ArchitectureType<Cpu> {
    typedef Cpu type;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_CPU_H
