/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Server.h"
#include "panda/Stream.h"
#include "panda/IpAddress.h"

namespace ska {
namespace panda {

template<typename ServerTraits, class ServerApp>
Server<ServerTraits, ServerApp>::Server(ServerConfig const & settings, ServerApp& app )
    : _settings(settings)
    , _run(false)
    , _app(app)
    , _listen_socket(engine(), EndPointType(_settings.listen_address().address(), _settings.listen_address().port()))
    , _recv_buffer(1)
{
    boost::system::error_code ec;
    _listen_socket.set_option(boost::asio::socket_base::reuse_address(true), ec); // TODO log any errors
    _start_receive();
}

template<typename ServerTraits, class ServerApp>
Server<ServerTraits, ServerApp>::~Server()
{
}

template<typename ServerTraits, class ServerApp>
typename Server<ServerTraits, ServerApp>::EndPointType Server<ServerTraits, ServerApp>::end_point() const
{
    return _listen_socket.local_endpoint();
}

template<typename ServerTraits, class ServerApp>
int Server<ServerTraits, ServerApp>::run()
{
    try {
        _run = true;
        while(_run) {
            engine().run_one();
            engine().reset();
        }
        return 0;
    }
    catch(...) {
        return 1;
    }
}

template<typename ServerTraits, class ServerApp>
bool Server<ServerTraits, ServerApp>::is_running() const
{
    return _run;
}

/*
template<typename ServerTraits, class ServerApp>
int Server<ServerTraits, ServerApp>::process_events()
{
    try {
        engine().run();
        engine().reset();
        return 0;
    }
    catch(...) {
        return 1;
    }
}
*/

template<typename ServerTraits, class ServerApp>
void Server<ServerTraits, ServerApp>::stop()
{
    _run = false;
    _app.stop();
    _listen_socket.cancel();
    engine().stop();
}

template<typename ServerTraits, class ServerApp>
Engine& Server<ServerTraits, ServerApp>::engine()
{
    return _settings.engine();
}

/**
 * UDP specialisation
 */
template<typename ServerTraits, class ServerApp>
void Server<ServerTraits, ServerApp>::_start_receive()
{
    typename ServerTraits::Session client_session = std::make_shared<ServerClient<typename ServerTraits::TraitsType>>(_listen_socket);
    _listen_socket.async_receive_from(
        boost::asio::buffer(_recv_buffer),
        client_session->end_point(),
        [this, client_session](boost::system::error_code const & error,
           std::size_t const & /*bytes_sent*/)
        {
            if (!error || error == boost::asio::error::message_size)
            {
                _app.on_connect(client_session);

                // wait for some other client
                _start_receive();

            }
            else {
                _app.on_error(client_session, error);
            }

        }
    );
}

} // namespace panda
} // namespace ska
