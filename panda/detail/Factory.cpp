/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Factory.h"
#include "panda/Error.h"
#include <memory>

template<typename TypeFactory>
class MixinGenerator {
    public:
        typedef typename std::remove_pointer<decltype(std::declval<TypeFactory>()())>::type GeneratedType;

    public:
        MixinGenerator(TypeFactory& factory) 
            : _factory(factory) {}

        GeneratedType* create() {
            return _factory();
        }

        template<typename Type>
        Type* create_mixin() {
            std::unique_ptr<GeneratedType> ptr(_factory());
            return new Type(std::move(*ptr)); 
        }

    private:
        TypeFactory _factory;
};

namespace ska {
namespace panda {

template<typename BaseType>
Factory<BaseType>::Factory(std::string error_prefix)
    : _prefix(std::move(error_prefix))
{
}

template<typename BaseType>
Factory<BaseType>::~Factory()
{
}

template<typename BaseType>
template<typename TypeFactory>
void Factory<BaseType>::add_type(std::string const& type_name, TypeFactory factory)
{
    // base types
    _types.push_back(type_name);
    _map.insert(std::make_pair(type_name, static_cast<Factory<BaseType>::FactoryType>(factory)));

    // generate functions for mixin types. We need to generate these functions before we lose the full type information
    // add a timer type. n.b these only work if there is a copy constructor
    /*
    typedef typename std::remove_pointer<decltype(std::declval<TypeFactory>()())>::type Type;
    _timed_map.insert(std::make_pair(type_name, [factory]() { std::unique_ptr<Type> ptr(factory());
                                                              return new panda::MixInTimer<Type>(std::move(*ptr)); 
                                                            } ));
    // add a timer type that takes a handler argument
    _timed_handler_map.insert(std::make_pair(type_name, [factory](typename MixInTimer<BaseType>::Handler const& handler) { std::unique_ptr<Type> ptr(factory());
                                                                      return new panda::MixInTimer<Type>(handler, std::move(*ptr)); 
                                                                    } ));
    */
}

template<typename BaseType>
BaseType* Factory<BaseType>::create(std::string const& type_name) const
{
    auto it = _map.find(type_name);
    if(it == _map.end())
        throw panda::Error(_prefix + " '" + type_name + "' unknown");

    return it->second();
}

/*
template<typename BaseType>
MixInTimer<BaseType>* Factory<BaseType>::create_timed(std::string const& type_name) const
{
    auto it = _timed_map.find(type_name);
    if(it == _timed_map.end())
        throw panda::Error(_prefix + " '" + type_name + "' unknown");

    return reinterpret_cast<MixInTimer<BaseType>*>(it->second());
}

template<typename BaseType>
MixInTimer<BaseType>* Factory<BaseType>::create_timed(std::string const& type_name, typename MixInTimer<BaseType>::Handler const& handler) const
{
    auto it = _timed_handler_map.find(type_name);
    if(it == _timed_handler_map.end())
        throw panda::Error(_prefix + " '" + type_name + "' unknown");

    return reinterpret_cast<MixInTimer<BaseType>*>(it->second(handler));
}
*/

} // namespace panda
} // namespace ska
