/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Buffer.h"
#include "panda/Copy.h"
#include "panda/Cpu.h"
#include "panda/DeviceCopy.h"
#include "panda/DeviceMemory.h"
#include "panda/TupleUtilities.h"
#include "panda/detail/DeviceCopy.cpp"
#include <algorithm>
#include <iterator>


namespace ska {
namespace panda {


// --------- DeviceCopyPostProcessor --------
template<typename Arch, typename BaseSrcIteratorType, typename BaseEndIteratorType, typename BaseDstIteratorType, typename IteratorCategory=typename std::iterator_traits<typename std::remove_reference<BaseDstIteratorType>::type>::iterator_category>
class DeviceCopyPostProcessor
{
        typedef std::iterator_traits<typename std::remove_reference<BaseDstIteratorType>::type> DstIteratorTraits;

    public:
        /**
         * @brief copy from device to host where the destination is not a contiguos memory chunk
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static inline typename std::remove_reference<DstIteratorType>::type copy_from_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
        {
            // create a buffer for the initial transfer
            panda::Buffer<typename DstIteratorTraits::value_type> buffer(std::distance(begin,end));
            DeviceCopy<Arch, Cpu, SrcIteratorType, EndIteratorType, DstIteratorType>::copy(std::forward<SrcIteratorType>(begin),
                                        std::forward<EndIteratorType>(end),
                                        buffer.begin());
            return std::copy(buffer.begin(), buffer.end(), std::forward<DstIteratorType>(dst_it));
        }
};

template<typename Arch, typename BaseSrcIteratorType, typename BaseEndIteratorType, typename BaseDstIteratorType>
class DeviceCopyPostProcessor<Arch, BaseSrcIteratorType, BaseEndIteratorType, BaseDstIteratorType, std::random_access_iterator_tag>
{
    public:
        /**
         * @brief copy from device to host where the destination is a contiguos memory chunk
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static inline typename std::remove_reference<DstIteratorType>::type copy_from_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
        {
            return DeviceCopy<Arch, Cpu, typename std::decay<SrcIteratorType>::type, typename std::decay<EndIteratorType>::type, typename std::decay<DstIteratorType>::type>::copy(std::forward<SrcIteratorType>(begin),
                                        std::forward<EndIteratorType>(end),
                                        std::forward<DstIteratorType>(dst_it));
        }
};

// --------- DeviceCopyPreProcessor --------
/**
 * @brief
 *    Helper class for preparing data to be transferred to a device
 */
template<typename Arch, typename BaseSrcIteratorType, typename BaseEndIteratorType, typename IteratorCategory=typename std::iterator_traits<typename std::remove_reference<BaseSrcIteratorType>::type>::iterator_category>
class DeviceCopyPreProcessor
{
    public:
        /**
         * @brief copy from host to device
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static inline typename std::remove_reference<DstIteratorType>::type copy_to_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
        {
            std::vector<typename std::iterator_traits<typename std::remove_reference<SrcIteratorType>::type>::value_type> local_mem;
            std::copy(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), std::back_inserter(local_mem)); // we need memory to be contiguous for cudaMemcpy to be efficient
            typedef typename decltype(local_mem)::const_iterator IteratorType;
            return DeviceCopyPreProcessor<Arch, IteratorType, IteratorType>::copy_to_device(
                        local_mem.cbegin(),
                        local_mem.cend(),
                        std::forward<DstIteratorType>(dst_it));
        }
};

template<typename Arch, typename BaseSrcIteratorType, typename BaseEndIteratorType>
class DeviceCopyPreProcessor<Arch, BaseSrcIteratorType, BaseEndIteratorType, std::random_access_iterator_tag>
{
    public:
        /**
         * @brief copy from host to device
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static inline typename std::remove_reference<DstIteratorType>::type  copy_to_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
        {
            return DeviceCopy<Cpu, Arch, SrcIteratorType, EndIteratorType, DstIteratorType>::copy(
                    std::forward<SrcIteratorType>(begin),
                    std::forward<EndIteratorType>(end),
                    std::forward<DstIteratorType>(dst_it));
        }
};

template<typename BaseSrcIteratorType, typename BaseEndIteratorType, typename BaseDstIteratorType>
struct DeviceCopy<Cpu, Cpu, BaseSrcIteratorType, BaseEndIteratorType, BaseDstIteratorType>
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        return std::copy(
                     std::forward<SrcIteratorType>(begin),
                     std::forward<EndIteratorType>(end),
                     std::forward<DstIteratorType>(out));
    }
};

namespace {

// --------- Behaviour Flag Helpers
template<typename QueryFlag, typename... Flags>
class HasCopyFlag : public HasType<QueryFlag, std::tuple<Flags...>>
{
};


// --------- Behaviour Decision Making Helpers
template<typename... Flags>
struct CopyBehaviour
{
    private:
        template<typename Flag, typename OtherT>
        using SelectIfSpecified = typename std::conditional<HasCopyFlag<Flag, Flags...>::value
                                                          , Flag
                                                          , OtherT
                                                          >::type;
    public:
        typedef SelectIfSpecified<CopyFlags::ConvertOnSrc
                                 , SelectIfSpecified<CopyFlags::ConvertOnDst
                                                    ,CopyFlags::ConvertViaHost
                                                    >
                                 > UserSelectedFlags;

};

// --------- DeviceCopyDelegate --------
// perform the copy via specialisations with common pre and post operations
template<typename SrcArch, typename DstArch>
struct DeviceCopyDelegate
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        return DeviceCopy<SrcArch, DstArch, typename std::remove_reference<SrcIteratorType>::type, typename std::remove_reference<EndIteratorType>::type, DstIteratorType>::copy(
                     std::forward<SrcIteratorType>(begin),
                     std::forward<EndIteratorType>(end),
                     std::forward<DstIteratorType>(out));
    }
};

// host -> host transfers just use std::copy if not specialised

template<>
struct DeviceCopyDelegate<Cpu, Cpu>
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        return DeviceCopy<Cpu, Cpu
                         , typename std::remove_reference<SrcIteratorType>::type
                         , typename std::remove_reference<EndIteratorType>::type
                         , typename std::remove_reference<DstIteratorType>::type>
                   ::copy(
                     std::forward<SrcIteratorType>(begin),
                     std::forward<EndIteratorType>(end),
                     std::forward<DstIteratorType>(out));
    }
};

// host -> device transfers are passed through a PreProcessing stage to ensure contiguous memeory
template<typename DstArch>
struct DeviceCopyDelegate<Cpu, DstArch>
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
    {
        return DeviceCopyPreProcessor<DstArch, SrcIteratorType, EndIteratorType>::copy_to_device(std::forward<SrcIteratorType>(begin)
                                        , std::forward<EndIteratorType>(end)
                                        , std::forward<DstIteratorType>(dst_it)
                                        );
    }
};

// device -> host transfers are passed through a PostProcessing stage to take advantage of contiguous memeory if possible
template<typename SrcArch>
struct DeviceCopyDelegate<SrcArch, Cpu>
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
    {
        return DeviceCopyPostProcessor<SrcArch, SrcIteratorType, EndIteratorType, DstIteratorType>::copy_from_device(std::forward<SrcIteratorType>(begin)
                                        , std::forward<EndIteratorType>(end)
                                        , std::forward<DstIteratorType>(dst_it)
                                        );
    }
};

// --------- Type Conversion Helpers
template<typename BufferT, class IteratorT, typename Enable=void>
struct CopyDelegateBufferHelper;

// specialisation where where we can easily calculate the size of data e.g. random_access_iterators
template<typename BufferT, class IteratorT>
struct CopyDelegateBufferHelper<BufferT, IteratorT
                              , typename std::enable_if<std::is_base_of<std::random_access_iterator_tag
                                                                       , typename std::iterator_traits<IteratorT>::iterator_category
                                                                       >::value
                                                       , void>::type
                               >
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        // copy to buffer
        BufferT buffer(std::distance(begin, end));
        panda::copy(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), buffer.begin());

        // copy from buffer to dst
        return panda::copy<CopyFlags::ConvertOnSrc>(buffer.begin(), buffer.end(), out);

    }
};

// specialisation where we cannot easily pre-calculate the size of data e.g. forward_iterators
template<typename BufferT, class IteratorT>
struct CopyDelegateBufferHelper<BufferT, IteratorT
                              , typename std::enable_if<!std::is_base_of<std::random_access_iterator_tag
                                                                       , typename std::iterator_traits<IteratorT>::iterator_category
                                                                       >::value
                                                       , void>::type
                               >
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        // copy to buffer
        BufferT buffer(0);
        panda::copy(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), std::back_inserter(buffer));

        // copy from buffer to dst
        return panda::copy<CopyFlags::ConvertOnSrc>(buffer.begin(), buffer.end(), out);

    }
};

template<typename SrcValueT
       , typename DstValueT
       , typename CopyFlags
        >
struct TypeConversionDelegate;

template<typename SrcValueT
       , typename DstValueT
        >
struct TypeConversionDelegate<SrcValueT, DstValueT, CopyFlags::ConvertViaHost>
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        // copy to host before conversion
        return CopyDelegateBufferHelper<panda::Buffer<SrcValueT>, SrcIteratorType>::copy(
                                         std::forward<SrcIteratorType>(begin)
                                       , std::forward<EndIteratorType>(end)
                                       , std::forward<DstIteratorType>(out));
    }
};

template<typename SrcValueT
       , typename DstValueT
        >
struct TypeConversionDelegate<SrcValueT, DstValueT, CopyFlags::ConvertOnSrc>
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        typedef typename ArchitectureType<typename std::decay<SrcIteratorType>::type>::type SrcArch;

        // perform type conversion on src device
        return CopyDelegateBufferHelper<DeviceMemory<SrcArch, DstValueT>, SrcIteratorType>::copy(
                                         std::forward<SrcIteratorType>(begin)
                                       , std::forward<EndIteratorType>(end)
                                       , std::forward<DstIteratorType>(out));

    }
};

template<typename SrcValueT
       , typename DstValueT
        >
struct TypeConversionDelegate<SrcValueT, DstValueT, CopyFlags::ConvertOnDst>
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        typedef typename ArchitectureType<typename std::decay<DstIteratorType>::type>::type DstArch;

        // copy memory to destination without conversion
        return CopyDelegateBufferHelper<DeviceMemory<DstArch, SrcValueT>, SrcIteratorType>::copy(
                                         std::forward<SrcIteratorType>(begin)
                                       , std::forward<EndIteratorType>(end)
                                       , std::forward<DstIteratorType>(out));

    }
};

// --- DeviceCopyDispatch ----------
// dispatch the copy via the appropriate delegates
// ---------------------------------
template<typename SrcValueT
       , typename DstValueT
       , typename... CopyFlags
        >
struct DeviceCopyDispatchHelper
{
    // type conversion required
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        return TypeConversionDelegate<SrcValueT, DstValueT
                                    , typename CopyBehaviour<CopyFlags...>::UserSelectedFlags
                                    >::copy( std::forward<SrcIteratorType>(begin)
                                       , std::forward<EndIteratorType>(end)
                                       , std::forward<DstIteratorType>(out));
    }
};

template< typename SrcValueT
        , typename... CopyFlags
        >
struct DeviceCopyDispatchHelper<SrcValueT, SrcValueT, CopyFlags...>
{
    // no type conversion required
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        typedef typename ArchitectureType<typename std::decay<SrcIteratorType>::type>::type SrcArch;
        typedef typename ArchitectureType<typename std::decay<DstIteratorType>::type>::type DstArch;
        return DeviceCopyDelegate<SrcArch, DstArch>::copy(std::forward<SrcIteratorType>(begin)
                                                        , std::forward<EndIteratorType>(end)
                                                        , std::forward<DstIteratorType>(out));
    }
};

// for all non-host to host copies we need to pass through our delegate chain
template<typename SrcArch, typename DstArch, typename... CopyFlags>
struct DeviceCopyDispatch
{
    template<typename IteratorT>
    using ValueType = typename std::iterator_traits<typename std::remove_reference<IteratorT>::type>::value_type;

    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        return DeviceCopyDispatchHelper<typename std::remove_const<ValueType<SrcIteratorType>>::type
                                       , ValueType<DstIteratorType>
                                       , CopyFlags...
                                       >::copy( std::forward<SrcIteratorType>(begin)
                                              , std::forward<EndIteratorType>(end)
                                              , std::forward<DstIteratorType>(out));
    }
};

// for host based, we can go straight to the Delegate as std::copy will handle all the details
template<typename... CopyFlags>
struct DeviceCopyDispatch<Cpu, Cpu, CopyFlags...>
{
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    static inline
    typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
    {
        typedef typename ArchitectureType<typename std::decay<SrcIteratorType>::type>::type SrcArch;
        typedef typename ArchitectureType<typename std::decay<DstIteratorType>::type>::type DstArch;
        return DeviceCopyDelegate<SrcArch, DstArch>::copy(std::forward<SrcIteratorType>(begin)
                                                        , std::forward<EndIteratorType>(end)
                                                        , std::forward<DstIteratorType>(out));
    }
};

} // namespace

// generic copy - forward to DeviceCopyDelegate for the appropriate transform
template<typename... CopyFlags, typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
inline
typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& out)
{
    typedef typename ArchitectureType<typename std::decay<SrcIteratorType>::type>::type SrcArch;
    typedef typename ArchitectureType<typename std::decay<DstIteratorType>::type>::type DstArch;
    return DeviceCopyDispatch<SrcArch, DstArch, CopyFlags...>::copy( std::forward<SrcIteratorType>(begin)
                                   , std::forward<EndIteratorType>(end)
                                   , std::forward<DstIteratorType>(out));
}

} // namespace panda
} // namespace ska
