/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONTEXTBLOCK_H
#define SKA_PANDA_CONTEXTBLOCK_H

#include "panda/Engine.h"
#include "ChunkHolder.h"
#include <vector>
#include <memory>
#include <mutex>

namespace ska {
namespace panda {
    class Engine;
namespace packet_stream {

/**
 * @brief
 *    A block for managing contexts
 * @details
 *    Performs context management functions as the context requests travers
 *    the boudary into another chunk
 */
template<typename ContextT>
class ContextBlock
{
    public:
        typedef ContextT ContextType;
        typedef typename ContextT::DataTraits DataTraits;
        typedef typename ContextT::ChunkFactory ChunkFactory;
        typedef typename ContextT::LimitType LimitType;
        typedef typename ContextT::ChunkHolderType ChunkHolderType;

    public:
        template<typename... ConfigArgs>
        ContextBlock(panda::Engine& engine
                    , ChunkFactory& factory
                    , std::size_t number_of_contexts
                    , ConfigArgs&&... traits_config);
        ~ContextBlock();

        ContextBlock(ContextBlock const&) = delete;

        void next();

        /*
         * @brief the first context object
         */
        ContextT* front() const;

        /*
         * @brief link to the block provided
         */
        void next_block(ContextBlock& block);

        /*
         * @brief provide prev block information
         */
        void prev_block(ContextBlock& block);

        /*
         * @brief setup the purge cadence (number of contexts to wait before a context is purged)
         * @details do not call unless this block is incorporated inside a circular structure (next_block/prev_block have been called)
         * @param cadence 0=prev ctx, 1=ctx prior to this etc.
         */
        void purge_cadence(LimitType cadence);

        /*
         * @brief the factory for creating new data chunks to be filled from the packets
         */
        inline ChunkFactory& chunk_factory() const { return _factory; }

        /**
         * @brief set the number of missing packets that are allowed (i.e will receive missing_packet notifiactions)
         *        before the current chunk should be resized and flushed and a new one started
         */
        void missing_packets_before_realign(LimitType num);

        /*
         * @brief returnd the number of missing packets that are allowed (i.e will receive missing_packet notifiactions)
         *        before the current chunk should be resized and flushed and a new one started
         */
        LimitType missing_packets_before_realign() const;

        /// post a job to be processed by the engine
        template<typename PostJob>
        void post(PostJob&& job);

        /// set the engine to use
        void engine(panda::Engine& engine);

        /// return an unused ChunkHolderType ready for reinitialisation
        ChunkHolderType& next_chunk_holder() const;

        /// return the number of contexts in this block
        std::size_t size() const;

        // destroy the contexts and block until all have been destroyed
        // This call should only be made prior to the destructor
        void destroy_contexts();

    private:
        /*
         * @brief reset the blocks contexts state for reuse
         */
        void activate();


    private:
        mutable std::vector<std::shared_ptr<ChunkHolder<DataTraits>>> _chunks;
        mutable std::vector<std::shared_ptr<ContextType>> _contexts;
        bool _full;
        std::mutex _mutex;
        mutable std::mutex _chunk_holder_mutex;
        ContextBlock* _next;
        ContextBlock* _purge;
        ChunkFactory& _factory;
        panda::Engine* _engine;
        LimitType _allowed_missing_packets;
        std::atomic<std::size_t> _live_contexts; // guard in shutdown
};

} // namespace packet_stream
} // namespace panda
} // namespace ska

#include "ContextBlock.cpp"

#endif // SKA_PANDA_CONTEXTBLOCK_H
