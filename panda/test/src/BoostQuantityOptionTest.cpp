/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "BoostQuantityOptionTest.h"
#include "panda/BoostQuantityOption.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <boost/program_options.hpp>
#include <boost/units/systems/si/time.hpp>
#pragma GCC diagnostic pop


#include <vector>


namespace ska {
namespace panda {
namespace test {

static
boost::program_options::variables_map parse(std::vector<const char*> const& options, boost::program_options::options_description const& desc)
{
    char** argv = const_cast<char**>(options.data());
    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::command_line_parser(options.size(), argv)
                                  .options(desc).run(), vm);

    boost::program_options::notify(vm);
    return vm;
}

BoostQuantityOptionTest::BoostQuantityOptionTest()
    : ::testing::Test()
{
}

BoostQuantityOptionTest::~BoostQuantityOptionTest()
{
}

void BoostQuantityOptionTest::SetUp()
{
}

void BoostQuantityOptionTest::TearDown()
{
}

TEST_F(BoostQuantityOptionTest, test_basic_option)
{
    typedef boost::units::si::time Unit;
    typedef boost::units::quantity<Unit, float> Quantity;
    boost::program_options::options_description desc;

    Quantity quantity;
    desc.add_options()
        ("my_opt", boost::program_options::value<Quantity>(&quantity), "boost quantity option");
    std::vector<const char*> options = {"app_name", "--my_opt", "77.8910777"};
    auto vm = parse(options, desc);
    ASSERT_EQ(quantity.value(), (float)77.8910777);
}

TEST_F(BoostQuantityOptionTest, test_default_option)
{
    typedef boost::units::si::time Unit;
    typedef boost::units::quantity<Unit, float> Quantity;
    boost::program_options::options_description desc;

    Quantity quantity;
    Quantity default_quant(99.0000000 * boost::units::si::seconds);
    desc.add_options()
        ("my_opt", boost::program_options::value<Quantity>(&quantity)->default_value(default_quant), "boost quantity option");
    std::vector<const char*> options = {"app_name"};
    auto vm = parse(options, desc);
    ASSERT_EQ(quantity.value(), default_quant.value());
}

} // namespace test
} // namespace panda
} // namespace ska
